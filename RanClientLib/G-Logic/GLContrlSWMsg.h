#pragma once

#include "GLContrlBaseMsg.h"

namespace GLMSG
{
	#pragma pack(1)
	
	struct SNET_SCHOOL_WARS_START_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		
		int						nTIME;
		char					szName[TEXT_LEN];
		DWORD					dwBattleType;
		DWORD					dwID;

		SNET_SCHOOL_WARS_START_BRD () 
			: nTIME(0)
			, dwBattleType(0)
			, dwID(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_START_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_START_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOL_WARS_READY_FLD
	{
		NET_MSG_GENERIC			nmg;

		DWORD					dwID;

		SNET_SCHOOL_WARS_READY_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_READY_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_START_FLD
	{
		NET_MSG_GENERIC			nmg;

		DWORD					dwID;
		DWORD					dwBattleType;

		SNET_SCHOOL_WARS_START_FLD () 
			: dwID(0)
			, dwBattleType(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_START_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_END_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;
		DWORD					dwBattleType;
		bool					bForceExit;					

		SNET_SCHOOL_WARS_END_FLD () 
			: dwID(0)
			, bForceExit(false)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_END_FLD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_END_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_END_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];
		char					szClubName[CHAR_SZNAME];
		int						index;
		DWORD					dwBattleType;

		SNET_SCHOOL_WARS_END_BRD ()
			: index(0)
			, dwBattleType(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_END_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_END_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
			memset(szClubName, 0, sizeof(char) * CHAR_SZNAME);
		}
	};

	struct SNET_SCHOOL_WARS_REMAIN_BRD
	{
		NET_MSG_GENERIC			nmg;

		DWORD					dwTime;
		DWORD					dwBattleType;
		int						index;

		SNET_SCHOOL_WARS_REMAIN_BRD ()
			: dwTime(0)
			, dwBattleType(0)
			, index(0)
		{
			nmg.dwSize = sizeof ( SNET_SCHOOL_WARS_REMAIN_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOL_WARS_REMAIN_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_PPOINT_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		
		DWORD					dwDamage;
		DWORD					dwHeal;
		bool					bKillPoint;
		bool					bDamagePoint;
		bool					bHealPoint;
		bool					bResuPoint;

		SNET_SCHOOL_WARS_PPOINT_UPDATE () 
			: dwDamage(0)
			, dwHeal(0)
			, bKillPoint(false)
			, bDamagePoint(false)	
			, bHealPoint(false)
			, bResuPoint(false)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_PPOINT_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_PPOINT_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOL_WARS_MYPRANK_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		
		STWP_RANK_EX			sMySwPRank;		

		SNET_SCHOOL_WARS_MYPRANK_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_MYPRANK_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_MYPRANK_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOL_WARS_PRANKING_UPDATE
	{
		NET_MSG_GENERIC		nmg;

        WORD				wRankNum;		
		STWP_RANK			sSwPRank[RANKING_PNUM];

		SNET_SCHOOL_WARS_PRANKING_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_PRANKING_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDCHAR ( const STWP_RANK& sRank )
		{
			if ( RANKING_PNUM==wRankNum )		return false;

			sSwPRank[wRankNum] = sRank;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(STWP_RANK)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOL_WARS_PRANKING_REQ
	{
		NET_MSG_GENERIC		nmg;    
		
		DWORD				dwMapID;		
		
		SNET_SCHOOL_WARS_PRANKING_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_PRANKING_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_PRANKING_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       

	};

	struct SNETPC_SERVER_SCHOOL_WARS_INFO
	{
		NET_MSG_GENERIC		nmg;
		bool				bSchoolWars;
		DWORD				dwBattleType;
		int					index;

		SNETPC_SERVER_SCHOOL_WARS_INFO () :
			bSchoolWars(false),
			dwBattleType(0),
			index(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_SCHOOL_WARS_INFO;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNETPC_SERVER_SCHOOL_WARS_REMAIN_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwGaeaID;
		DWORD				dwBattleType;
		int					index;

		SNETPC_SERVER_SCHOOL_WARS_REMAIN_AG ()
			: dwGaeaID(0)
			, dwBattleType(0)
			, index(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_SERVER_SCHOOL_WARS_REMAIN_AG;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_SETWINSCHOOL_BRD
	{
		NET_MSG_GENERIC			nmg;
		WORD					wSchool;
		bool					bEnd;

		SNET_SCHOOL_WARS_SETWINSCHOOL_BRD () 
			: wSchool(GLSCHOOL_NUM)
			, bEnd(false)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_SETWINSCHOOL_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_SETWINSCHOOL_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_CERTIFIED_AGT
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;
		DWORD					dwSCHOOL;

		SNET_SCHOOL_WARS_CERTIFIED_AGT () 
			: dwID(0)
			, dwSCHOOL(3)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_CERTIFIED_AGT);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_CERTIFIED_AGT;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_DOBATTLEEND_AGT
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;
		DWORD					dwSCHOOL;
		bool					bEnd;

		SNET_SCHOOL_WARS_DOBATTLEEND_AGT () 
			: dwID(0)
			, dwSCHOOL(3)
			, bEnd(false)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_DOBATTLEEND_AGT);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_DOBATTLEEND_AGT;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_DOBATTLEEND_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;
		DWORD					dwSCHOOL;
		bool					bEnd;

		SNET_SCHOOL_WARS_DOBATTLEEND_FLD () 
			: dwID(0)
			, dwSCHOOL(3)
			, bEnd(false)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_DOBATTLEEND_FLD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_DOBATTLEEND_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	//add ctf
	struct SNETPC_SERVER_SCHOOLWAR_CAPTURE_AG
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC		nmg;
		DWORD		dwID;
		SNATIVEID	sID;
		char		szName[TEXT_LEN];
		char		szNameCrow[TEXT_LEN];
		SW_SCHOOL_DATA	emHOLDER;

		SNETPC_SERVER_SCHOOLWAR_CAPTURE_AG ()
			: dwID(0)
			, emHOLDER(SW_SCHOOL_MAX)
		{
			memset(szName, 0, sizeof(char) * TEXT_LEN);
			memset(szNameCrow, 0, sizeof(char) * TEXT_LEN);

			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_TO_AGENT_SCHOOLWAR_CAPTURE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_CAPTURE_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		SNATIVEID	sID;
		char		szName[TEXT_LEN];
		char		szNameCrow[TEXT_LEN];
		SW_SCHOOL_DATA	emHOLDER;

		SNET_SCHOOLWAR_CAPTURE_BRD ()
			: emHOLDER(SW_SCHOOL_MAX)
		{
			memset(szName, 0, sizeof(char) * TEXT_LEN);
			memset(szNameCrow, 0, sizeof(char) * TEXT_LEN);

			nmg.dwSize = sizeof ( SNET_SCHOOLWAR_CAPTURE_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOL_WARS_CAPTURE_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_TOWER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wNum;		
		SSCHOOLWAR_TOWER_STATUS	sTower[SW_TOWER_NUM];
	
		SNET_SCHOOLWAR_TOWER_UPDATE () 
			: wNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_TOWER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDTOWER ( const SSCHOOLWAR_TOWER_STATUS& sTOWER )
		{
			if ( SW_TOWER_NUM==wNum )		return false;

			sTower[wNum] = sTOWER;

			++wNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWAR_TOWER_STATUS)*wNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOL_WARS_CTFICON_REQ
	{
		NET_MSG_GENERIC		nmg;    
		
		DWORD				dwMapID;		
		
		SNET_SCHOOL_WARS_CTFICON_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_CTFICON_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_CTFICON_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       

	};

	struct SNET_SCHOOL_WARS_SEND_POINTREWARD_AGT
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwUserNum;
		DWORD					dwPoint;

		SNET_SCHOOL_WARS_SEND_POINTREWARD_AGT () 
			: dwUserNum(0)
			, dwPoint(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_SEND_POINTREWARD_AGT);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_SEND_POINTREWARD_AGT;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_GM_SCHOOL_EVENT
	{
		NET_MSG_GENERIC		nmg;
		BYTE				dwEventType; // 0 : EXP 1 : ITEM 2 : MONEY
		WORD				m_wSchool;
		float				fEventRate;
		bool				bStart;

		SNET_GM_SCHOOL_EVENT () :
				dwEventType(0),
				m_wSchool(GLSCHOOL_NUM),
				fEventRate(0.0f),
				bStart(FALSE)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GM_SCHOOL_EVENT;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOL_WARS_CERTIFIED_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;
		DWORD					dwSCHOOL;

		SNET_SCHOOL_WARS_CERTIFIED_FLD () 
			: dwID(0)
			, dwSCHOOL(3)
		{
			nmg.dwSize = sizeof(SNET_SCHOOL_WARS_CERTIFIED_FLD);
			nmg.nType = NET_MSG_GCTRL_SCHOOL_WARS_CERTIFIED_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNETPC_UPDATE_HP_MULTIPLIER
	{
		NET_MSG_GENERIC		nmg;
		float				fMultiplier[3];

		SNETPC_UPDATE_HP_MULTIPLIER() 
		{
			for(int i=0;i<3;i++)	fMultiplier[i]=0.0f;

			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_UPDATE_HP_MULTIPLIER;
		}
	};
	// Revert to default structure packing
	#pragma pack()
};