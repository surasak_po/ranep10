#include "StdAfx.h"

#include <algorithm>
#include "GLDefine.h"
#include "gltexfile.h"
#include "IniLoader.h"
#include "GLSuperiorWar.h"
#include "GLLandMan.h"
#include "GLGaeaServer.h"
#include "GLAgentServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

GLSuperiorWar::GLSuperiorWar ()
	: m_dwID( UINT_MAX )
	, m_emBattleState( BATTLE_NOSTART )
	, m_dwWarMap( 0 )
	, m_dwWarMapGate( 0 )
	, m_fCHECK_TIMER( 0.0f )
	, m_fCHECK_TIMER_MAX( 300.0f )
	, m_wPlayerScoreAdd( 0 )
	, m_fPartyScoreRange( 140.0f )

	, m_dwLastBattleHour( 0 )
	, m_dwBattleOrder( 0 )
	, m_dwBattleTime( 3600 )
	, m_bNotifyOneHour( false )
	, m_bNotifyHalfHour( false )
	, m_bNotify10MinHour( false )
	, m_fBattleTimer( 0.0f )

	, m_fRankingUpdate( 0.0f )
	, m_fRankingUpdateTime( 60.0f )
	, m_bAwardStart( false )
	
{
	m_mapMobScore.clear();
}

GLSuperiorWar& GLSuperiorWar::operator= ( const GLSuperiorWar& value )
{
	m_dwID					= value.m_dwID;
	m_strName				= value.m_strName;
	m_emBattleState			= value.m_emBattleState;
	m_dwWarMap				= value.m_dwWarMap;
	m_dwWarMapGate			= value.m_dwWarMapGate;
	m_fCHECK_TIMER			= value.m_fCHECK_TIMER;
	m_fCHECK_TIMER_MAX		= value.m_fCHECK_TIMER_MAX;
	m_wPlayerScoreAdd		= value.m_wPlayerScoreAdd;
	m_mapMobScore			= value.m_mapMobScore;
	m_fPartyScoreRange		= value.m_fPartyScoreRange;

	m_dwLastBattleHour		= value.m_dwLastBattleHour;
	m_dwBattleTime			= value.m_dwBattleTime;
	m_bNotifyOneHour		= value.m_bNotifyOneHour;
	m_bNotifyHalfHour		= value.m_bNotifyHalfHour;
	m_bNotify10MinHour		= value.m_bNotify10MinHour;
	m_fBattleTimer			= value.m_fBattleTimer;
	
	m_fRankingUpdate		= value.m_fRankingUpdate;
	m_fRankingUpdateTime	= value.m_fRankingUpdateTime;
	m_sAwardItem			= value.m_sAwardItem;
	m_vecAwardChar			= value.m_vecAwardChar;
	m_bAwardStart			= value.m_bAwardStart;


	for ( int i=0; i<MAX_TIME; ++i )
		m_sTIME[i] = value.m_sTIME[i];

	return *this;
}

bool GLSuperiorWar::Load ( std::string strFile )
{
	if( strFile.empty() )	return FALSE;

	std::string strPath;
	strPath = GLOGIC::GetServerPath ();
	strPath += strFile;

	CIniLoader cFILE;

	if( GLOGIC::bGLOGIC_ZIPFILE )
		cFILE.SetZipFile( GLOGIC::strGLOGIC_SERVER_ZIPFILE );

	if( !cFILE.open ( strPath, true ) )
	{
		CDebugSet::ToLogFile ( "ERROR : GLSuperiorWar::Load(), File Open %s", strFile.c_str() );
		return false;
	}
	
	cFILE.getflag( "SUPERIORWAR", "ID", 0, 1, m_dwID );
	cFILE.getflag( "SUPERIORWAR", "NAME", 0, 1, m_strName );

	SNATIVEID nidMAP;
	cFILE.getflag( "SUPERIORWAR", "WAR_MAP", 0, 2, nidMAP.wMainID );
	cFILE.getflag( "SUPERIORWAR", "WAR_MAP", 1, 2, nidMAP.wSubID );
	cFILE.getflag( "SUPERIORWAR", "WAR_GATE", 0, 1, m_dwWarMapGate );
	m_dwWarMap = nidMAP.dwID;

	DWORD dwNUM = cFILE.GetKeySize( "SUPERIORWAR", "BATTLE_TIME" );
	if( dwNUM > MAX_TIME )
	{
		CDebugSet::ToLogFile( "ERROR : GLSuperiorWar::Load(), %s, It was wrong size of BATTLE_TIME", strFile.c_str() );
		return false;
	}

	for( DWORD i=0; i<dwNUM; ++i )
	{
		cFILE.getflag( i, "SUPERIORWAR", "BATTLE_TIME", 0, 2, m_sTIME[i].dwWeekDay );
		cFILE.getflag( i, "SUPERIORWAR", "BATTLE_TIME", 1, 2, m_sTIME[i].dwStartTime );
	}

	cFILE.getflag( "SUPERIORWAR", "BATTLE_THE_TIME", 0, 1, m_dwBattleTime );
	cFILE.getflag( "SUPERIORWAR", "AWARD_TIME_LIMIT", 0, 1, m_fCHECK_TIMER_MAX );
	cFILE.getflag( "SUPERIORWAR", "RANKING_UPDATE_INTERVAL", 0, 1, m_fRankingUpdateTime );

	cFILE.getflag( "SUPERIORWAR", "SCORE_PLAYER", 0, 1, m_wPlayerScoreAdd );
	cFILE.getflag( "SUPERIORWAR", "SCORE_PLAYER_RANGE", 0, 1, m_fPartyScoreRange );


	DWORD dwNUMSCORE = cFILE.GetKeySize( "SUPERIORWAR", "SCORE_MOB" );
	for( DWORD i=0; i<dwNUMSCORE; ++i )
	{
		SSPW_MOBSCORE sSCORE;
		cFILE.getflag( i, "SUPERIORWAR", "SCORE_MOB", 0, 3, sSCORE.sIDMOB.wMainID );
		cFILE.getflag( i, "SUPERIORWAR", "SCORE_MOB", 1, 3, sSCORE.sIDMOB.wSubID );
		cFILE.getflag( i, "SUPERIORWAR", "SCORE_MOB", 2, 3, sSCORE.wMOBSCORE );
		m_mapMobScore[sSCORE.sIDMOB.dwID] = sSCORE;
	}

	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_LIMIT", 0, 1, m_sAwardItem.dwAwardLimit );
	cFILE.getflag( "SUPERIORWAR", "AWARD_SCORE_MIN", 0, 1, m_sAwardItem.dwMINSCORE );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_001", 0, 2, m_sAwardItem.nAwardItem[0].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_001", 1, 2, m_sAwardItem.nAwardItem[0].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_002", 0, 2, m_sAwardItem.nAwardItem[1].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_002", 1, 2, m_sAwardItem.nAwardItem[1].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_003", 0, 2, m_sAwardItem.nAwardItem[2].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_003", 1, 2, m_sAwardItem.nAwardItem[2].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_004", 0, 2, m_sAwardItem.nAwardItem[3].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_004", 1, 2, m_sAwardItem.nAwardItem[3].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_005", 0, 2, m_sAwardItem.nAwardItem[4].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_005", 1, 2, m_sAwardItem.nAwardItem[4].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_006", 0, 2, m_sAwardItem.nAwardItem[5].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_006", 1, 2, m_sAwardItem.nAwardItem[5].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_007", 0, 2, m_sAwardItem.nAwardItem[6].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_007", 1, 2, m_sAwardItem.nAwardItem[6].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_008", 0, 2, m_sAwardItem.nAwardItem[7].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_008", 1, 2, m_sAwardItem.nAwardItem[7].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_009", 0, 2, m_sAwardItem.nAwardItem[8].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_009", 1, 2, m_sAwardItem.nAwardItem[8].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_010", 0, 2, m_sAwardItem.nAwardItem[9].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_010", 1, 2, m_sAwardItem.nAwardItem[9].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_011", 0, 2, m_sAwardItem.nAwardItem[10].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_011", 1, 2, m_sAwardItem.nAwardItem[10].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_012", 0, 2, m_sAwardItem.nAwardItem[11].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_012", 1, 2, m_sAwardItem.nAwardItem[11].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_013", 0, 2, m_sAwardItem.nAwardItem[12].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_013", 1, 2, m_sAwardItem.nAwardItem[12].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_014", 0, 2, m_sAwardItem.nAwardItem[13].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_014", 1, 2, m_sAwardItem.nAwardItem[13].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_015", 0, 2, m_sAwardItem.nAwardItem[14].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_015", 1, 2, m_sAwardItem.nAwardItem[14].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_016", 0, 2, m_sAwardItem.nAwardItem[15].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_016", 1, 2, m_sAwardItem.nAwardItem[15].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_017", 0, 2, m_sAwardItem.nAwardItem[16].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_017", 1, 2, m_sAwardItem.nAwardItem[16].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_018", 0, 2, m_sAwardItem.nAwardItem[17].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_018", 1, 2, m_sAwardItem.nAwardItem[17].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_019", 0, 2, m_sAwardItem.nAwardItem[18].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_019", 1, 2, m_sAwardItem.nAwardItem[18].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_020", 0, 2, m_sAwardItem.nAwardItem[19].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_020", 1, 2, m_sAwardItem.nAwardItem[19].wSubID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_ETCH", 0, 2, m_sAwardItem.nAwardItem[20].wMainID );
	cFILE.getflag( "SUPERIORWAR", "AWARD_ITEM_ETCH", 1, 2, m_sAwardItem.nAwardItem[20].wSubID );

	return true;
}

bool GLSuperiorWar::IsEnterMap()
{
	if ( IsBattle() ) return true;
	if ( IsBattleReady() ) return true;

	return false;
}

BOOL GLSuperiorWar::CheckAwardTime( float fElaps )
{
	m_fCHECK_TIMER += fElaps;

	if ( m_fCHECK_TIMER > m_fCHECK_TIMER_MAX )
	{
		SetBattle( EM_BATTLE_STATE::BATTLE_END );
		m_fCHECK_TIMER = 0.0f;
		return TRUE;
	}

	return FALSE;
}


//agent server
DWORD GLSuperiorWar::IsBattleWeekDay ( int nDayOfWeek, int nHour )
{
	if ( IsBattle() )	return UINT_MAX;

	for ( DWORD i=0; i<MAX_TIME; ++i )
	{
		if ( (nDayOfWeek==m_sTIME[i].dwWeekDay) && 
			(nHour==m_sTIME[i].dwStartTime) &&
			(m_dwLastBattleHour!=nHour) )
		{
			return i;
		}
	}
	return UINT_MAX;
}

bool GLSuperiorWar::IsBattleHour ( DWORD dwORDER, int nHour )
{
	if ( dwORDER>=MAX_TIME )	return false;

	if ( m_sTIME[dwORDER].dwStartTime == nHour )
	{
		return true;
	}
		
	return false;
}

void GLSuperiorWar::UpdateNotifyBattle ( DWORD dwORDER, int nHour, int nMinute )
{
	if ( dwORDER>=MAX_TIME )	return;

	DWORD dwOTime = m_sTIME[dwORDER].dwStartTime;
	if ( dwOTime <= 0 )			return;

	dwOTime -= 1;
	if ( nHour == dwOTime )
	{
		if ( !m_bNotifyOneHour )
		{
			m_bNotifyOneHour = true;
			NotifyBattleBroadcast( 60-nMinute, FALSE );
		}

		if ( nMinute == 30 )
		{
			if ( !m_bNotifyHalfHour )
			{
				m_bNotifyHalfHour = true;
				NotifyBattleBroadcast( 30, FALSE );
			}
		}

		if ( nMinute == 50 )
		{
			if ( !m_bNotify10MinHour )
			{
				m_bNotify10MinHour = true;
				m_emBattleState = BATTLE_READY;
				NotifyBattleBroadcast( 10, TRUE );
			}
		}
	}
}

void GLSuperiorWar::NotifyBattleBroadcast( int nTIME, BOOL bREADY )
{
	GLMSG::SNET_SUPERIORWAR_START_BRD NetMsgBrd;
	NetMsgBrd.nTIME = nTIME;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SUPERIORWAR_START_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SUPERIORWAR:Update Time To Start ID:%d Time:%d Minutes", m_dwID, nTIME );

	if ( bREADY )
	{
		GLMSG::SNET_SUPERIORWAR_READY_FLD NetMsgFld;
		NetMsgFld.dwID = m_dwID;
		GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

		GLMSG::SNET_SUPERIORWAR_READY_BRD NetMsgReadyBrd;
		StringCchCopy ( NetMsgReadyBrd.szName, GLMSG::SNET_SUPERIORWAR_READY_BRD::TEXT_LEN, m_strName.c_str() );
		GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgReadyBrd );
		GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SUPERIORWAR:Ready Battle:ID %d", m_dwID );
	}
}

void GLSuperiorWar::DoBattleStart ( DWORD dwORDER, int nHour )
{
	m_emBattleState = BATTLE_START;
	m_dwBattleOrder = dwORDER;
	
	m_dwLastBattleHour = nHour;

	GLMSG::SNET_SUPERIORWAR_START_FLD NetMsgFld;
	NetMsgFld.dwID = m_dwID;
	GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

	GLMSG::SNET_SUPERIORWAR_START_BRD NetMsgBrd;
	NetMsgBrd.nTIME = 0;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SUPERIORWAR_START_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SUPERIORWAR:DoBattleStart:ID %d", m_dwID );
}

void GLSuperiorWar::DoBattleEndAward()
{
	m_emBattleState = BATTLE_END_AWARD;
	m_dwBattleOrder = UINT_MAX;

	m_bNotifyOneHour = false;
	m_bNotifyHalfHour = false;
	m_bNotify10MinHour = false;
	m_fBattleTimer = 0.0f;

	GLMSG::SNET_SUPERIORWAR_END_FLD NetMsgFld;
	NetMsgFld.dwID = m_dwID;
	GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

	GLMSG::SNET_SUPERIORWAR_END_BRD NetMsgBrd;
	NetMsgBrd.bREWARD = TRUE;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SUPERIORWAR_END_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SUPERIORWAR:DoBattleEndAward:ID %d", m_dwID );
}

void GLSuperiorWar::DoBattleEnd()
{
	m_emBattleState = BATTLE_END;
	m_fCHECK_TIMER = 0.0f;

	GLMSG::SNET_SUPERIORWAR_END_BRD NetMsgBrd;
	NetMsgBrd.bREWARD = FALSE;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SUPERIORWAR_END_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SUPERIORWAR:DoBattleEnd:ID %d", m_dwID );
}



//field server
void GLSuperiorWar::FieldBattleEnd()
{
	m_emBattleState = BATTLE_END;
	m_fCHECK_TIMER = 0.0f;

	m_mapScorePlayer.clear();
	m_vecAwardChar.clear();

	m_fRankingUpdate = 0.0f;
	m_bAwardStart = false;
}

SSUPERIORWAR_RANK_PLAYER* GLSuperiorWar::GetRankingPlayer( DWORD dwCharID )
{
	SUPERIORWAR_RANK_PLAYER_MAP_ITER pos = m_mapScorePlayer.find( dwCharID );
	if( pos == m_mapScorePlayer.end() )	return NULL;

	return &(*pos).second;;
}

SSPW_MOBSCORE* GLSuperiorWar::GetMobScore( DWORD dwID )
{
	SUPERIORWAR_MOBSCORE_MAP_ITER pos = m_mapMobScore.find( dwID );
	if( pos == m_mapMobScore.end() )	return NULL;

	return &(*pos).second;;
}

void GLSuperiorWar::AddScorePlayer( DWORD dwKillChar )
{
	PGLCHAR pCHARKILL = GLGaeaServer::GetInstance().GetCharID( dwKillChar );
	if ( pCHARKILL )
	{
		SSUPERIORWAR_RANK_PLAYER* pKillPlayer = GetRankingPlayer( dwKillChar );
		if ( pKillPlayer )
		{
			pKillPlayer->wPoints += m_wPlayerScoreAdd;
		}else{
			SSUPERIORWAR_RANK_PLAYER sRANK;
			sRANK.dwCharID = dwKillChar;
			StringCchCopy ( sRANK.szCharName, SPW_RANKING_NAME+1, pCHARKILL->m_szName );
			sRANK.szCharName[SPW_RANKING_NAME] = '\0';
			sRANK.nSCHOOL = (BYTE)pCHARKILL->m_wSchool;
			sRANK.nCLASS = (BYTE)CharClassToIndex( pCHARKILL->m_emClass );
			sRANK.wPoints += m_wPlayerScoreAdd;
			m_mapScorePlayer[sRANK.dwCharID] = sRANK;
		}
	}
}
void GLSuperiorWar::AddScoreMob( DWORD dwKillChar, SNATIVEID sMOBID )
{
	PGLCHAR pCHARKILL = GLGaeaServer::GetInstance().GetCharID( dwKillChar );
	if ( pCHARKILL )
	{
		SSPW_MOBSCORE* pSCORE = GetMobScore( sMOBID.dwID );
		if ( pSCORE )
		{
			SSUPERIORWAR_RANK_PLAYER* pKillPlayer = GetRankingPlayer( dwKillChar );
			if ( pKillPlayer )
			{
				pKillPlayer->wPoints += pSCORE->wMOBSCORE;
			}else{
				SSUPERIORWAR_RANK_PLAYER sRANK;
				sRANK.dwCharID = dwKillChar;
				StringCchCopy ( sRANK.szCharName, SPW_RANKING_NAME+1, pCHARKILL->m_szName );
				sRANK.szCharName[SPW_RANKING_NAME] = '\0';
				sRANK.nSCHOOL = (BYTE)pCHARKILL->m_wSchool;
				sRANK.nCLASS = (BYTE)CharClassToIndex( pCHARKILL->m_emClass );
				sRANK.wPoints += pSCORE->wMOBSCORE;
				m_mapScorePlayer[sRANK.dwCharID] = sRANK;
			}
		}
	}
}

WORD GLSuperiorWar::GetPoints( DWORD dwCHARID )
{
	PGLCHAR pCHAR = GLGaeaServer::GetInstance().GetCharID( dwCHARID );
	if ( pCHAR )
	{
		SSUPERIORWAR_RANK_PLAYER* pRANK = GetRankingPlayer( dwCHARID );
		if ( pRANK )
		{
			return pRANK->wPoints;
		}
	}

	return 0;
}
