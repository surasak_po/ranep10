#include "StdAfx.h"
#include "GLSuperiorWarMan.h"
#include "GLGaeaServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//field server
GLSuperiorWarFieldMan& GLSuperiorWarFieldMan::GetInstance()
{
	static GLSuperiorWarFieldMan cInstance;
	return cInstance;
}

bool GLSuperiorWarFieldMan::SetMapState ()
{
	for ( SPW_VEC::size_type i=0; i<m_vecWar.size(); ++i )
	{
		GLSuperiorWar &sSPW = m_vecWar[i];

		SNATIVEID nidMAP ( sSPW.m_dwWarMap );
		SMAPNODE *pMAPNODE = GLGaeaServer::GetInstance().FindMapNode ( nidMAP );
		if ( pMAPNODE )
		{
			pMAPNODE->bSPWZone = TRUE;
		}

		GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSuperiorWarMapID = sSPW.m_dwID;
			pLandMan->m_bSuperiorWarMap = true;	
		}
	}

	return true;
}

bool GLSuperiorWarFieldMan::FrameMove ( float fElaps )
{
	for ( SPW_VEC::size_type i=0; i<m_vecWar.size(); ++i )
	{
		GLSuperiorWar &sSPW = m_vecWar[i];

		if( sSPW.IsBattle() )
		{
			sSPW.m_fRankingUpdate += fElaps;
			if ( sSPW.m_fRankingUpdate > sSPW.m_fRankingUpdateTime )
			{
				UpdateRanking( sSPW.m_dwID );
				sSPW.m_fRankingUpdate = 0.0f;
			}

		}

		if ( sSPW.IsBattleEndAward() )
		{
			BOOL bENDED = sSPW.CheckAwardTime( fElaps );
			if ( bENDED )
			{
				EndBattleField( sSPW.m_dwID );
			}
			else
			{
				if ( sSPW.m_bAwardStart )
				{
					SendRewards( sSPW.m_dwID );
				}
			}
		}
	}

	return true;
}


bool GLSuperiorWarFieldMan::ReadyBattle( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return false;

	pSPW->SetBattle( GLSuperiorWar::BATTLE_READY );

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:ReadyBattle:ID %d", dwID );
	return true;
}

bool GLSuperiorWarFieldMan::BeginBattle ( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID);
	if ( !pSPW )	return false;

	pSPW->SetBattle( GLSuperiorWar::BATTLE_START );
	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:BeginBattle:ID %d", dwID );

	return true;
}

bool GLSuperiorWarFieldMan::EndBattleAward ( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return false;

	pSPW->SetBattle( GLSuperiorWar::BATTLE_END_AWARD );

	UpdateRanking( dwID );

	pSPW->m_bAwardStart = true;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:EndBattleAward:ID %d", dwID );

	return true;
}

bool GLSuperiorWarFieldMan::EndBattleField ( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return false;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:Reward Ended:ID %d RewardNum:%d", dwID, pSPW->m_vecAwardChar.size() );

	pSPW->SetBattle( GLSuperiorWar::BATTLE_END );
	pSPW->FieldBattleEnd();

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:EndBattleField:ID %d", dwID );

	return true;
}

void GLSuperiorWarFieldMan::UpdateRanking( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return;

	UpdateRankingPlayer( dwID );
	UpdateRankingSelf( dwID );
}

void GLSuperiorWarFieldMan::UpdateRankingPlayer( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return;

	GLSuperiorWar &sPW = *pSPW;

	SUPERIORWAR_RANK_PLAYER_VEC		m_vecNew;
	m_vecNew.reserve( sPW.m_mapScorePlayer.size() );

	SUPERIORWAR_RANK_PLAYER_MAP_ITER	pos = sPW.m_mapScorePlayer.begin();
	SUPERIORWAR_RANK_PLAYER_MAP_ITER	end = sPW.m_mapScorePlayer.end();
	for ( ; pos != end; pos++ )
	{
		const SSUPERIORWAR_RANK_PLAYER&	sRankInfo = pos->second;					
		m_vecNew.push_back( sRankInfo );
	}

	std::sort( m_vecNew.begin(), m_vecNew.end() );	

	int nSize = (int)m_vecNew.size();

	for ( int i = 0; i < nSize; ++i )
	{
		m_vecNew[i].nRanking = i+1;
		m_vecNew[i].nIndex = i;
	}

	for ( int i = nSize-1; i > 0; --i )
	{
		if ( m_vecNew[i] == m_vecNew[i-1] )
		{
			m_vecNew[i-1].nRanking = m_vecNew[i].nRanking;			
		}
	}	

	GLMSG::SNET_SUPERIORWAR_RANKING_PLAYER_UPDATE	NetMsg;

	for ( int i = 0; i <(int) m_vecNew.size(); ++i )
	{
		SUPERIORWAR_RANK_PLAYER_MAP_ITER iter = sPW.m_mapScorePlayer.find( m_vecNew[i].dwCharID );
		if ( iter == sPW.m_mapScorePlayer.end() )	continue;

		SSUPERIORWAR_RANK_PLAYER&	sRankInfo = iter->second;

		sRankInfo.nRanking = m_vecNew[i].nRanking;
		sRankInfo.nIndex = m_vecNew[i].nIndex;

		if ( m_vecNew[i].nIndex >= 0 && m_vecNew[i].nIndex < SPW_RANKING_NUM ) 
		{
			SSUPERIORWAR_RANK_PLAYER_CLIENT sRank = sRankInfo;
			NetMsg.ADDRANK( sRank );
		}		
	}

	if ( NetMsg.wRankNum > 0 )	
	{
		GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sPW.m_dwWarMap, &NetMsg );
		GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SUPERIORWAR:Update Ranking Player RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
	}
}

void GLSuperiorWarFieldMan::UpdateRankingSelf( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )	return;

	GLSuperiorWar &sSPW = *pSPW;

	GLMSG::SNET_SUPERIORWAR_RANKING_SELF_UPDATE	NetMsgMy;

	SUPERIORWAR_RANK_PLAYER_MAP_ITER	pos = sSPW.m_mapScorePlayer.begin();
	SUPERIORWAR_RANK_PLAYER_MAP_ITER	end = sSPW.m_mapScorePlayer.end();

	for ( ; pos != end; pos++ )
	{
		const SSUPERIORWAR_RANK_PLAYER&	sRankInfo = pos->second;
		NetMsgMy.sRank.nRanking		= sRankInfo.nRanking;
		NetMsgMy.sRank.wPoints		= sRankInfo.wPoints;
		NetMsgMy.sRank.nSCHOOL		= sRankInfo.nSCHOOL;
		NetMsgMy.sRank.nCLASS		= sRankInfo.nCLASS;
		NetMsgMy.sRank.bVALID		= TRUE;

		PGLCHAR pCHAR = GLGaeaServer::GetInstance().GetCharID( sRankInfo.dwCharID );
		if ( pCHAR )
		{
			GLGaeaServer::GetInstance().SENDTOCLIENT( pCHAR->m_dwClientID, &NetMsgMy );
		}
	}
}

void GLSuperiorWarFieldMan::SendRewards( DWORD dwID )
{
	GLSuperiorWar *pSPW = FindWar ( dwID );
	if ( !pSPW )						return;
	if ( !pSPW->IsBattleEndAward() )	return;
	if ( !pSPW->m_bAwardStart )		return;

	GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( pSPW->m_dwWarMap );
	if ( !pLandMan ) return;

	GLCHARNODE* pCharNode = pLandMan->m_GlobPCList.m_pHead;
	for ( ; pCharNode; pCharNode = pCharNode->pNext )
	{
		PGLCHAR pChar = GLGaeaServer::GetInstance().GetChar( pCharNode->Data->m_dwGaeaID );
		if ( !pChar ) continue;

		SUPERIORWAR_AWARD_CHAR_ITER iterRewarded = pSPW->m_vecAwardChar.find( pChar->m_dwCharID );
		if ( iterRewarded != pSPW->m_vecAwardChar.end() )	continue;

		SUPERIORWAR_RANK_PLAYER_MAP_ITER iterWinner = pSPW->m_mapScorePlayer.find( pChar->m_dwCharID );
		if ( iterWinner != pSPW->m_mapScorePlayer.end() ) 
		{
			SSUPERIORWAR_RANK_PLAYER& sRankInfo = iterWinner->second;

			int nAwardIndex;
			if ( sRankInfo.nRanking < SPW_REWARD_NUM ) nAwardIndex = sRankInfo.nRanking - 1;
			else if ( sRankInfo.nRanking <= pSPW->m_sAwardItem.dwAwardLimit && sRankInfo.wPoints >= pSPW->m_sAwardItem.dwMINSCORE ) nAwardIndex = ( SPW_REWARD_NUM -1 );
			else continue;

			SNATIVEID sNativeID = pSPW->m_sAwardItem.nAwardItem[nAwardIndex];

			SITEM* pItem = GLItemMan::GetInstance().GetItem( sNativeID );
			if ( !pItem ) continue;

			SITEMCUSTOM sITEM_NEW;
			sITEM_NEW.sNativeID = sNativeID;
			sITEM_NEW.tBORNTIME = CTime::GetCurrentTime().GetTime();
			sITEM_NEW.cGenType = EMGEN_SYSTEM;
			sITEM_NEW.cChnID = (BYTE)GLGaeaServer::GetInstance().GetServerChannel();
			sITEM_NEW.cFieldID = (BYTE)GLGaeaServer::GetInstance().GetFieldSvrID();
			sITEM_NEW.lnGenNum = GLITEMLMT::GetInstance().RegItemGen ( sITEM_NEW.sNativeID, (EMITEMGEN)sITEM_NEW.cGenType );

			CItemDrop cDropItem;
			cDropItem.sItemCustom = sITEM_NEW;
			if ( pChar->IsInsertToInvenEx ( &cDropItem ) )
			{
				pChar->InsertToInvenEx ( &cDropItem );
				GLITEMLMT::GetInstance().ReqItemRoute ( sITEM_NEW, ID_CLUB, pChar->m_dwGuild, ID_CHAR, pChar->m_dwCharID, EMITEM_ROUTE_SYSTEM, sITEM_NEW.wTurnNum );
			}else{
				pLandMan->DropItem ( pChar->m_vPos, &(cDropItem.sItemCustom), EMGROUP_ONE, pChar->m_dwGaeaID );
			}

			pSPW->m_vecAwardChar.insert( pChar->m_dwCharID );
		}
	}
}