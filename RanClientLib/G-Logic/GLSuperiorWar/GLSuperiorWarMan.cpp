#include "StdAfx.h"
#include "GLSuperiorWarMan.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

bool GLSuperiorWarMan::Load ( std::vector<std::string> &vecFiles )
{
	std::vector<std::string>::size_type i = 0, j = vecFiles.size();
	for( ; i < j; ++i )
	{
		GLSuperiorWar sWar;
		bool bOK = sWar.Load( vecFiles[i] );
		if( !bOK )
		{
			CDebugSet::ToLogFile ( "GLSuperiorWarMan.Load() fail, %s", vecFiles[i].c_str() );
		}

		m_vecWar.push_back ( sWar );
	}

	std::sort( m_vecWar.begin(), m_vecWar.end() );

	return true;
}

GLSuperiorWar* GLSuperiorWarMan::FindWar ( DWORD dwID )
{
	GLSuperiorWar cObj;
	cObj.m_dwID = dwID;

	SPW_VEC_ITER pos = std::lower_bound ( m_vecWar.begin(), m_vecWar.end(), cObj );
	if ( pos==m_vecWar.end() )
	{
		return NULL;
	}else{
		return &(*pos);
	}
}

bool GLSuperiorWarMan::IsBattle ()
{
	for ( SPW_VEC::size_type i=0; i<m_vecWar.size(); ++i )
	{
		GLSuperiorWar &sSPW = m_vecWar[i];
		if ( sSPW.IsBattle() )		return true;
	}

	return false;
}
