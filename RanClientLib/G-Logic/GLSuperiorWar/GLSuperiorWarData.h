
#include "GLCharDefine.h"
#include <set>

#pragma once

#define SPW_RANKING_NUM			20 //top 1 to 20 since we dont have top 0
#define SPW_RANKING_NAME		33
#define SPW_REWARD_NUM			21 // ( SPW_RANKING_NUM + 1 ) reward from top 1 ~ 20  and etch 

struct SSPW_TIME
{
	DWORD dwWeekDay;
	DWORD dwStartTime;

	SSPW_TIME () :
	dwWeekDay(0),
		dwStartTime(0)
	{
	}
};

struct SSPW_MOBSCORE
{
	SNATIVEID	sIDMOB;
	WORD		wMOBSCORE;

	SSPW_MOBSCORE () :
	sIDMOB(false),
		wMOBSCORE(0)
	{
	}
};

struct SSUPERIORWAR_RANK_PLAYER
{
	int		nIndex;   
	DWORD	dwCharID;
	BYTE	nSCHOOL;
	BYTE	nCLASS;
	BYTE	nRanking;
	WORD	wPoints;
	char	szCharName[SPW_RANKING_NAME+1];	

	SSUPERIORWAR_RANK_PLAYER()
		: nIndex ( -1 )
		, dwCharID( 0 )
		, nSCHOOL(0)
		, nCLASS(0)
		, nRanking(0)
		, wPoints(0)
	{
		memset( szCharName, 0, sizeof(char) * (SPW_RANKING_NAME+1) );
	}

	SSUPERIORWAR_RANK_PLAYER ( const SSUPERIORWAR_RANK_PLAYER &value )
	{
		operator=(value);
	}

	SSUPERIORWAR_RANK_PLAYER& operator = ( const SSUPERIORWAR_RANK_PLAYER& rvalue )
	{
		nIndex		= rvalue.nIndex;
		dwCharID	= rvalue.dwCharID;
		nSCHOOL		= rvalue.nSCHOOL;
		nCLASS		= rvalue.nCLASS;
		nRanking	= rvalue.nRanking;
		wPoints		= rvalue.wPoints;
		StringCchCopy( szCharName, SPW_RANKING_NAME+1, rvalue.szCharName );
		return *this;
	}

	bool operator < ( const SSUPERIORWAR_RANK_PLAYER& sRank )
	{			
		if ( wPoints > sRank.wPoints ) return true;
		return  false;
	}

	bool operator == ( const SSUPERIORWAR_RANK_PLAYER& sRank ) 
	{
		if ( wPoints == sRank.wPoints  ) return true;
		return false;
	}
};

struct SSUPERIORWAR_RANK_PLAYER_CLIENT
{
	int		nIndex;
	DWORD	dwCharID;
	BYTE	nSCHOOL;
	BYTE	nCLASS;
	BYTE	nRanking;
	WORD	wPoints;
	char	szCharName[SPW_RANKING_NAME+1];	

	SSUPERIORWAR_RANK_PLAYER_CLIENT() 
		: nIndex(-1)
		, nRanking (0)
		, wPoints(0)
		, nSCHOOL(0)
		, nCLASS(0)
	{
		memset( szCharName, 0, sizeof(char) * (SPW_RANKING_NAME+1) );
	}

	SSUPERIORWAR_RANK_PLAYER_CLIENT ( const SSUPERIORWAR_RANK_PLAYER_CLIENT &value )
	{
		operator=(value);
	}

	SSUPERIORWAR_RANK_PLAYER_CLIENT& operator = ( const SSUPERIORWAR_RANK_PLAYER_CLIENT& rvalue )
	{
		nIndex		= rvalue.nIndex;
		nRanking	= rvalue.nRanking;
		wPoints		= rvalue.wPoints;
		nSCHOOL		= rvalue.nSCHOOL;
		nCLASS		= rvalue.nCLASS;
		dwCharID	= rvalue.dwCharID;
		StringCchCopy( szCharName, SPW_RANKING_NAME+1, rvalue.szCharName );
		return *this;
	}

	SSUPERIORWAR_RANK_PLAYER_CLIENT ( const SSUPERIORWAR_RANK_PLAYER &value )
	{
		operator=(value);
	}

	SSUPERIORWAR_RANK_PLAYER_CLIENT& operator = ( const SSUPERIORWAR_RANK_PLAYER& rvalue )
	{
		nIndex		= rvalue.nIndex;
		nRanking	= rvalue.nRanking;
		wPoints		= rvalue.wPoints;
		nSCHOOL		= rvalue.nSCHOOL;
		nCLASS		= rvalue.nCLASS;
		dwCharID	= rvalue.dwCharID;

		StringCchCopy( szCharName, SPW_RANKING_NAME+1, rvalue.szCharName );
		return *this;
	}
};

struct SSUPERIORWAR_RANK_SELF
{
	BYTE	nRanking;
	WORD	wPoints;
	BYTE	nSCHOOL;
	BYTE	nCLASS;
	BOOL	bVALID;

	SSUPERIORWAR_RANK_SELF()
		: nRanking(0)
		, wPoints (0)
		, nSCHOOL(0)
		, nCLASS(0)
		, bVALID( FALSE )
	{
	}

	void Init()
	{
		nRanking	= 0;
		wPoints		= 0;
		nSCHOOL		= 0;
		nCLASS		= 0;
		bVALID		= FALSE;
	}
};


typedef std::map< DWORD, SSUPERIORWAR_RANK_PLAYER >		SUPERIORWAR_RANK_PLAYER_MAP;
typedef SUPERIORWAR_RANK_PLAYER_MAP::iterator			SUPERIORWAR_RANK_PLAYER_MAP_ITER;

typedef std::map< DWORD, SSPW_MOBSCORE >				SUPERIORWAR_MOBSCORE_MAP;
typedef SUPERIORWAR_MOBSCORE_MAP::iterator				SUPERIORWAR_MOBSCORE_MAP_ITER;

typedef std::vector<SSUPERIORWAR_RANK_PLAYER>			SUPERIORWAR_RANK_PLAYER_VEC;
typedef SUPERIORWAR_RANK_PLAYER_VEC::iterator			SUPERIORWAR_RANK_PLAYER_VEC_ITER;

typedef std::vector<SSUPERIORWAR_RANK_PLAYER_CLIENT>	SUPERIORWAR_RANK_PLAYER_CLIENT_VEC;
typedef SUPERIORWAR_RANK_PLAYER_CLIENT_VEC::iterator	SUPERIORWAR_RANK_PLAYER_CLIENT_VEC_ITER;

typedef std::set<DWORD>									SUPERIORWAR_AWARD_CHAR;
typedef SUPERIORWAR_AWARD_CHAR::iterator				SUPERIORWAR_AWARD_CHAR_ITER;


struct SSUPERIORWAR_AWARD_ITEM
{
	SNATIVEID	nAwardItem[SPW_REWARD_NUM];
	DWORD		dwAwardLimit;
	DWORD		dwMINSCORE;

	SSUPERIORWAR_AWARD_ITEM()
		: dwAwardLimit(SPW_REWARD_NUM)
		, dwMINSCORE( 0 )
	{	
		memset( nAwardItem, -1, sizeof( SNATIVEID ) * SPW_REWARD_NUM );
	}
};

/*
                               ........
                              .IHHHHHHMMMHI.
                            .IHHHHHHHMMMMM.IHI:.
                          .IHHHMMMMMMMMMM.IHHHHHI.
                         .IHHHHHMMMMMMMM.IHHMMMMMHI.
                        .IHHHMMMMMMMMMM.HHMMMMMMMMHI.
                       .IHHHHMMMMMMMMM.HMMMMMMMMMMMHI.
                      .IHHMMMMMMM:''''':IHHHMMMMMMMMHI.
                    .:IHHHMMMM:'.   '.  .:IHHMMMMMMMMHI.
                   .IHHHMMMH:'        .   ':IHHMMMMMMMHI.
                   .:IHHMMM'     .       .  IHHMMMMMMMMHI.
                  .:IHMMM:.  .      .     . ':IHHMMMMMHHI.
                  .IHHMMM:.                  .:IHHMMMMMMH.
                 .:IIHMMM:..           .APMMPHA.IHHMMMMMMI
                 .IHHMMMM.PPMMMA     .APPV''' VI.HHHMMMMMI
                .:IHHHMMMM. 'VDDA.  .    ...   .:IHHHMMMMI.
                .:IHHIHMMM   ..    ..  .:IM): . .:IHHHMMMH:
                :IHIIHMMM .:(MI:  ..    '''   ..:IHHHMMMI:
                .:IHHHMMM:   ''    ..       .   .:IHHMMMMI:
                .IHHHIMMM:.  .,    ...   .     ..:IHHMMMMI:
                .:IHHIMMM:.   .   ..:.      .  .:IHHHMMMMII
               .:IHHHIMMM:.   .  .".".    .   ..:IHHHMMMMHI
              .:IHHHHIMVM:..       :          ..:IHHHMMMMHI
             .:IHHHHIIMMI:.. .   .-.-..  .  ...:IHHHMMMMHI.
             ..:IHHHIIMMMI:..  .APMMMMPA.   . ..:IHMMMMHHHI:
            .:IHIHHIHHHHI:..   'VMMMMV'    ...:IHHHMMMMMHI:
           .:IHIHIHIIHMMMHI:..    ''''     ...:IHIHHMMMHHHHI
           .:IHIHHHIIHHMMMI:...   .   .   ..::IHHHMMMMMHHHH:
          .:IIHIHHH NHHHMMMHI:...   .    ..:IHHMMMMIMMHHHHHI:
         .:IHHIIHHHHIIHMMMMMMMI:::...-'''IIHHHHMMMIHHHHIIIH:.
         ':IHIHHHHHHHIMMMMMHHI::.. . .  ..:IHHMMMIHMMMMMHHHII:
          .:IIHHHHHHHIIMMMMMMI:.:.  .   .:IHHMMMMIHHHMHHHHHIH:.
          .:IHHIHHHHHHIMMMMMMI:... . . .:IHHMMMIHHHHHHHIHHHHI:.
           .IHIHIHHHHHIHMMMMMI:.. .  . ..:IHHMMMIHHMMMMMHHHHI:.
           .:IHHHHIHHHHIMMMMMI:. .  .  ..:IHHMMMIHMMMMHHHHHIHI:.
          ..:IHHHIHHHMMMIHMMI:... .   . .:IHHMMIHMMMHHHHIIN:'AMM.
           ':IHIHHIHHHIHIIMMM:...  .    ..XX.MHHIHMMHHII:' A......'.
            '.:IHIHHHHHIIHMMMI:.. .   . XX. .HMMMIHHI::'  A.........'.
             .:IHHIIHIIHHHMMHI: ..  .XX.  .  .IHMHI::' . A............'.
              ':IIHHIIHHHMMMHI:'   .XX.       '''''' .  A..............'.
                ':IIHHIIHHMH:'   .XX.   .      .    .   H................
                 /.:IIHHMMH:'  .XX.        .      .   . H...............I
                 /.IHHI:'..  .XX.   .   .    .      .   H...............W
                /..: . X:'  .XX.     .         .     .  H..............IW
               /. .WW.X.   .XX.     .      .     .     .H..............WI
              / .WW  X.   .XX.     .     .      .      IH.............IWI
             /. WW. X.   .XX.        .         .    .  HI.............IWI
            .  WW .X.   XX:.      .         .    .     H..............IWI
           /..WW. X    XX.      .    .    .    .       H..............IWI
          /.WW.  X    XX.      .     .     .      .   .H..............IWW
         /WW.   X   .XX.     .       .         .      .H..............WWI
        /.A . .X   .XX.  .  .   .      .    .    .    .H..............WW:
        A'  . X.  .XX     .   .      .        .       .H.............IWW.
      A'    ..X. .XX   .    .      .        .         .H.............IWW.
    A'     .:X. .X'     .:IHI:.   .  .    .    .     .:H.............WWI.
   A'     ..:X..X'     ..:IHI:.     .      .      .  ..H............IWW'.
  AI I   .::X...X:..    .:IHI:     .   .   .    .    .:H............WWI .
 AMI .  ..::X...X:..     .:HI.    .      .   .       .:H...........IWW .
 MMI .  ...:X..X:... .    .I.   .    .      .   .   ..:H...........IWW. .
 VM.   ..:.:X..X:...  .  .    .   .    .     .       .:H...........IWW .
 'M: .  ..::X..X:.. .      .    .    .     .         .:H...........WWI. .
  V;.  . ..:X.:X:I.. .  .     .    .    .     .      .:H..........WWI .
  'V:.  . .:X..X:.V:..    .  .  .     .   .         ..VH..........WWI. .
   'V. . .::X.:X:..V:... .     .   .    .    .      :V.H.........IWW . ..
    'V:.. .:X:.X:. .V:... .  .    .   . /  .   .   .V' H.........IWW...
      V:. .:X..X:. ..V::.. .    .  .  .V    .     .:V V.........IWW... .
    / . V..:X..X:... ..V::::....   ..V'  .   .   ..:' V.........IWW.... .
   /.    V.:X..X:... . . V:::::::.'V   .  .    ..:V.A..........IWW.......
  /. . WWV:.X.X::.. .  .   '''' .   .   .     ..:V A..........IWW. .....
 A. .WWJ' :.X:X::... . .  .  . .     .   .   ..:V A..........WWV........
A.WWWW'  H:.X:X:::.. .  .   .      .    .  . .:V AI.........WWWIH........
WWWJS  . 'V.X.X:...  .   .  .   .   .   .  ..:V  V.........WWW. V:.......
WWJ. .   A':X.X::...  . .  .   . .   .     ..:V  H........WWW.  IH:......
WM  .   A H:X.X::... .  .   .    .      . ...:H  HW......WWW.  . V:......
W'  .  A .H:X.X::.... .  . .   .  .  .    ..:H IHWWWWWWWWW.  .  IH:.....
I.  . A . .:X:X:.... . .   .    .  .     . ..:H HIIWWWWWWW.  .    V:.....
. .  A. . .:X:X::.. . . .    .    .  .    . .:IAHII......H  .   . IH:....
  . A.  . .:X:X:.... .   . .  .   .   .  .   ..AHI.......H.  .   . H:....
   A .  ..::X:X:...... .  .  .   .   .     .  ..AI......IH..   .   V::...
  A.  .. .::X/X:.....  .  . .  .    .  .   .   ..A......IH.  .  .  IH::..
 A   . .:.::X:X:... .  . . . . .   .   .     . ..:A.....HI  .   .   V.:..
A.  . ....::X:X::..... .  .  .  .  .   .    .  . .:A...IH.  .   .  .IH::.
   . ....:::X:X::... ..... .  . .   .   .   .   .  .A..HI.  .  .  .  V::.
  . . .....:X:X:.... .. . . .  . .   .  .    .   .  .A.HI.  .  .    ..H:.
.  . ...::::X:X:.I:.  . . .. . .  .  .   .   .   .   .AHI..... .    . V::
   .  ..::::X:X:IHH::. . .  . .  .   .      .     . . .AI.     .   .  IH:
. . ....::::X:X:IMHI:. . . .    .   .     .     .      .:A..  .   .  . V:
 . ...::::V:X X:IMI:. . .... . .   .   .  .    .     .  .:A..  .  .   .IH
. ..:::::V:.X:X'V.H:.. . . ...  .   .  .   .      .      ..:A.  .   .  .V
 ..:::::V . X X H:... .  . . . . . . . .   .   .   .       .:A. .   . . .
..::::.'    X X IH:.. . . . . . .  .  .  .  .   .   .    .  .:A.  . . . .
..:::V   .  X X  V:........... .  . .     .   .  .   .       .:A. . . . .
 ..V'  .  . X X .H::............ . . .   .  .   .  .     .     .A. . . .
.V' .   .   X.X  H:........... . .   .     .     .       .       A. . . .
 .   .   .  X X .X::......... . . . .   .     .     .   .       .IA. . . 
. .   .   . X X .H:............. .  .  .   .      .   .   .     .IH.. . .
 .  .   .  .X.X  I...::::.:.:.:. .  . .    .    .  .             IH:.. .. 
. .   .   . X X  H  . .:::.:.:..... .  . .   .   .   .     .      I:.. ..
.   .   . . X X .H.   ..:::::.:.:... . .  .     .  .      .      .HI ...
   . .:.  . X X  H. .  .:IHIHIHII:;:. . . . .  .  .     .     .   HI. . .
   .:. .  . X X. H:.. .::IHIHIHIHMMHII:... . .  .  .        .   . HI.....
. . : . . .000X  H::...:IHMHMMMMMMHHI::... .  .   .     .    .   .HI.....
 . : .   . 000X. H:::..::IHMHMMMMMMHI:::. .  .    .    .     .    HI.:...
. .:' . . .000X  A:. .::::IHHMMMMMMMHI:::.. .    .      .        .HI.....
 .:. . .  .000X.:I....::::IHHMMMMMMMMMHI::. . .   .    .     .   .HI.....
..:.  .  . 000X :H.  ..:::IIHHMMMMMMMHI::.:.. .   .    .     .   .H......
 :  .  .   'V'X :H: ...::::IHHMMMMMMMI::.... .  .  .    .  .     :IWWWWWW
:. .    .   . X IH.. ...::::IHHHMMMMM::.......  .  . .   .      .:VWWWWW'
. .  .    .  .X I:........::IIIHMMMMI:::....   .   .   .   .    .:'WWWW'
.   .   .   . X.H... ......::IIHHMM::::.....   .  .   .     .  .V'WWW''
  .   .   .  000I.. .....:.:::IIHM:::.... ..  . .   .  .    . .:' 'W'
.   .   .   .000.. . ...:::::IHMM::::...... .  .    .     .   .V'  .  . .
 . :.    .  .000.. . ...:::.:IHMM:::.:.... .   .  .    .   .  .:'   .   .
  .:.  .   ..000.:. . . ..::::IHV::::. . .   . .   .     .   V. .   .   
 . .:   .  . 000... ......:::IHM:::.... . .  .    .   .    . V. .   .  .
  . :.  . .  'VH.. .......::IHMV::::....... .      .   .   V .   .      
.. .:. .   . IH. .. .....::IHM::::... . .  .  .   .   .  . V.  .    .   .
  . :. .     IH.. . ....::IHMMV::.. .  .  .  .   .     .   .    .     .
.. .:.  .  . IH. . .....:.IHM:::.... .  .   .  .   .    . V.  .   .     .
 . ..:.  .   HH. . .....::IHMV:::......... .   .   .   . V' . .  .   . WW
. . .:. .   :HI. .. .....:IHM:::....... .  .  . .   . ..V . .  .  .:WWWWW
 . . .:. . .IH. . . ..:::IHMV:::..... .  .   .   .   ..V' .  . .:WWWWWJ''
. . . .:. .:HI.  . ....:IHM:.........  .  .   .   .  .V'  .  ..:WWWJ''
 . . ..:. .IH . .....:::IHMV:::..... .  .   .   .   .V' . ...WWWJ''
  . . ..:. HI. . ....::IHM:::....... . . .   .   . .V' . . .WWWJ'
 . . . . .:HI: .. ....::IHV::::..... . .  .  .  . .V' ...WWWJ'
  . . . .:IH. .  . ...::IM::........ .  .   .   ..V  .:WWWJ'
W. . . . .HI.. . ....:IHMV:::...... .  .   . .  .V' .WWWJ'
 WWWW.. . H.. . . ..::HM::::...... . .   .  .  .V' .WWWJ'
  'WWWW. IH.. .. ...:IHMV:::.. .... . .  .  . .V'WWWWJ''
    ''WWWWH.. . . .:::HM:::....... .  .   . ..V'WWWJ'
       'WWH. . ....:IHMV:::..... .   .    .  V'WW''
         IH.. . .:.:IHM:::....... . .   . ..V'WW'
         HI. . .:.::IHV:::....... . .    ..V'W'
         H::....:::IHM::.:...... .  . .  .V'
         H... . .::IHV::...... . .  .  . V'
        :H. . . ..::M:::...... . .   . .V'
        IH. .. ..:IHV:::.....  .  .   .V'
        HI. . . .:IH:... ..... .  .  .V'
        H . . .:::IV:::.... . .. . . V'
       .H. . . .:::I:::..... .  .  .V'
*/