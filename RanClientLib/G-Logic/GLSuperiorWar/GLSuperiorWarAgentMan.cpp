#include "StdAfx.h"
#include "GLSuperiorWarMan.h"
#include "GLAgentServer.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//agent server
GLSuperiorWarAgentMan& GLSuperiorWarAgentMan::GetInstance()
{
	static GLSuperiorWarAgentMan cInstance;
	return cInstance;
}

bool GLSuperiorWarAgentMan::SetMapState ()
{
	for ( SPW_VEC::size_type i=0; i<m_vecWar.size(); ++i )
	{
		GLSuperiorWar &sSPW= m_vecWar[i];

		SNATIVEID nidMAP ( sSPW.m_dwWarMap );
		GLAGLandMan *pLandMan = GLAgentServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSuperiorWarMapID = sSPW.m_dwID;
			pLandMan->m_bSuperiorWarMap = true;
		}
	}

	return true;
}

bool GLSuperiorWarAgentMan::FrameMove ( float fElapsedAppTime )
{
	if ( GLCONST_CHAR::bPKLESS ) return false;

	for ( SPW_VEC::size_type i=0; i<m_vecWar.size(); ++i )
	{
		GLSuperiorWar &sSPW = m_vecWar[i];

		CTime cCurTime = CTime::GetCurrentTime();
		int nDayOfWeek = cCurTime.GetDayOfWeek ();
		int nDay = cCurTime.GetDay ();
		int nHour = cCurTime.GetHour();
		int nMinute = cCurTime.GetMinute ();

		DWORD dwORDER = sSPW.IsBattleWeekDay(nDayOfWeek, nHour );
		if ( dwORDER!=UINT_MAX )
		{
			if ( sSPW.IsBattleHour ( dwORDER, nHour ) )
			{
				sSPW.DoBattleStart ( dwORDER, nHour );
				m_fRemainTimer = (float)sSPW.m_dwBattleTime;
				m_fTimer = 0.0f;
			}
			else
			{
				sSPW.UpdateNotifyBattle ( dwORDER, nHour, nMinute );
			}
		}

		if ( sSPW.IsBattle() )
		{
			sSPW.m_fBattleTimer += fElapsedAppTime;
			if ( sSPW.m_fBattleTimer > float(sSPW.m_dwBattleTime) )
			{
				sSPW.DoBattleEndAward();
			}
		}

		if ( sSPW.IsBattleEndAward() )
		{
			BOOL bENDED = sSPW.CheckAwardTime( fElapsedAppTime );
			if ( bENDED )
			{
				sSPW.DoBattleEnd();
			}
		}
	}

	if ( !m_vecWar.empty() )
	{
		if ( m_vecWar[0].IsBattle() )
		{
			GLMSG::SNET_SUPERIORWAR_REMAIN_BRD NetMsgBrd;

			if ( m_fRemainTimer == m_vecWar[0].m_dwBattleTime )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
			}

			m_fTimer += fElapsedAppTime;
			m_fRemainTimer -= fElapsedAppTime;

			if ( m_fTimer > 600.0f )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
				m_fTimer = 0.0f;
			}
		}
	}

	return true;
}