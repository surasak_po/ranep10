#pragma once

#include "GLSuperiorWar.h"

class GLSuperiorWarMan
{
public:
	typedef std::vector<GLSuperiorWar>	SPW_VEC;
	typedef SPW_VEC::iterator			SPW_VEC_ITER;

protected:
	SPW_VEC	m_vecWar;

public:
	bool Load ( std::vector<std::string> &vecFiles );

public:
	GLSuperiorWar* FindWar ( DWORD dwID );
	bool IsBattle ();

public:
	GLSuperiorWarMan ()
	{
	}

public:
};


class GLSuperiorWarFieldMan : public GLSuperiorWarMan
{
public:

public:
	bool FrameMove ( float fElaps );

public:
	bool SetMapState ();

	bool ReadyBattle( DWORD dwID );
	bool BeginBattle ( DWORD dwID );
	bool EndBattleAward ( DWORD dwID );
	bool EndBattleField ( DWORD dwID );

	void UpdateRanking( DWORD dwID );
	void UpdateRankingPlayer( DWORD dwID );
	void UpdateRankingSelf( DWORD dwID );

	void SendRewards( DWORD dwID );

protected:
	GLSuperiorWarFieldMan ()
	{
	}

public:
	static GLSuperiorWarFieldMan& GetInstance();
};


class GLSuperiorWarAgentMan : public GLSuperiorWarMan
{
protected:
	float m_fRemainTimer, m_fTimer;
	float m_fEndedTimer;

public:
	bool SetMapState ();

public:
	DWORD GetRemainTime () { return (DWORD)m_fRemainTimer; }

public:
	bool FrameMove ( float fElapsedAppTime );

public:
	GLSuperiorWarAgentMan ()
		: m_fRemainTimer(0)
		, m_fTimer(0)
		, m_fEndedTimer( 0.0f )
	{
	}

public:
	static GLSuperiorWarAgentMan& GetInstance();
};