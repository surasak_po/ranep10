#pragma once

#include "GLContrlBaseMsg.h"

namespace GLMSG
{
	#pragma pack(1)

	struct SNETPC_SERVER_SCHOOLWARPVP_INFO
	{
		NET_MSG_GENERIC		nmg;
		bool				bSchoolWarPVP;

		SNETPC_SERVER_SCHOOLWARPVP_INFO () :
		bSchoolWarPVP(false)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_SCHOOLWARPVP_INFO;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};


	struct SNET_SCHOOLWARPVP_START_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWARPVP_START_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_START_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_START_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		int						nTIME;
		char					szName[TEXT_LEN];

		SNET_SCHOOLWARPVP_START_BRD () 
			: nTIME(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_START_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_START_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWARPVP_READY_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWARPVP_READY_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_READY_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_READY_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];

		SNET_SCHOOLWARPVP_READY_BRD () /*:*/ 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_READY_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_READY_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWARPVP_REMAIN_BRD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwTime;

		SNET_SCHOOLWARPVP_REMAIN_BRD ()
			: dwTime(0)
		{
			nmg.dwSize = sizeof ( SNET_SCHOOLWARPVP_REMAIN_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOLWARPVP_REMAIN_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNETPC_SERVER_SCHOOLWARPVP_REMAIN_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwGaeaID;

		SNETPC_SERVER_SCHOOLWARPVP_REMAIN_AG ()
			: dwGaeaID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_SERVER_SCHOOLWARPVP_REMAIN_AG;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_END_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWARPVP_END_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_END_FLD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_END_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_END_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];
		bool					bREWARD;

		SNET_SCHOOLWARPVP_END_BRD () :
			bREWARD( false )
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_END_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_END_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWARPVP_RANKING_PLAYER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWARPVP_RANK_PLAYER_CLIENT	sRank[SWPVP_RANKING_NUM];

		SNET_SCHOOLWARPVP_RANKING_PLAYER_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_RANKING_PLAYER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWARPVP_RANK_PLAYER_CLIENT& sRANK )
		{
			if ( SWPVP_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWARPVP_RANK_PLAYER_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOLWARPVP_RANKING_SCHOOL_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWARPVP_RANK_SCHOOL_CLIENT		sRank[SWPVP_SCHOOL_RANKING_NUM];

		SNET_SCHOOLWARPVP_RANKING_SCHOOL_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_RANKING_SCHOOL_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWARPVP_RANK_SCHOOL_CLIENT& sRANK )
		{
			if ( SWPVP_SCHOOL_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWARPVP_RANK_SCHOOL_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOLWARPVP_RANKING_SELF_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		SSCHOOLWARPVP_RANK_SELF	sRank;		

		SNET_SCHOOLWARPVP_RANKING_SELF_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_RANKING_SELF_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_RANKING_SELF_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWARPVP_POINT_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		bool					bKillPoint;

		SNET_SCHOOLWARPVP_POINT_UPDATE () 
			: bKillPoint(false)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_POINT_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_POINT_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWARPVP_RANKING_REQ
	{
		NET_MSG_GENERIC		nmg;    
		DWORD				dwMapID;		

		SNET_SCHOOLWARPVP_RANKING_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_RANKING_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_RANKING_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};


	struct SNET_SCHOOLWARPVP_WINNER_REQ
	{
		NET_MSG_GENERIC		nmg;    
		DWORD				dwMapID;		

		SNET_SCHOOLWARPVP_WINNER_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_WINNER_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_WINNER_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};

	struct SNETPC_SERVER_SCHOOLWARPVP_WINNER_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwID;
		WORD				wSCHOOL;

		SNETPC_SERVER_SCHOOLWARPVP_WINNER_AG ()
			: dwID(0)
			, wSCHOOL(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_TO_AGENT_SCHOOLWARPVP_WINNER;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_WINNER_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		WORD					wSCHOOL;
		char					szName[TEXT_LEN];


		SNET_SCHOOLWARPVP_WINNER_BRD ()
			: wSCHOOL(0)
		{
			memset(szName, 0, sizeof(char) * TEXT_LEN);

			nmg.dwSize = sizeof ( SNET_SCHOOLWARPVP_WINNER_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOLWARPVP_WINNER_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWARPVP_WINNER_SCHOOL_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		SSCHOOLWARPVP_RANK_SCHOOL_WINNER		sWinner;		

		SNET_SCHOOLWARPVP_WINNER_SCHOOL_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWARPVP_WINNER_SCHOOL_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_WINNER_SCHOOL_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWARPVP_WINNER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWARPVP_RANK_PLAYER_CLIENT			sRank[SWPVP_RANKING_NUM];

		SNET_SCHOOLWARPVP_WINNER_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWARPVP_WINNER_PLAYER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWARPVP_RANK_PLAYER_CLIENT& sRANK )
		{
			if ( SWPVP_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWARPVP_RANK_PLAYER_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};


	// Revert to default structure packing
	#pragma pack()
};