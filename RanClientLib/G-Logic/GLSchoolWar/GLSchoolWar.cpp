#include "StdAfx.h"

#include <algorithm>
#include "GLDefine.h"
#include "gltexfile.h"
#include "IniLoader.h"
#include "GLSchoolWar.h"
#include "GLLandMan.h"
#include "GLGaeaServer.h"
#include "GLAgentServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

GLSchoolWar::GLSchoolWar ()
	: m_dwID( UINT_MAX )
	, m_emBattleState( BATTLE_NOSTART )
	, m_dwSchoolWarMap( 0 )
	, m_dwSchoolWarMapGate( 0 )
	, m_dwSchoolWarHallMap( 0 )
	, m_dwSchoolWarHallGate( 0 )
	, m_fCHECK_TIMER( 0.0f )
	, m_fCHECK_TIMER_MAX( 300.0f )
	, m_wSchoolWinner( MAX_SCHOOL )
	, m_nCTFWinner(-1)
	, m_dwLastBattleDay( 0 )
	, m_dwBattleOrder( 0 )
	, m_dwBattleTime( 3600 )
	, m_dwBattleTimePrevious( 0 )
	, m_dwBattleTimeNext( 0 )
	, m_dwNextTime( 0 )
	, m_bNotifyOneHour( false )
	, m_bNotifyHalfHour( false )
	, m_bNotify10MinHour( false )
	, m_fBattleTimer( 0.0f )


	, m_fRankingUpdate( 0.0f )
	, m_fRankingUpdateTime( 60.0f )
	, m_bAwardStart( false )
	
{
}

GLSchoolWar& GLSchoolWar::operator= ( const GLSchoolWar& value )
{
	m_dwID					= value.m_dwID;
	m_strName				= value.m_strName;
	m_emBattleState			= value.m_emBattleState;
	m_dwSchoolWarMap		= value.m_dwSchoolWarMap;
	m_dwSchoolWarMapGate	= value.m_dwSchoolWarMapGate;
	m_dwSchoolWarHallMap	= value.m_dwSchoolWarHallMap;
	m_dwSchoolWarHallGate	= value.m_dwSchoolWarHallGate;
	m_fCHECK_TIMER			= value.m_fCHECK_TIMER;
	m_fCHECK_TIMER_MAX		= value.m_fCHECK_TIMER_MAX;
	m_wSchoolWinner			= value.m_wSchoolWinner;

	m_dwLastBattleDay		= value.m_dwLastBattleDay;
	m_dwBattleTime			= value.m_dwBattleTime;
	m_dwBattleTimeNext		= value.m_dwBattleTimeNext;
	m_dwBattleTimePrevious	= value.m_dwBattleTimePrevious;
	m_dwNextTime			= value.m_dwNextTime;
	m_bNotifyOneHour		= value.m_bNotifyOneHour;
	m_bNotifyHalfHour		= value.m_bNotifyHalfHour;
	m_bNotify10MinHour		= value.m_bNotify10MinHour;
	m_fBattleTimer			= value.m_fBattleTimer;
	m_fRankingUpdate		= value.m_fRankingUpdate;
	m_fRankingUpdateTime	= value.m_fRankingUpdateTime;
	m_sAwardItem			= value.m_sAwardItem;
	m_vecAwardChar			= value.m_vecAwardChar;
	m_bAwardStart			= value.m_bAwardStart;

	for ( int i=0; i<MAX_TIME; ++i )
		m_sSWTIME[i] = value.m_sSWTIME[i];

	return *this;
}

bool GLSchoolWar::Load ( std::string strFile )
{
	if( strFile.empty())	return FALSE;

	std::string strPath;
	strPath = GLOGIC::GetServerPath ();
	strPath += strFile;

	CIniLoader cFILE;

	if( GLOGIC::bGLOGIC_ZIPFILE )
		cFILE.SetZipFile( GLOGIC::strGLOGIC_SERVER_ZIPFILE );

	if( !cFILE.open ( strPath, true ) )
	{
		CDebugSet::ToLogFile ( "ERROR : GLSchoolWar::Load(), File Open %s", strFile.c_str() );
		return false;
	}
	
	cFILE.getflag( "SCHOOLWAR", "ID", 0, 1, m_dwID );
	cFILE.getflag( "SCHOOLWAR", "NAME", 0, 1, m_strName );

	SNATIVEID nidMAP;
	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_MAP", 0, 2, nidMAP.wMainID );
	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_MAP", 1, 2, nidMAP.wSubID );
	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_GATE", 0, 1, m_dwSchoolWarMapGate );
	m_dwSchoolWarMap = nidMAP.dwID;

	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_HALL_MAP", 0, 2, nidMAP.wMainID );
	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_HALL_MAP", 1, 2, nidMAP.wSubID );
	cFILE.getflag( "SCHOOLWAR", "SCHOOLWAR_HALL_GATE", 0, 1, m_dwSchoolWarHallGate );
	m_dwSchoolWarHallMap = nidMAP.dwID;

	DWORD dwNUM = cFILE.GetKeySize( "SCHOOLWAR", "BATTLE_TIME" );
	if( dwNUM > MAX_TIME )
	{
		CDebugSet::ToLogFile( "ERROR : GLSchoolWar::Load(), %s, It was wrong size of BATTLE_TIME", strFile.c_str() );
		return false;
	}

	for( DWORD i=0; i<dwNUM; ++i )
	{
		cFILE.getflag( i, "SCHOOLWAR", "BATTLE_TIME", 0, 3, m_sSWTIME[i].dwWeekDay );
		cFILE.getflag( i, "SCHOOLWAR", "BATTLE_TIME", 1, 3, m_sSWTIME[i].dwStartTime );
		cFILE.getflag( i, "SCHOOLWAR", "BATTLE_TIME", 2, 3, m_sSWTIME[i].dwStartMin );

	}

	cFILE.getflag( "SCHOOLWAR", "BATTLE_THE_TIME", 0, 1, m_dwBattleTime );
	cFILE.getflag( "SCHOOLWAR", "BATTLE_LAST_TIME", 0, 1, m_dwBattleTimeNext );
	cFILE.getflag( "SCHOOLWAR", "BATTLE_NEXT_TIME", 0, 1, m_dwNextTime );
	cFILE.getflag( "SCHOOLWAR", "BATTLE_PREVIOUS_TIME", 0, 1, m_dwBattleTimePrevious );
	cFILE.getflag( "SCHOOLWAR", "AWARD_TIME_LIMIT", 0, 1, m_fCHECK_TIMER_MAX );
	cFILE.getflag( "SCHOOLWAR", "RANKING_UPDATE_INTERVAL", 0, 1, m_fRankingUpdateTime );

	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_LIMIT", 0, 1, m_sAwardItem.dwAwardLimit );
	cFILE.getflag( "SCHOOLWAR", "AWARD_KILL_MIN", 0, 1, m_sAwardItem.dwKillMin );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_001", 0, 2, m_sAwardItem.nAwardItem[0].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_001", 1, 2, m_sAwardItem.nAwardItem[0].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_002", 0, 2, m_sAwardItem.nAwardItem[1].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_002", 1, 2, m_sAwardItem.nAwardItem[1].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_003", 0, 2, m_sAwardItem.nAwardItem[2].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_003", 1, 2, m_sAwardItem.nAwardItem[2].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_004", 0, 2, m_sAwardItem.nAwardItem[3].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_004", 1, 2, m_sAwardItem.nAwardItem[3].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_005", 0, 2, m_sAwardItem.nAwardItem[4].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_005", 1, 2, m_sAwardItem.nAwardItem[4].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_006", 0, 2, m_sAwardItem.nAwardItem[5].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_006", 1, 2, m_sAwardItem.nAwardItem[5].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_007", 0, 2, m_sAwardItem.nAwardItem[6].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_007", 1, 2, m_sAwardItem.nAwardItem[6].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_008", 0, 2, m_sAwardItem.nAwardItem[7].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_008", 1, 2, m_sAwardItem.nAwardItem[7].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_009", 0, 2, m_sAwardItem.nAwardItem[8].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_009", 1, 2, m_sAwardItem.nAwardItem[8].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_010", 0, 2, m_sAwardItem.nAwardItem[9].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_010", 1, 2, m_sAwardItem.nAwardItem[9].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_011", 0, 2, m_sAwardItem.nAwardItem[10].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_011", 1, 2, m_sAwardItem.nAwardItem[10].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_012", 0, 2, m_sAwardItem.nAwardItem[11].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_012", 1, 2, m_sAwardItem.nAwardItem[11].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_013", 0, 2, m_sAwardItem.nAwardItem[12].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_013", 1, 2, m_sAwardItem.nAwardItem[12].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_014", 0, 2, m_sAwardItem.nAwardItem[13].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_014", 1, 2, m_sAwardItem.nAwardItem[13].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_015", 0, 2, m_sAwardItem.nAwardItem[14].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_015", 1, 2, m_sAwardItem.nAwardItem[14].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_016", 0, 2, m_sAwardItem.nAwardItem[15].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_016", 1, 2, m_sAwardItem.nAwardItem[15].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_017", 0, 2, m_sAwardItem.nAwardItem[16].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_017", 1, 2, m_sAwardItem.nAwardItem[16].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_018", 0, 2, m_sAwardItem.nAwardItem[17].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_018", 1, 2, m_sAwardItem.nAwardItem[17].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_019", 0, 2, m_sAwardItem.nAwardItem[18].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_019", 1, 2, m_sAwardItem.nAwardItem[18].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_020", 0, 2, m_sAwardItem.nAwardItem[19].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_020", 1, 2, m_sAwardItem.nAwardItem[19].wSubID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_ETCH", 0, 2, m_sAwardItem.nAwardItem[20].wMainID );
	cFILE.getflag( "SCHOOLWAR", "AWARD_ITEM_ETCH", 1, 2, m_sAwardItem.nAwardItem[20].wSubID );

	return true;
}

bool GLSchoolWar::IsEnterMap( WORD wSCHOOL )
{
	if ( IsBattle() ) return true;
	if ( IsBattleReady() ) return true;

	if ( m_wSchoolWinner != MAX_SCHOOL )
	{
		if ( m_wSchoolWinner == wSCHOOL )	
			return true;
	}

	return false;
}

BOOL GLSchoolWar::CheckAwardTime( float fElaps )
{
	//m_fCHECK_TIMER += fElaps;

	//if ( m_fCHECK_TIMER > m_fCHECK_TIMER_MAX )
	//{
	SetBattle( EM_BATTLE_STATE::BATTLE_END );
	m_fCHECK_TIMER = 0.0f;
	return TRUE;
	//}

	//return FALSE;
}

void GLSchoolWar::SetCTFWinner(int nSchool)
{
	if( nSchool == -1 ) return;

	m_nCTFWinner = nSchool;
}


//agent server
DWORD GLSchoolWar::IsBattleWeekDay ( int nDayOfWeek, int nDay )
{
	if ( IsBattle() )	return UINT_MAX;

	for ( DWORD i=0; i<MAX_TIME; ++i )
	{

		if ( (nDayOfWeek==m_sSWTIME[i].dwWeekDay) && 
			(nDay==m_sSWTIME[i].dwStartTime) &&
			(m_dwLastBattleDay!=nDay) )
		{
		    m_dwBattleTimeNext = m_sSWTIME[i+1].dwStartTime;
		    m_dwBattleTimePrevious = m_sSWTIME[i].dwStartTime;
			return i;
		}
		else if ( (nDayOfWeek==m_sSWTIME[i].dwWeekDay) && 
			 (nDay==m_sSWTIME[i].dwStartTime-1) &&
			 (m_dwLastBattleDay!=nDay) )
		{
			return i;
		}
	}
	return UINT_MAX;
}

bool GLSchoolWar::IsBattleHour ( DWORD dwORDER, int nHour, int nMinute )
{
	if ( dwORDER>=MAX_TIME )	return false;

	if ( m_sSWTIME[dwORDER].dwStartTime == nHour && m_sSWTIME[dwORDER].dwStartMin == nMinute )
	{
		return true;
	}
		
	return false;
}

void GLSchoolWar::UpdateNotifyBattle ( DWORD dwORDER, int nHour, int nMinute )
{
	if ( dwORDER>=MAX_TIME )	return;

	DWORD dwOTime = m_sSWTIME[dwORDER].dwStartTime;
	if ( dwOTime <= 0 )			return;
	
	DWORD dwOTimeMin = m_sSWTIME[dwORDER].dwStartMin;
	if( dwOTimeMin > 0 )
	{
		dwOTimeMin -= nMinute;
		if( dwOTimeMin == 30)
		{
			if ( !m_bNotifyHalfHour )
			{
				m_bNotifyHalfHour = true;
			    NotifyBattleBroadcast( 30, FALSE );			
			}
		}
		if ( dwOTimeMin == 10 )
		{
			if ( !m_bNotify10MinHour )
			{
			  m_bNotify10MinHour = true;
			  m_emBattleState = BATTLE_READY;
			  NotifyBattleBroadcast( 10, FALSE );
			}
		}
	}
	else
	{

	dwOTime -= 1;
	if ( nHour == dwOTime )
	{
		if ( !m_bNotifyOneHour )
		{
			m_bNotifyOneHour = true;
			NotifyBattleBroadcast( 60-nMinute, FALSE );
		}

		if ( nMinute == 30 )
		{
			if ( !m_bNotifyHalfHour )
			{
				m_bNotifyHalfHour = true;
				NotifyBattleBroadcast( 30, FALSE );
			}
		}

		if ( nMinute == 50 )
		{
			if ( !m_bNotify10MinHour )
			{
				m_bNotify10MinHour = true;
				m_emBattleState = BATTLE_READY;
				NotifyBattleBroadcast( 10, TRUE );
			}
		}
    }
  }
}

void GLSchoolWar::NotifyBattleBroadcast( int nTIME, BOOL bREADY )
{
	GLMSG::SNET_SCHOOLWAR_START_BRD NetMsgBrd;
	NetMsgBrd.nTIME = nTIME;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SCHOOLWAR_START_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );

	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SCHOOLWARINFO:School War Update Time To Start ID:%d Time:%d Minutes", m_dwID, nTIME );

	if ( bREADY )
	{
		GLMSG::SNET_SCHOOLWAR_READY_FLD NetMsgFld;
		NetMsgFld.dwID = m_dwID;
		GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

		GLMSG::SNET_SCHOOLWAR_READY_BRD NetMsgReadyBrd;
		StringCchCopy ( NetMsgReadyBrd.szName, GLMSG::SNET_SCHOOLWAR_READY_BRD::TEXT_LEN, m_strName.c_str() );
		GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgReadyBrd );

		GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SCHOOLWARINFO:School War Ready Battle:ID %d", m_dwID );
	}
}

void GLSchoolWar::DoBattleStart ( DWORD dwORDER, int nDay )
{
	m_emBattleState = BATTLE_START;
	m_dwBattleOrder = dwORDER;
	
	m_dwLastBattleDay = nDay;
	m_nCTFWinner = -1;

	GLMSG::SNET_SCHOOLWAR_START_FLD NetMsgFld;
	NetMsgFld.dwID = m_dwID;
	GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

	GLMSG::SNET_SCHOOLWAR_START_BRD NetMsgBrd;
	NetMsgBrd.nTIME = 0;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SCHOOLWAR_START_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );

	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SCHOOLWARINFO:School War DoBattleStart:ID %d", m_dwID );
}

void GLSchoolWar::DoBattleEndAward()
{
	m_emBattleState = BATTLE_END_AWARD;
	m_dwBattleOrder = UINT_MAX;

	m_bNotifyOneHour = false;
	m_bNotifyHalfHour = false;
	m_bNotify10MinHour = false;
	m_fBattleTimer = 0.0f;


	GLMSG::SNET_SCHOOLWAR_END_FLD NetMsgFld;
	NetMsgFld.dwID = m_dwID;
	GLAgentServer::GetInstance().SENDTOCHANNEL ( &NetMsgFld, 0 );

	GLMSG::SNET_SCHOOLWAR_END_BRD NetMsgBrd;
	NetMsgBrd.bREWARD = TRUE;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SCHOOLWAR_END_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );

	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SCHOOLWARINFO:School War DoBattleEndAward:ID %d", m_dwID );
}

void GLSchoolWar::DoBattleEnd()
{
	m_emBattleState = BATTLE_END;
	m_wSchoolWinner = MAX_SCHOOL;
	m_fCHECK_TIMER = 0.0f;


	GLMSG::SNET_SCHOOLWAR_END_BRD NetMsgBrd;
	NetMsgBrd.bREWARD = FALSE;
	StringCchCopy ( NetMsgBrd.szName, GLMSG::SNET_SCHOOLWAR_END_BRD::TEXT_LEN, m_strName.c_str() );
	GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );

	GLAgentServer::GetInstance().CONSOLEMSG_WRITE( "SCHOOLWARINFO:School War DoBattleEnd:ID %d", m_dwID );
}



//field server
void GLSchoolWar::FieldBattleEnd()
{
	m_emBattleState = BATTLE_END;
	m_wSchoolWinner = MAX_SCHOOL;
	m_fCHECK_TIMER = 0.0f;

	m_mapScorePlayer.clear();
	m_mapScoreSchool.clear();
	m_mapScoreWinner.clear();
	m_vecAwardChar.clear();

	m_fRankingUpdate = 0.0f;
	m_bAwardStart = false;
}

SSCHOOLWAR_RANK_PLAYER* GLSchoolWar::GetRankingPlayer( DWORD dwCharID )
{
	SCHOOLWAR_RANK_PLAYER_MAP_ITER pos = m_mapScorePlayer.find( dwCharID );
	if( pos == m_mapScorePlayer.end() )	return NULL;

	return &(*pos).second;;
}

SSCHOOLWAR_RANK_SCHOOL* GLSchoolWar::GetRankingSchool( WORD wSchool )
{
	SCHOOLWAR_RANK_SCHOOL_MAP_ITER pos = m_mapScoreSchool.find( wSchool );
	if( pos == m_mapScoreSchool.end() )	return NULL;

	return &(*pos).second;;
}

void GLSchoolWar::AddScore( DWORD dwKillChar, DWORD dwDeathChar )
{
	PGLCHAR pCHARKILL = GLGaeaServer::GetInstance().GetCharID( dwKillChar );
	if ( pCHARKILL )
	{
		SSCHOOLWAR_RANK_PLAYER* pKillPlayer = GetRankingPlayer( dwKillChar );
		if ( pKillPlayer )
		{
			pKillPlayer->wKillNum++;
		}else{
			SSCHOOLWAR_RANK_PLAYER sRANK;
			sRANK.dwCharID = dwKillChar;
			StringCchCopy ( sRANK.szCharName, SW_RANKING_NAME+1, pCHARKILL->m_szName );
			sRANK.szCharName[SW_RANKING_NAME] = '\0';
			sRANK.wSCHOOL = pCHARKILL->m_wSchool;
			sRANK.wKillNum++;
			m_mapScorePlayer[sRANK.dwCharID] = sRANK;
		}

		SSCHOOLWAR_RANK_SCHOOL* pKillSchool = GetRankingSchool( pCHARKILL->m_wSchool );
		if ( pKillSchool )
		{
			pKillSchool->wKillNum++;
		}else{
			SSCHOOLWAR_RANK_SCHOOL sRANK;	
			sRANK.wSCHOOL = pCHARKILL->m_wSchool;
			sRANK.wKillNum++;
			m_mapScoreSchool[sRANK.wSCHOOL] = sRANK;
		}
	}

	PGLCHAR pCHARDEATH = GLGaeaServer::GetInstance().GetCharID( dwDeathChar );
	if ( pCHARDEATH )
	{
		SSCHOOLWAR_RANK_PLAYER* pDeathPlayer = GetRankingPlayer( dwDeathChar );
		if ( pDeathPlayer )
		{
			pDeathPlayer->wDeathNum++;
		}else{
			SSCHOOLWAR_RANK_PLAYER sRANK;
			sRANK.dwCharID = dwDeathChar;
			StringCchCopy ( sRANK.szCharName, SW_RANKING_NAME+1, pCHARDEATH->m_szName );
			sRANK.szCharName[SW_RANKING_NAME] = '\0';
			sRANK.wSCHOOL = pCHARDEATH->m_wSchool;
			sRANK.wDeathNum++;
			m_mapScorePlayer[sRANK.dwCharID] = sRANK;
		}

		SSCHOOLWAR_RANK_SCHOOL* pDeathSchool = GetRankingSchool( pCHARDEATH->m_wSchool );
		if ( pDeathSchool )
		{
			pDeathSchool->wDeathNum++;
		}else{
			SSCHOOLWAR_RANK_SCHOOL sRANK;	
			sRANK.wSCHOOL = pCHARDEATH->m_wSchool;
			sRANK.wDeathNum++;
			m_mapScoreSchool[sRANK.wSCHOOL] = sRANK;
		}
	}
}

void GLSchoolWar::AddWinner( SSCHOOLWAR_RANK_PLAYER sRANK )
{
	SCHOOLWAR_RANK_PLAYER_MAP_ITER pos = m_mapScoreWinner.find( sRANK.dwCharID );
	if( pos == m_mapScoreWinner.end() )
	{
		m_mapScoreWinner[sRANK.dwCharID] = sRANK;
	}
}