#pragma once

#include "GLSchoolWar.h"

class GLSchoolWarMan
{
public:
	typedef std::vector<GLSchoolWar>	SW_VEC;
	typedef SW_VEC::iterator			SW_VEC_ITER;

public:
	SW_VEC	m_vecSchoolWar;

public:
	bool Load ( std::vector<std::string> &vecFiles );

public:
	GLSchoolWar* FindSchoolWar ( DWORD dwID, BOOL bCHECK );
	bool IsBattle ();

public:
	GLSchoolWarMan ()
	{
	}

public:
};


class GLSchoolWarFieldMan : public GLSchoolWarMan
{
public:

public:
	bool FrameMove ( float fElaps );

public:
	bool SetMapState ();


	bool ReadyBattle( DWORD dwID );
	bool BeginBattle ( DWORD dwID );
	bool EndBattleAward ( DWORD dwID );
	bool EndBattleField ( DWORD dwID );

	void UpdateRanking( DWORD dwID );
	void UpdateRankingPlayer( DWORD dwID );
	void UpdateRankingSchool( DWORD dwID );
	void UpdateRankingSelf( DWORD dwID );

	void CheckWinner( DWORD dwID );
	void SendRewards( DWORD dwID );


protected:
	GLSchoolWarFieldMan ()
	{
	}

public:
	static GLSchoolWarFieldMan& GetInstance();
};




class GLSchoolWarAgentMan : public GLSchoolWarMan
{
protected:
	float m_fRemainTimer, m_fTimer, m_fTime;
	float m_fNextBattleTime;
	float m_fPreviousBattleTime;
	bool  m_bNextWar;
	bool  m_bFirstStart;
	bool  m_bBattleEnd;
public:
	bool SetMapState ();

public:
	DWORD GetRemainTime () { return (DWORD)m_fRemainTimer; }

public:
	bool FrameMove ( float fElapsedAppTime );

public:
	GLSchoolWarAgentMan ()
		: m_fRemainTimer(0)
		, m_fTimer(0)
		, m_fNextBattleTime( 0.0f )
		, m_fPreviousBattleTime( 0.0f )
		, m_fTime(0.0f)
		, m_bNextWar(false)
        , m_bBattleEnd(false)
		, m_bFirstStart(false)
	{
	}

public:
	static GLSchoolWarAgentMan& GetInstance();
};