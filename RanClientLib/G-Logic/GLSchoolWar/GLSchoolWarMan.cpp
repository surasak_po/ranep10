#include "StdAfx.h"
#include "GLSchoolWarMan.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

bool GLSchoolWarMan::Load ( std::vector<std::string> &vecFiles )
{
	std::vector<std::string>::size_type i = 0, j = vecFiles.size();
	for( ; i < j; ++i )
	{
		GLSchoolWar sSchoolWar;
		bool bOK = sSchoolWar.Load( vecFiles[i] );
		if( !bOK )
		{
			CDebugSet::ToLogFile ( "GLSchoolWar.Load() fail, %s", vecFiles[i].c_str() );
		}

		m_vecSchoolWar.push_back ( sSchoolWar );
	}

	std::sort( m_vecSchoolWar.begin(), m_vecSchoolWar.end() );

	return true;
}

GLSchoolWar* GLSchoolWarMan::FindSchoolWar ( DWORD dwID, BOOL bCHECK )
{
	GLSchoolWar cObj;
	cObj.m_dwID = dwID;

	SW_VEC_ITER pos = std::lower_bound ( m_vecSchoolWar.begin(), m_vecSchoolWar.end(), cObj );
	if ( pos==m_vecSchoolWar.end() )
	{
		return NULL;
	}else{
		return &(*pos);
	}
}

bool GLSchoolWarMan::IsBattle ()
{
	for ( SW_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWar &sSW = m_vecSchoolWar[i];
		if ( sSW.IsBattle() )		return true;
	}

	return false;
}
