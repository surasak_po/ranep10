#include "StdAfx.h"
#include "GLSchoolWarMan.h"
#include "GLGaeaServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//field server
GLSchoolWarFieldMan& GLSchoolWarFieldMan::GetInstance()
{
	static GLSchoolWarFieldMan cInstance;
	return cInstance;
}

bool GLSchoolWarFieldMan::SetMapState ()
{
	for ( SW_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWar &sSW = m_vecSchoolWar[i];

		SNATIVEID nidHallMAP ( sSW.m_dwSchoolWarHallMap );
		GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( nidHallMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarMapID = sSW.m_dwID;
			pLandMan->m_bSchoolWarMapHall = true;
		}

		SNATIVEID nidMAP ( sSW.m_dwSchoolWarMap );
		SMAPNODE *pMAPNODE = GLGaeaServer::GetInstance().FindMapNode ( nidMAP );
		if ( pMAPNODE )
		{
			pMAPNODE->bSWZone = TRUE;
		}

		pLandMan = GLGaeaServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarMapID = sSW.m_dwID;
			pLandMan->m_bSchoolWarMap = true;	
		}
	}

	return true;
}

bool GLSchoolWarFieldMan::FrameMove ( float fElaps )
{
	for ( SW_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWar &sSW = m_vecSchoolWar[i];

		if( sSW.IsBattle() )
		{
			sSW.m_fRankingUpdate += fElaps;
			if ( sSW.m_fRankingUpdate > sSW.m_fRankingUpdateTime )
			{
				UpdateRanking( sSW.m_dwID );
				sSW.m_fRankingUpdate = 0.0f;
			}

		}

		if ( sSW.IsBattleEndAward() )
		{
			EndBattleField( sSW.m_dwID );
			SendRewards( sSW.m_dwID );
		}
	}

	return true;
}


bool GLSchoolWarFieldMan::ReadyBattle( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return false;

	pSW->SetBattle( GLSchoolWar::BATTLE_READY );

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:SchoolWar ReadyBattle:ID %d", dwID );
	return true;
}

bool GLSchoolWarFieldMan::BeginBattle ( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return false;

	pSW->SetBattle( GLSchoolWar::BATTLE_START );
	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:SchoolWar BeginBattle:ID %d", dwID );

	return true;
}

bool GLSchoolWarFieldMan::EndBattleAward ( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return false;

	pSW->SetBattle( GLSchoolWar::BATTLE_END_AWARD );

	//send final rankings to players
	UpdateRanking( dwID );

	//decide the winner
	CheckWinner( dwID );

	pSW->m_bAwardStart = true;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:SchoolWar EndBattleAward:ID %d", dwID );

	return true;
}

bool GLSchoolWarFieldMan::EndBattleField ( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return false;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:SchoolWar Reward Ended:ID %d RewardNum:%d", dwID, pSW->m_vecAwardChar.size() );

	pSW->SetBattle( GLSchoolWar::BATTLE_END );
	pSW->FieldBattleEnd();

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:SchoolWar EndBattleField:ID %d", dwID );

	return true;
}

void GLSchoolWarFieldMan::UpdateRanking( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return;

	UpdateRankingPlayer( dwID );
	UpdateRankingSchool( dwID );
	UpdateRankingSelf( dwID );
}

void GLSchoolWarFieldMan::UpdateRankingPlayer( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return;

	GLSchoolWar &sSW = *pSW;

	SCHOOLWAR_RANK_PLAYER_VEC		m_vecNew;
	m_vecNew.reserve( sSW.m_mapScorePlayer.size() );

	SCHOOLWAR_RANK_PLAYER_MAP_ITER	pos = sSW.m_mapScorePlayer.begin();
	SCHOOLWAR_RANK_PLAYER_MAP_ITER	end = sSW.m_mapScorePlayer.end();
	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWAR_RANK_PLAYER&	sRankInfo = pos->second;					
		m_vecNew.push_back( sRankInfo );
	}

	std::sort( m_vecNew.begin(), m_vecNew.end() );	

	int nSize = (int)m_vecNew.size();

	for ( int i = 0; i < nSize; ++i )
	{
		m_vecNew[i].wRanking = i+1;
		m_vecNew[i].nIndex = i;
	}

	for ( int i = nSize-1; i > 0; --i )
	{
		if ( m_vecNew[i] == m_vecNew[i-1] )
		{
			m_vecNew[i-1].wRanking = m_vecNew[i].wRanking;			
		}
	}	

	GLMSG::SNET_SCHOOLWAR_RANKING_PLAYER_UPDATE	NetMsg;

	for ( int i = 0; i < (int)m_vecNew.size(); ++i )
	{
		SCHOOLWAR_RANK_PLAYER_MAP_ITER iter = sSW.m_mapScorePlayer.find( m_vecNew[i].dwCharID );
		if ( iter == sSW.m_mapScorePlayer.end() )	continue;

		SSCHOOLWAR_RANK_PLAYER&	sRankInfo = iter->second;

		sRankInfo.wRanking = m_vecNew[i].wRanking;
		sRankInfo.nIndex = m_vecNew[i].nIndex;

		if ( m_vecNew[i].nIndex >= 0 && m_vecNew[i].nIndex < SW_RANKING_NUM ) 
		{
			SSCHOOLWAR_RANK_PLAYER_CLIENT sRank = sRankInfo;
			NetMsg.ADDRANK( sRank );
		}		
	}

	if ( NetMsg.wRankNum > 0 )	
	{
		GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSW.m_dwSchoolWarMap, &NetMsg );
		GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:Update Ranking Player RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
	}
}

void GLSchoolWarFieldMan::UpdateRankingSchool( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return;

	GLSchoolWar &sSW = *pSW;

	SCHOOLWAR_RANK_SCHOOL_VEC		m_vecNew;
	m_vecNew.reserve( sSW.m_mapScoreSchool.size() );

	SCHOOLWAR_RANK_SCHOOL_MAP_ITER	pos = sSW.m_mapScoreSchool.begin();
	SCHOOLWAR_RANK_SCHOOL_MAP_ITER	end = sSW.m_mapScoreSchool.end();
	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWAR_RANK_SCHOOL&	sRankInfo = pos->second;					
		m_vecNew.push_back( sRankInfo );
	}

	std::sort( m_vecNew.begin(), m_vecNew.end() );	

	int nSize = (int)m_vecNew.size();

	for ( int i = 0; i < nSize; ++i )
	{
		m_vecNew[i].wRanking = i+1;
		m_vecNew[i].nIndex = i;
	}

	for ( int i = nSize-1; i > 0; --i )
	{
		if ( m_vecNew[i] == m_vecNew[i-1] )
		{
			m_vecNew[i-1].wRanking = m_vecNew[i].wRanking;			
		}
	}

	GLMSG::SNET_SCHOOLWAR_RANKING_SCHOOL_UPDATE	NetMsg;

	for ( int i = 0; i < (int)m_vecNew.size(); ++i )
	{
		SCHOOLWAR_RANK_SCHOOL_MAP_ITER iter = sSW.m_mapScoreSchool.find( m_vecNew[i].wSCHOOL );
		if ( iter == sSW.m_mapScoreSchool.end() )	continue;

		SSCHOOLWAR_RANK_SCHOOL&	sRankInfo = iter->second;

		sRankInfo.wRanking = m_vecNew[i].wRanking;
		sRankInfo.nIndex = m_vecNew[i].nIndex;

		if ( m_vecNew[i].nIndex < SW_SCHOOL_RANKING_NUM ) 
		{
			SSCHOOLWAR_RANK_SCHOOL_CLIENT sRank = sRankInfo;
			NetMsg.ADDRANK( sRank );
		}		
	}

	if ( NetMsg.wRankNum > 0 )	
	{
		GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSW.m_dwSchoolWarMap, &NetMsg );
		GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:Update Ranking School RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
	}
}

void GLSchoolWarFieldMan::UpdateRankingSelf( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	return;

	GLSchoolWar &sSW = *pSW;

	GLMSG::SNET_SCHOOLWAR_RANKING_SELF_UPDATE	NetMsgMy;

	SCHOOLWAR_RANK_PLAYER_MAP_ITER	pos = sSW.m_mapScorePlayer.begin();
	SCHOOLWAR_RANK_PLAYER_MAP_ITER	end = sSW.m_mapScorePlayer.end();

	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWAR_RANK_PLAYER&	sRankInfo = pos->second;
		NetMsgMy.sRank.wRanking		= sRankInfo.wRanking;
		NetMsgMy.sRank.wKillNum		= sRankInfo.wKillNum;
		NetMsgMy.sRank.wDeathNum	= sRankInfo.wDeathNum;
		NetMsgMy.sRank.wSCHOOL		= sRankInfo.wSCHOOL;
		NetMsgMy.sRank.bVALID		= TRUE;

		PGLCHAR pCHAR = GLGaeaServer::GetInstance().GetCharID( sRankInfo.dwCharID );
		if ( pCHAR )
		{
			GLGaeaServer::GetInstance().SENDTOCLIENT( pCHAR->m_dwClientID, &NetMsgMy );
		}
	}
}

void GLSchoolWarFieldMan::CheckWinner( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )	
		return;

	if ( !pSW->IsBattleEndAward() )	
		return;

	GLSchoolWar &sSW = *pSW;

	sSW.m_mapScoreWinner.clear();
	sSW.m_wSchoolWinner = MAX_SCHOOL;

	WORD wWINNER = sSW.m_wSchoolWinner;

	//send winner school to agent
	GLMSG::SNETPC_SERVER_SCHOOLWAR_WINNER_AG NetMsgAgentWinner;
	NetMsgAgentWinner.dwID = sSW.m_dwID;
	NetMsgAgentWinner.wSCHOOL = sSW.m_wSchoolWinner;
	GLGaeaServer::GetInstance().SENDTOAGENT ( &NetMsgAgentWinner );

	//create ranking map of winners and validate their ranking order
	SCHOOLWAR_RANK_PLAYER_MAP_ITER	pos = sSW.m_mapScorePlayer.begin();
	SCHOOLWAR_RANK_PLAYER_MAP_ITER	end = sSW.m_mapScorePlayer.end();
	SCHOOLWAR_RANK_PLAYER_VEC		m_vec;

	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWAR_RANK_PLAYER&	sRankInfo = pos->second;
		m_vec.push_back( sRankInfo );
	}

	std::sort( m_vec.begin(), m_vec.end() );

	int nSize = (int)m_vec.size();

	if ( nSize > 0 )
	{
		for ( int i = 0; i < nSize; ++i )
		{
			m_vec[i].wRanking = i+1;
			m_vec[i].nIndex = i;
		}

		for ( int i = nSize-1; i > 0; --i )
		{
			if ( m_vec[i] == m_vec[i-1] )
				m_vec[i-1].wRanking = m_vec[i].wRanking;			
		}

		for ( int i = 0; i < nSize; ++i )
			sSW.AddWinner( m_vec[i] );
	}

	{
		GLMSG::SNET_SCHOOLWAR_WINNER_UPDATE	NetMsg;

		SCHOOLWAR_RANK_PLAYER_MAP_ITER	pos = sSW.m_mapScoreWinner.begin();
		SCHOOLWAR_RANK_PLAYER_MAP_ITER	end = sSW.m_mapScoreWinner.end();

		for ( ; pos != end; pos++ )
		{
			const SSCHOOLWAR_RANK_PLAYER&	sRankInfo = pos->second;

			if ( sRankInfo.nIndex >= 0 && sRankInfo.nIndex < SW_RANKING_NUM ) 
			{
				SSCHOOLWAR_RANK_PLAYER_CLIENT sRank = sRankInfo;
				NetMsg.ADDRANK( sRank );	
			}
		}

		if ( NetMsg.wRankNum > 0 )	
		{
			GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSW.m_dwSchoolWarMap, &NetMsg );
			GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARINFO:CheckWinner RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
		}
	}
}

void GLSchoolWarFieldMan::SendRewards( DWORD dwID )
{
	GLSchoolWar *pSW = FindSchoolWar ( dwID, FALSE );
	if ( !pSW )						return;
	//if ( !pSW->IsBattleEndAward() )	return;
	//if ( !pSW->m_bAwardStart )		return;

	GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( pSW->m_dwSchoolWarMap );
	if ( !pLandMan ) return;

	GLCHARNODE* pCharNode = pLandMan->m_GlobPCList.m_pHead;
	for ( ; pCharNode; pCharNode = pCharNode->pNext )
	{
		PGLCHAR pChar = GLGaeaServer::GetInstance().GetChar( pCharNode->Data->m_dwGaeaID );
		if ( !pChar ) continue;

		SCHOOLWAR_AWARD_CHAR_ITER iterRewarded = pSW->m_vecAwardChar.find( pChar->m_dwCharID );
		if ( iterRewarded != pSW->m_vecAwardChar.end() )	continue;

		SCHOOLWAR_RANK_PLAYER_MAP_ITER iterWinner = pSW->m_mapScoreWinner.find( pChar->m_dwCharID );
		if ( iterWinner != pSW->m_mapScoreWinner.end() ) 
		{
			SSCHOOLWAR_RANK_PLAYER& sRankInfo = iterWinner->second;

			int nAwardIndex;
			if ( sRankInfo.wRanking < SW_REWARD_NUM ) nAwardIndex = sRankInfo.wRanking - 1;
			else if ( sRankInfo.wRanking <= pSW->m_sAwardItem.dwAwardLimit && sRankInfo.wKillNum >= pSW->m_sAwardItem.dwKillMin ) nAwardIndex = ( SW_REWARD_NUM -1 );
			else continue;


			SNATIVEID sNativeID = pSW->m_sAwardItem.nAwardItem[nAwardIndex];

			SITEM* pItem = GLItemMan::GetInstance().GetItem( sNativeID );
			if ( !pItem ) continue;

			SITEMCUSTOM sITEM_NEW;
			sITEM_NEW.sNativeID = sNativeID;
			sITEM_NEW.tBORNTIME = CTime::GetCurrentTime().GetTime();
			sITEM_NEW.cGenType = EMGEN_SYSTEM;
			sITEM_NEW.cChnID = (BYTE)GLGaeaServer::GetInstance().GetServerChannel();
			sITEM_NEW.cFieldID = (BYTE)GLGaeaServer::GetInstance().GetFieldSvrID();
			sITEM_NEW.lnGenNum = GLITEMLMT::GetInstance().RegItemGen ( sITEM_NEW.sNativeID, (EMITEMGEN)sITEM_NEW.cGenType );

			CItemDrop cDropItem;
			cDropItem.sItemCustom = sITEM_NEW;
			if ( pChar->IsInsertToInvenEx ( &cDropItem ) )
			{
				pChar->InsertToInvenEx ( &cDropItem );

				GLITEMLMT::GetInstance().ReqItemRoute ( sITEM_NEW, ID_CLUB, pChar->m_dwGuild, ID_CHAR, pChar->m_dwCharID, 
					EMITEM_ROUTE_SYSTEM, sITEM_NEW.wTurnNum );
			}else{
				pLandMan->DropItem ( pChar->m_vPos, &(cDropItem.sItemCustom), EMGROUP_ONE, pChar->m_dwGaeaID );
			}

			pSW->m_vecAwardChar.insert( pChar->m_dwCharID );
		}
	}
}