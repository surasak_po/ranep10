#pragma once

#include "GLSchoolWarData.h"

class GLSchoolWar
{
public:
	enum 
	{		
		MAX_TIME = 80,
	};

	enum EM_BATTLE_STATE 
	{
		BATTLE_NOSTART		= 0,
		BATTLE_READY		= 1,
		BATTLE_START		= 2,
		BATTLE_END_AWARD	= 3,
		BATTLE_END			= 4,
	};

public:
	//common var
	DWORD					m_dwID;	
	std::string				m_strName;
	EM_BATTLE_STATE			m_emBattleState;
	DWORD					m_dwSchoolWarMap;
	DWORD					m_dwSchoolWarMapGate;
	DWORD					m_dwSchoolWarHallMap;
	DWORD					m_dwSchoolWarHallGate;
	float					m_fCHECK_TIMER;
	float					m_fCHECK_TIMER_MAX;
	WORD					m_wSchoolWinner;
	int						m_nCTFWinner;

	//agent server
	SSW_TIME				m_sSWTIME[MAX_TIME];
	DWORD					m_dwLastBattleDay;
	DWORD					m_dwBattleOrder;
	bool					m_bNotifyOneHour;
	bool					m_bNotifyHalfHour;
	bool					m_bNotify10MinHour;
	DWORD					m_dwBattleTime;
	DWORD					m_dwBattleTimeNext;
	DWORD					m_dwBattleTimePrevious;
	DWORD					m_dwNextTime;
	float					m_fBattleTimer;
	float					m_fBattleTimerHold;

	//field server
	SCHOOLWAR_RANK_PLAYER_MAP		m_mapScorePlayer;
	SCHOOLWAR_RANK_SCHOOL_MAP		m_mapScoreSchool;
	SCHOOLWAR_RANK_PLAYER_MAP		m_mapScoreWinner;
	float							m_fRankingUpdate;
	float							m_fRankingUpdateTime;
	SSCHOOLWAR_AWARD_ITEM			m_sAwardItem;
	SCHOOLWAR_AWARD_CHAR			m_vecAwardChar;
	bool							m_bAwardStart;
	
public:
	GLSchoolWar ();

	GLSchoolWar ( const GLSchoolWar &value )
	{
		operator= ( value );
	}

	GLSchoolWar& operator= ( const GLSchoolWar& value );

	bool operator < ( const GLSchoolWar& sSchoolWar )
	{
		return m_dwID < sSchoolWar.m_dwID;
	}

public:
	bool Load ( std::string strFile );


public:
	//common
	void SetBattle( EM_BATTLE_STATE emBattleState )	{ m_emBattleState = emBattleState; }
	bool IsBattle ()		{ return ( m_emBattleState == BATTLE_START ); }
	bool IsBattleReady()	{ return ( m_emBattleState == BATTLE_READY ); }
	bool IsBattleEndAward() { return ( m_emBattleState == BATTLE_END_AWARD); }
	bool IsBattleEnd()		{ return ( m_emBattleState == BATTLE_END); }

	bool IsEnterMap( WORD wSCHOOL );
	BOOL CheckAwardTime( float fElaps );

	void SetCTFWinner(int nSchool);

	//agent server 
	DWORD IsBattleWeekDay ( int nDayOfWeek, int nDay );
	bool IsBattleHour ( DWORD dwORDER, int nHour, int nMinute );
	void UpdateNotifyBattle ( DWORD dwORDER, int nHour, int nMinute );
	void NotifyBattleBroadcast( int nTIME, BOOL bREADY );
	void DoBattleStart ( DWORD dwORDER, int nDay );
	void DoBattleEndAward();
	void DoBattleEnd();

	//field server
	void FieldBattleEnd();
	SSCHOOLWAR_RANK_PLAYER* GetRankingPlayer( DWORD dwCharID );
	SSCHOOLWAR_RANK_SCHOOL*	GetRankingSchool( WORD wSchool );
	void AddScore( DWORD dwKillChar, DWORD dwDeathChar );
	void AddWinner( SSCHOOLWAR_RANK_PLAYER sRANK );


public:
};
