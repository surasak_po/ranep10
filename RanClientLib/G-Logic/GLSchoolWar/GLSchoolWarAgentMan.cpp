#include "StdAfx.h"
#include "GLSchoolWarMan.h"
#include "GLAgentServer.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//agent server
GLSchoolWarAgentMan& GLSchoolWarAgentMan::GetInstance()
{
	static GLSchoolWarAgentMan cInstance;
	return cInstance;
}

bool GLSchoolWarAgentMan::SetMapState ()
{
	for ( SW_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWar &sSW= m_vecSchoolWar[i];

		SNATIVEID nidHallMAP ( sSW.m_dwSchoolWarHallMap );
		GLAGLandMan *pLandMan = GLAgentServer::GetInstance().GetByMapID ( nidHallMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarMapID = sSW.m_dwID;
		}

		SNATIVEID nidMAP ( sSW.m_dwSchoolWarMap );
		pLandMan = GLAgentServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarMapID = sSW.m_dwID;
			pLandMan->m_bSchoolWarMap = true;
		}
	}

	return true;
}

bool GLSchoolWarAgentMan::FrameMove ( float fElapsedAppTime )
{
	if ( GLCONST_CHAR::bPKLESS ) return false;

	for ( SW_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWar &sSchoolWar = m_vecSchoolWar[i];

		CTime cCurTime = CTime::GetCurrentTime();
		int nDayOfWeek = cCurTime.GetDayOfWeek ();
		int nDay = cCurTime.GetDay ();
		int nHour = cCurTime.GetHour();
		int nMinute = cCurTime.GetMinute ();
       //battlefieldui data - Eduj
	   /*if ( m_bNextWar )
		{
		   m_fTime += fElapsedAppTime;
		   if ( m_fTime < 1.0f ) m_fNextBattleTime = float(sSchoolWar.m_dwNextTime+1)-m_fPreviousBattleTime;
		   sSchoolWar.IsBattleWeekDay ( nDayOfWeek, nHour );
		   m_fNextBattleTime -= fElapsedAppTime;
		   GLMSG::SNET_SCHOOLWAR_NEXTWAR_BRD NetMsgBrd;
		   if ( !m_bBattleEnd ) NetMsgBrd.fProgressTime = 0.0f;
		   if ( m_fTime == 0.0f  ) NetMsgBrd.fProgressTime = m_fNextBattleTime;
           NetMsgBrd.fNextStartTime = sSchoolWar.m_dwBattleTimeNext;
		   NetMsgBrd.fNextEndTime = sSchoolWar.m_dwBattleTime;
		   if ( m_bBattleEnd )
		   {
             NetMsgBrd.fProgressTime = m_fNextBattleTime;
			 NetMsgBrd.fPreviousStartTime = sSchoolWar.m_dwBattleTimePrevious;
             NetMsgBrd.fPreviousEndTime = m_fPreviousBattleTime;
		   }
		   GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
		}*/
		//
		DWORD dwORDER = sSchoolWar.IsBattleWeekDay(nDayOfWeek, nHour );
		if ( dwORDER!=UINT_MAX )
		{
			if ( sSchoolWar.IsBattleHour ( dwORDER, nHour, nMinute ) )
			{
				GLAgentServer::GetInstance().m_nCTFWINNER = -1;
				sSchoolWar.DoBattleStart ( dwORDER, nHour );
				m_fRemainTimer = (float)sSchoolWar.m_dwBattleTime;
				m_bBattleEnd = false;
				m_fNextBattleTime = 0.0f;
				m_fPreviousBattleTime = 0.0f;
				m_fTimer = 0.0f;
				m_fTime = 0.0f;

			}
			else
			{
				sSchoolWar.UpdateNotifyBattle ( dwORDER, nHour, nMinute );
			}
		}

		if ( sSchoolWar.IsBattle() )
		{
			sSchoolWar.m_fBattleTimer += fElapsedAppTime;
			if (!m_bBattleEnd ) m_fPreviousBattleTime += fElapsedAppTime;
			
			if ( sSchoolWar.m_fBattleTimer > float(sSchoolWar.m_dwBattleTime)  )	//On progress
			{            
				  sSchoolWar.DoBattleEndAward();
				  m_bBattleEnd = true;		  
			}
			if ( GLAgentServer::GetInstance().GetWinner() != -1  )
			{
                 sSchoolWar.DoBattleEndAward();
			     m_bBattleEnd = true;
		    }
		}
		  
      

		if ( sSchoolWar.IsBattleEndAward() )
		{
			BOOL bENDED = sSchoolWar.CheckAwardTime( fElapsedAppTime );
			if ( bENDED )
			{
			 sSchoolWar.DoBattleEnd();					
			}
		}
	}

	if ( !m_vecSchoolWar.empty() )
	{   
		//m_bNextWar = true;
		if ( m_vecSchoolWar[0].IsBattle() )
		{

			GLMSG::SNET_SCHOOLWAR_REMAIN_BRD NetMsgBrd;

			if ( m_fRemainTimer == m_vecSchoolWar[0].m_dwBattleTime )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
			}

			m_fTimer += fElapsedAppTime;
			m_fRemainTimer -= fElapsedAppTime;

			if ( m_fTimer > 600.0f )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
				m_fTimer = 0.0f;
			}
		}
	}

	return true;
}