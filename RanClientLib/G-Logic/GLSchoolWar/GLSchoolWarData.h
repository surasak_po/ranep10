//2-4-2015
//devlordx
//schoolwar data structures

#pragma once

#include <vector>
#include <set>
#include "GLDefine.h"

#define SW_RANKING_NUM				20 //top 1 to 20 since we dont have top 0
#define SW_SCHOOL_RANKING_NUM		3
#define SW_RANKING_NAME				33
#define SW_REWARD_NUM				21 // ( SW_RANKING_NUM + 1 ) reward from top 1 ~ 20  and etch 

struct SSW_TIME
{
	DWORD dwWeekDay;
	DWORD dwStartTime;
	DWORD dwStartMin;

	SSW_TIME () :
	dwWeekDay(0),
		dwStartTime(0),
		dwStartMin(0)
	{
	}
};

struct SSCHOOLWAR_RANK_PLAYER
{
	int		nIndex;   
	DWORD	dwCharID;
	WORD	wSCHOOL;
	WORD	wRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	char	szCharName[SW_RANKING_NAME+1];	

	SSCHOOLWAR_RANK_PLAYER()
		: nIndex ( -1 )
		, dwCharID( 0 )
		, wSCHOOL(0)
		, wRanking(0)
		, wKillNum(0)
		, wDeathNum(0)
	{
		memset( szCharName, 0, sizeof(char) * (SW_RANKING_NAME+1) );
	}

	SSCHOOLWAR_RANK_PLAYER ( const SSCHOOLWAR_RANK_PLAYER &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_PLAYER& operator = ( const SSCHOOLWAR_RANK_PLAYER& rvalue )
	{
		nIndex		= rvalue.nIndex;
		dwCharID	= rvalue.dwCharID;
		wSCHOOL		= rvalue.wSCHOOL;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;
		StringCchCopy( szCharName, SW_RANKING_NAME+1, rvalue.szCharName );

		return *this;
	}

	bool operator < ( const SSCHOOLWAR_RANK_PLAYER& sRank )
	{			
		if ( wKillNum > sRank.wKillNum ) return true;
		else if ( wKillNum == sRank.wKillNum && wDeathNum < sRank.wDeathNum ) return true;
		return  false;
	}

	bool operator == ( const SSCHOOLWAR_RANK_PLAYER& sRank ) 
	{
		if ( wKillNum == sRank.wKillNum && wDeathNum == sRank.wDeathNum ) return true;
		return false;

	}
};

struct SSCHOOLWAR_RANK_PLAYER_CLIENT
{
	int		nIndex;
	DWORD	dwCharID;
	WORD	wRanking;
	WORD	wSCHOOL;
	WORD	wKillNum;
	WORD	wDeathNum;
	char	szCharName[SW_RANKING_NAME+1];	

	SSCHOOLWAR_RANK_PLAYER_CLIENT() 
		: nIndex(-1)
		, wRanking (0)
		, wKillNum(0)
		, wDeathNum(0)
		, wSCHOOL(-1)
	{
		memset( szCharName, 0, sizeof(char) * (SW_RANKING_NAME+1) );
	}

	void Init()
	{
		nIndex		= -1;
		wRanking	= 0;
		wKillNum	= 0;
		wDeathNum	= 0;
		wSCHOOL		= -1;
	}

	SSCHOOLWAR_RANK_PLAYER_CLIENT ( const SSCHOOLWAR_RANK_PLAYER_CLIENT &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_PLAYER_CLIENT& operator = ( const SSCHOOLWAR_RANK_PLAYER_CLIENT& rvalue )
	{
		nIndex		= rvalue.nIndex;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;
		wSCHOOL		= rvalue.wSCHOOL;
		dwCharID	= rvalue.dwCharID;

		StringCchCopy( szCharName, SW_RANKING_NAME+1, rvalue.szCharName );

		return *this;
	}

	SSCHOOLWAR_RANK_PLAYER_CLIENT ( const SSCHOOLWAR_RANK_PLAYER &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_PLAYER_CLIENT& operator = ( const SSCHOOLWAR_RANK_PLAYER& rvalue )
	{
		nIndex		= rvalue.nIndex;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;
		wSCHOOL		= rvalue.wSCHOOL;
		dwCharID	= rvalue.dwCharID;

		StringCchCopy( szCharName, SW_RANKING_NAME+1, rvalue.szCharName );

		return *this;
	}
};

struct SSCHOOLWAR_RANK_SCHOOL
{ 
	int		nIndex;
	WORD	wRanking;
	WORD	wSCHOOL;
	WORD	wKillNum;
	WORD	wDeathNum;

	SSCHOOLWAR_RANK_SCHOOL()
		:  nIndex(-1)
		, wSCHOOL(0)
		, wRanking(0)
		, wKillNum(0)
		, wDeathNum(0)
	{
	}

	SSCHOOLWAR_RANK_SCHOOL ( const SSCHOOLWAR_RANK_SCHOOL &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_SCHOOL& operator = ( const SSCHOOLWAR_RANK_SCHOOL& rvalue )
	{
		nIndex		= rvalue.nIndex;
		wSCHOOL		= rvalue.wSCHOOL;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;

		return *this;
	}

	bool operator < ( const SSCHOOLWAR_RANK_SCHOOL& sRank )
	{			
		if ( wKillNum > sRank.wKillNum ) return true;
		else if ( wKillNum == sRank.wKillNum && wDeathNum < sRank.wDeathNum ) return true;
		return  false;
	}

	bool operator == ( const SSCHOOLWAR_RANK_SCHOOL& sRank ) 
	{
		if ( wKillNum == sRank.wKillNum && wDeathNum == sRank.wDeathNum ) return true;
		return false;
	}
};

struct SSCHOOLWAR_RANK_SCHOOL_CLIENT
{
	int		nIndex;
	WORD	wSCHOOL;
	WORD	wRanking;
	WORD	wKillNum;
	WORD	wDeathNum;

	SSCHOOLWAR_RANK_SCHOOL_CLIENT() 
		: nIndex(-1)
		, wSCHOOL(0)
		, wRanking (0)
		, wKillNum(0)
		, wDeathNum(0)
	{
	}

	SSCHOOLWAR_RANK_SCHOOL_CLIENT ( const SSCHOOLWAR_RANK_SCHOOL_CLIENT &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_SCHOOL_CLIENT& operator = ( const SSCHOOLWAR_RANK_SCHOOL_CLIENT& rvalue )
	{
		nIndex		= rvalue.nIndex;
		wSCHOOL		= rvalue.wSCHOOL;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;
		
		return *this;
	}

	SSCHOOLWAR_RANK_SCHOOL_CLIENT ( const SSCHOOLWAR_RANK_SCHOOL &value )
	{
		operator=(value);
	}

	SSCHOOLWAR_RANK_SCHOOL_CLIENT& operator = ( const SSCHOOLWAR_RANK_SCHOOL& rvalue )
	{
		nIndex		= rvalue.nIndex;
		wSCHOOL		= rvalue.wSCHOOL;
		wRanking	= rvalue.wRanking;
		wKillNum	= rvalue.wKillNum;
		wDeathNum	= rvalue.wDeathNum;
	
		return *this;
	}
};

struct SSCHOOLWAR_RANK_SELF
{
	WORD	wRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wSCHOOL;
	BOOL	bVALID;

	SSCHOOLWAR_RANK_SELF()
		: wRanking(0)
		, wKillNum (0)
		, wDeathNum(0)
		, wSCHOOL(0)
		, bVALID( FALSE )
	{
	}

	void Init()
	{
		wRanking	= 0;
		wKillNum	= 0;
		wDeathNum	= 0;
		wSCHOOL		= 0;
		bVALID		= FALSE;
	}
};

struct SSCHOOLWAR_RANK_SCHOOL_WINNER
{
	WORD	wRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wSCHOOL;
	BOOL	bVALID;

	SSCHOOLWAR_RANK_SCHOOL_WINNER()
		: wRanking(0)
		, wKillNum (0)
		, wDeathNum(0)
		, wSCHOOL(-1)
		, bVALID( FALSE )
	{
	}

	void Init()
	{
		wRanking	= 0;
		wKillNum	= 0;
		wDeathNum	= 0;
		wSCHOOL		= -1;
		bVALID = FALSE;
	}
};

typedef std::map< DWORD, SSCHOOLWAR_RANK_PLAYER >	SCHOOLWAR_RANK_PLAYER_MAP;
typedef SCHOOLWAR_RANK_PLAYER_MAP::iterator			SCHOOLWAR_RANK_PLAYER_MAP_ITER;

typedef std::vector<SSCHOOLWAR_RANK_PLAYER>			SCHOOLWAR_RANK_PLAYER_VEC;
typedef SCHOOLWAR_RANK_PLAYER_VEC::iterator			SCHOOLWAR_RANK_PLAYER_VEC_ITER;

typedef std::vector<SSCHOOLWAR_RANK_PLAYER_CLIENT>	SCHOOLWAR_RANK_PLAYER_CLIENT_VEC;
typedef SCHOOLWAR_RANK_PLAYER_CLIENT_VEC::iterator	SCHOOLWAR_RANK_PLAYER_CLIENT_VEC_ITER;

typedef std::map< WORD, SSCHOOLWAR_RANK_SCHOOL >	SCHOOLWAR_RANK_SCHOOL_MAP;
typedef SCHOOLWAR_RANK_SCHOOL_MAP::iterator			SCHOOLWAR_RANK_SCHOOL_MAP_ITER;

typedef std::vector<SSCHOOLWAR_RANK_SCHOOL>			SCHOOLWAR_RANK_SCHOOL_VEC;
typedef SCHOOLWAR_RANK_SCHOOL_VEC::iterator			SCHOOLWAR_RANK_SCHOOL_VEC_ITER;

typedef std::vector<SSCHOOLWAR_RANK_SCHOOL_CLIENT>	SCHOOLWAR_RANK_SCHOOL_CLIENT_VEC;
typedef SCHOOLWAR_RANK_SCHOOL_CLIENT_VEC::iterator	SCHOOLWAR_RANK_SCHOOL_CLIENT_VEC_ITER;

typedef std::set<DWORD>								SCHOOLWAR_AWARD_CHAR;
typedef SCHOOLWAR_AWARD_CHAR::iterator				SCHOOLWAR_AWARD_CHAR_ITER;


struct SSCHOOLWAR_AWARD_ITEM
{
	SNATIVEID	nAwardItem[SW_REWARD_NUM];
	DWORD		dwAwardLimit;
	DWORD		dwKillMin;

	SSCHOOLWAR_AWARD_ITEM()
		: dwAwardLimit(SW_REWARD_NUM)
		, dwKillMin( 0 )
	{	
		memset( nAwardItem, -1, sizeof( SNATIVEID ) * SW_REWARD_NUM );
	}
};

/*
                       8888  8888888
                  888888888888888888888888
               8888:::8888888888888888888888888
             8888::::::8888888888888888888888888888
            88::::::::888:::8888888888888888888888888
          88888888::::8:::::::::::88888888888888888888
        888 8::888888::::::::::::::::::88888888888   888
           88::::88888888::::m::::::::::88888888888    8
         888888888888888888:M:::::::::::8888888888888
        88888888888888888888::::::::::::M88888888888888
        8888888888888888888888:::::::::M8888888888888888
         8888888888888888888888:::::::M888888888888888888
        8888888888888888::88888::::::M88888888888888888888
      88888888888888888:::88888:::::M888888888888888   8888
     88888888888888888:::88888::::M::;o*M*o;888888888    88
    88888888888888888:::8888:::::M:::::::::::88888888    8
   88888888888888888::::88::::::M:;:::::::::::888888888     
  8888888888888888888:::8::::::M::aAa::::::::M8888888888       8
  88   8888888888::88::::8::::M:::::::::::::888888888888888 8888
 88  88888888888:::8:::::::::M::::::::::;::88:88888888888888888
 8  8888888888888:::::::::::M::"@@@@@@@"::::8w8888888888888888
  88888888888:888::::::::::M:::::"@a@":::::M8i888888888888888
 8888888888::::88:::::::::M88:::::::::::::M88z88888888888888888 
8888888888:::::8:::::::::M88888:::::::::MM888!888888888888888888
888888888:::::8:::::::::M8888888MAmmmAMVMM888*88888888   88888888
888888 M:::::::::::::::M888888888:::::::MM88888888888888   8888888
8888   M::::::::::::::M88888888888::::::MM888888888888888    88888
 888   M:::::::::::::M8888888888888M:::::mM888888888888888    8888
  888  M::::::::::::M8888:888888888888::::m::Mm88888 888888   8888
   88  M::::::::::::8888:88888888888888888::::::Mm8   88888   888
   88  M::::::::::8888M::88888::888888888888:::::::Mm88888    88
   8   MM::::::::8888M:::8888:::::888888888888::::::::Mm8     4
       8M:::::::8888M:::::888:::::::88:::8888888::::::::Mm    2
      88MM:::::8888M:::::::88::::::::8:::::888888:::M:::::M
     8888M:::::888MM::::::::8:::::::::::M::::8888::::M::::M
    88888M:::::88:M::::::::::8:::::::::::M:::8888::::::M::M
   88 888MM:::888:M:::::::::::::::::::::::M:8888:::::::::M:
   8 88888M:::88::M:::::::::::::::::::::::MM:88::::::::::::M
     88888M:::88::M::::::::::*88*::::::::::M:88::::::::::::::M             
    888888M:::88::M:::::::::88@@88:::::::::M::88::::::::::::::M
    888888MM::88::MM::::::::88@@88:::::::::M:::8::::::::::::::*8
    88888  M:::8::MM:::::::::*88*::::::::::M:::::::::::::::::88@@
    8888   MM::::::MM:::::::::::::::::::::MM:::::::::::::::::88@@
     888    M:::::::MM:::::::::::::::::::MM::M::::::::::::::::*8
     888    MM:::::::MMM::::::::::::::::MM:::MM:::::::::::::::M
      88     M::::::::MMMM:::::::::::MMMM:::::MM::::::::::::MM
       88    MM:::::::::MMMMMMMMMMMMMMM::::::::MMM::::::::MMM
        88    MM::::::::::::MMMMMMM::::::::::::::MMMMMMMMMM
         88   8MM::::::::::::::::::::::::::::::::::MMMMMM
          8   88MM::::::::::::::::::::::M:::M::::::::MM
              888MM::::::::::::::::::MM::::::MM::::::MM
             88888MM:::::::::::::::MMM:::::::mM:::::MM
             888888MM:::::::::::::MMM:::::::::MMM:::M
            88888888MM:::::::::::MMM:::::::::::MM:::M
           88 8888888M:::::::::MMM::::::::::::::M:::M
           8  888888 M:::::::MM:::::::::::::::::M:::M:
              888888 M::::::M:::::::::::::::::::M:::MM
             888888  M:::::M::::::::::::::::::::::::M:M
             888888  M:::::M:::::::::@::::::::::::::M::M
             88888   M::::::::::::::@@:::::::::::::::M::M
            88888   M::::::::::::::@@@::::::::::::::::M::M
           88888   M:::::::::::::::@@::::::::::::::::::M::M
          88888   M:::::m::::::::::@::::::::::Mm:::::::M:::M
          8888   M:::::M:::::::::::::::::::::::MM:::::::M:::M
         8888   M:::::M:::::::::::::::::::::::MMM::::::::M:::M
        888    M:::::Mm::::::::::::::::::::::MMM:::::::::M::::M
      8888    MM::::Mm:::::::::::::::::::::MMMM:::::::::m::m:::M
     888      M:::::M::::::::::::::::::::MMM::::::::::::M::mm:::M
  8888       MM:::::::::::::::::::::::::MM:::::::::::::mM::MM:::M:
             M:::::::::::::::::::::::::M:::::::::::::::mM::MM:::Mm
            MM::::::m:::::::::::::::::::::::::::::::::::M::MM:::MM
            M::::::::M:::::::::::::::::::::::::::::::::::M::M:::MM         
           MM:::::::::M:::::::::::::M:::::::::::::::::::::M:M:::MM
           M:::::::::::M88:::::::::M:::::::::::::::::::::::MM::MMM
           M::::::::::::8888888888M::::::::::::::::::::::::MM::MM
           M:::::::::::::88888888M:::::::::::::::::::::::::M::MM
           M::::::::::::::888888M:::::::::::::::::::::::::M::MM
           M:::::::::::::::88888M:::::::::::::::::::::::::M:MM
           M:::::::::::::::::88M::::::::::::::::::::::::::MMM
           M:::::::::::::::::::M::::::::::::::::::::::::::MMM
           MM:::::::::::::::::M::::::::::::::::::::::::::MMM
            M:::::::::::::::::M::::::::::::::::::::::::::MMM
            MM:::::::::::::::M::::::::::::::::::::::::::MMM
             M:::::::::::::::M:::::::::::::::::::::::::MMM
             MM:::::::::::::M:::::::::::::::::::::::::MMM
              M:::::::::::::M::::::::::::::::::::::::MMM
              MM:::::::::::M::::::::::::::::::::::::MMM
               M:::::::::::M:::::::::::::::::::::::MMM  
               MM:::::::::M:::::::::::::::::::::::MMM
                M:::::::::M::::::::::::::::::::::MMM
                MM:::::::M::::::::::::::::::::::MMM
                 MM::::::M:::::::::::::::::::::MMM
                 MM:::::M:::::::::::::::::::::MMM
                  MM::::M::::::::::::::::::::MMM
                  MM:::M::::::::::::::::::::MMM
                   MM::M:::::::::::::::::::MMM
                   MM:M:::::::::::::::::::MMM
                    MMM::::::::::::::::::MMM
                    MM::::::::::::::::::MMM
                     M:::::::::::::::::MMM
                    MM::::::::::::::::MMM
                    MM:::::::::::::::MMM
                    MM::::M:::::::::MMM:
                    mMM::::MM:::::::MMMM
                     MMM:::::::::::MMM:M
                     mMM:::M:::::::M:M:M
                      MM::MMMM:::::::M:M
                      MM::MMM::::::::M:M
                      mMM::MM::::::::M:M
                       MM::MM:::::::::M:M
                       MM::MM::::::::::M:m
                       MM:::M:::::::::::MM
                       MMM:::::::::::::::M:
                       MMM:::::::::::::::M:
                       MMM::::::::::::::::M
                       MMM::::::::::::::::M
                       MMM::::::::::::::::Mm
                        MM::::::::::::::::MM
                        MMM:::::::::::::::MM
                        MMM:::::::::::::::MM
                        MMM:::::::::::::::MM
                        MMM:::::::::::::::MM
                         MM::::::::::::::MMM
                         MMM:::::::::::::MM
                         MMM:::::::::::::MM
                         MMM::::::::::::MM
                          MM::::::::::::MM
                          MM::::::::::::MM
                          MM:::::::::::MM
                          MMM::::::::::MM
                          MMM::::::::::MM
                           MM:::::::::MM
                           MMM::::::::MM
                           MMM::::::::MM
                            MM::::::::MM
                            MMM::::::MM
                            MMM::::::MM
                             MM::::::MM
                             MM::::::MM
                              MM:::::MM
                              MM:::::MM:
                              MM:::::M:M
                              MM:::::M:M
                              :M::::::M:
                             M:M:::::::M
                            M:::M::::::M
                           M::::M::::::M
                          M:::::M:::::::M
                         M::::::MM:::::::M
                         M:::::::M::::::::M
                         M;:;::::M:::::::::M
                         M:m:;:::M::::::::::M
                         MM:m:m::M::::::::;:M
                          MM:m::MM:::::::;:;M
                           MM::MMM::::::;:m:M
                            MMMM MM::::m:m:MM
                                  MM::::m:MM
                                   MM::::MM
                                    MM::MM

*/