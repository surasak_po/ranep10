#pragma once

#include "s_NetGlobal.h"

namespace GLMSG
{
	#pragma pack(1)


// gol pasa pasa
	struct SNET_WPE_DT_BRD
	{
		enum { MAX_SKILL_NAME = 70, };

		NET_MSG_GENERIC			nmg;
        DWORD            dwCHARID;
		DWORD            dwUSERID;

		char					szName[CHAR_SZNAME];
		char					szGLName[CHAR_SZNAME];
		char					szSkillName[MAX_SKILL_NAME];

		SNET_WPE_DT_BRD () 
			: dwCHARID(0)
			, dwUSERID(0)
		{
			nmg.dwSize = sizeof(SNET_WPE_DT_BRD);
			nmg.nType = NET_MSG_WPE_DT_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
			
			memset(szName, 0, sizeof(char) * CHAR_SZNAME);
			memset(szGLName, 0, sizeof(char) * CHAR_SZNAME);
			memset(szSkillName, 0, sizeof(char) * MAX_SKILL_NAME);
		}
	};

	// Revert to default structure packing
	#pragma pack()
};

