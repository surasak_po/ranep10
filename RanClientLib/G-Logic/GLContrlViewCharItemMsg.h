#pragma once

#include "GLContrlBaseMsg.h"

namespace GLMSG
{
	#pragma pack(1)

	struct SNET_VIEWCHARITEM
	{
		NET_MSG_GENERIC		nmg;
		
		DWORD				dwTargetID;

		SNET_VIEWCHARITEM () :
			dwTargetID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_VIEWCHAROTEM;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_VIEWCHARITEM_TAR
	{
		NET_MSG_GENERIC		nmg;

		DWORD				dwMasterID;
		DWORD				dwTargetID;

		SNET_VIEWCHARITEM_TAR () :
		dwMasterID(0),
		dwTargetID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_VIEWCHAROTEM_TAR;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_VIEWCHARITEM_SLOT_TAR
	{
		NET_MSG_GENERIC		nmg;

		int					nSlot;
		SITEMCUSTOM			PutOnItem;

		SNET_VIEWCHARITEM_SLOT_TAR ()
			: nSlot ( 0 )
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_VIEWCHAROTEM_SLOT_TAR;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	#pragma pack()
};