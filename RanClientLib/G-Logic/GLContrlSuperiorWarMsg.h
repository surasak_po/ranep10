#pragma once

#include "GLContrlBaseMsg.h"

namespace GLMSG
{
	#pragma pack(1)

	struct SNETPC_SERVER_SUPERIORWAR_INFO
	{
		NET_MSG_GENERIC		nmg;
		bool				bWar;

		SNETPC_SERVER_SUPERIORWAR_INFO () :
		bWar(false)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_SUPERIORWAR_INFO;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};


	struct SNET_SUPERIORWAR_START_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SUPERIORWAR_START_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_START_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SUPERIORWAR_START_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		int						nTIME;
		char					szName[TEXT_LEN];

		SNET_SUPERIORWAR_START_BRD () 
			: nTIME(0)
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_START_BRD);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_START_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SUPERIORWAR_READY_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SUPERIORWAR_READY_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_READY_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SUPERIORWAR_READY_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];

		SNET_SUPERIORWAR_READY_BRD () /*:*/ 
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_READY_BRD);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_READY_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SUPERIORWAR_REMAIN_BRD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwTime;

		SNET_SUPERIORWAR_REMAIN_BRD ()
			: dwTime(0)
		{
			nmg.dwSize = sizeof ( SNET_SUPERIORWAR_REMAIN_BRD );
			nmg.nType  = NET_MSG_GCTRL_SUPERIORWAR_REMAIN_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNETPC_SERVER_SUPERIORWAR_REMAIN_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwGaeaID;

		SNETPC_SERVER_SUPERIORWAR_REMAIN_AG ()
			: dwGaeaID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_SERVER_SUPERIORWAR_REMAIN_AG;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SUPERIORWAR_END_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SUPERIORWAR_END_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_END_FLD);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_END_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SUPERIORWAR_END_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];
		bool					bREWARD;

		SNET_SUPERIORWAR_END_BRD () :
			bREWARD( false )
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_END_BRD);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_END_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SUPERIORWAR_RANKING_PLAYER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSUPERIORWAR_RANK_PLAYER_CLIENT	sRank[SPW_RANKING_NUM];

		SNET_SUPERIORWAR_RANKING_PLAYER_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_RANKING_PLAYER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSUPERIORWAR_RANK_PLAYER_CLIENT& sRANK )
		{
			if ( SPW_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSUPERIORWAR_RANK_PLAYER_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};


	struct SNET_SUPERIORWAR_RANKING_SELF_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		SSUPERIORWAR_RANK_SELF	sRank;		

		SNET_SUPERIORWAR_RANKING_SELF_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_RANKING_SELF_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_RANKING_SELF_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SUPERIORWAR_POINT_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		WORD	wCURRENTSCORE;
		DWORD	dwCHARID;

		SNET_SUPERIORWAR_POINT_UPDATE () 
			: wCURRENTSCORE(0)	
			, dwCHARID( 0 )
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_POINT_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_POINT_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SUPERIORWAR_RANKING_REQ
	{
		NET_MSG_GENERIC		nmg;    
		DWORD				dwMapID;		

		SNET_SUPERIORWAR_RANKING_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SUPERIORWAR_RANKING_REQ);
			nmg.nType = NET_MSG_GCTRL_SUPERIORWAR_RANKING_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};


	


	// Revert to default structure packing
	#pragma pack()
};