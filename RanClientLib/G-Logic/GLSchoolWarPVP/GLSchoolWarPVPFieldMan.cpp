#include "StdAfx.h"
#include "GLSchoolWarPVPMan.h"
#include "GLGaeaServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//field server
GLSchoolWarPVPFieldMan& GLSchoolWarPVPFieldMan::GetInstance()
{
	static GLSchoolWarPVPFieldMan cInstance;
	return cInstance;
}

bool GLSchoolWarPVPFieldMan::SetMapState ()
{
	for ( SWPVP_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWarPVP &sSWPVP = m_vecSchoolWar[i];

		SNATIVEID nidHallMAP ( sSWPVP.m_dwSchoolWarHallMap );
		GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( nidHallMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarPVPMapID = sSWPVP.m_dwID;
			pLandMan->m_bSchoolWarPVPMapHall = true;
		}

		SNATIVEID nidMAP ( sSWPVP.m_dwSchoolWarMap );
		SMAPNODE *pMAPNODE = GLGaeaServer::GetInstance().FindMapNode ( nidMAP );
		if ( pMAPNODE )
		{
			pMAPNODE->bSWPVPZone = TRUE;
		}

		pLandMan = GLGaeaServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarPVPMapID = sSWPVP.m_dwID;
			pLandMan->m_bSchoolWarPVPMap = true;	
		}
	}

	return true;
}

bool GLSchoolWarPVPFieldMan::FrameMove ( float fElaps )
{
	for ( SWPVP_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWarPVP &sSWPVP = m_vecSchoolWar[i];

		if( sSWPVP.IsBattle() )
		{
			sSWPVP.m_fRankingUpdate += fElaps;
			if ( sSWPVP.m_fRankingUpdate > sSWPVP.m_fRankingUpdateTime )
			{
				UpdateRanking( sSWPVP.m_dwID );
				sSWPVP.m_fRankingUpdate = 0.0f;
			}

		}

		if ( sSWPVP.IsBattleEndAward() )
		{
			BOOL bENDED = sSWPVP.CheckAwardTime( fElaps );
			if ( bENDED )
			{
				EndBattleField( sSWPVP.m_dwID );
			}
			else
			{
				if ( sSWPVP.m_bAwardStart )
				{
					SendRewards( sSWPVP.m_dwID );
				}
			}
		}
	}

	return true;
}


bool GLSchoolWarPVPFieldMan::ReadyBattle( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return false;

	pSWPVP->SetBattle( GLSchoolWarPVP::BATTLE_READY );

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:SchoolWar ReadyBattle:ID %d", dwID );
	return true;
}

bool GLSchoolWarPVPFieldMan::BeginBattle ( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return false;

	pSWPVP->SetBattle( GLSchoolWarPVP::BATTLE_START );
	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:SchoolWar BeginBattle:ID %d", dwID );

	return true;
}

bool GLSchoolWarPVPFieldMan::EndBattleAward ( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return false;

	pSWPVP->SetBattle( GLSchoolWarPVP::BATTLE_END_AWARD );

	//send final rankings to players
	UpdateRanking( dwID );

	//decide the winner
	CheckWinner( dwID );

	pSWPVP->m_bAwardStart = true;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:SchoolWar EndBattleAward:ID %d", dwID );

	return true;
}

bool GLSchoolWarPVPFieldMan::EndBattleField ( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return false;

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:SchoolWar Reward Ended:ID %d RewardNum:%d", dwID, pSWPVP->m_vecAwardChar.size() );

	pSWPVP->SetBattle( GLSchoolWarPVP::BATTLE_END );
	pSWPVP->FieldBattleEnd();

	GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:SchoolWar EndBattleField:ID %d", dwID );

	return true;
}

void GLSchoolWarPVPFieldMan::UpdateRanking( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return;

	UpdateRankingPlayer( dwID );
	UpdateRankingSchool( dwID );
	UpdateRankingSelf( dwID );
}

void GLSchoolWarPVPFieldMan::UpdateRankingPlayer( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return;

	GLSchoolWarPVP &sSWPVP = *pSWPVP;

	SCHOOLWARPVP_RANK_PLAYER_VEC		m_vecNew;
	m_vecNew.reserve( sSWPVP.m_mapScorePlayer.size() );

	SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	pos = sSWPVP.m_mapScorePlayer.begin();
	SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	end = sSWPVP.m_mapScorePlayer.end();
	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWARPVP_RANK_PLAYER&	sRankInfo = pos->second;					
		m_vecNew.push_back( sRankInfo );
	}

	std::sort( m_vecNew.begin(), m_vecNew.end() );	

	int nSize = (int)m_vecNew.size();

	for ( int i = 0; i < nSize; ++i )
	{
		m_vecNew[i].wRanking = i+1;
		m_vecNew[i].nIndex = i;
	}

	for ( int i = nSize-1; i > 0; --i )
	{
		if ( m_vecNew[i] == m_vecNew[i-1] )
		{
			m_vecNew[i-1].wRanking = m_vecNew[i].wRanking;			
		}
	}	

	GLMSG::SNET_SCHOOLWARPVP_RANKING_PLAYER_UPDATE	NetMsg;

	for ( int i = 0; i < (int)m_vecNew.size(); ++i )
	{
		SCHOOLWARPVP_RANK_PLAYER_MAP_ITER iter = sSWPVP.m_mapScorePlayer.find( m_vecNew[i].dwCharID );
		if ( iter == sSWPVP.m_mapScorePlayer.end() )	continue;

		SSCHOOLWARPVP_RANK_PLAYER&	sRankInfo = iter->second;

		sRankInfo.wRanking = m_vecNew[i].wRanking;
		sRankInfo.nIndex = m_vecNew[i].nIndex;

		if ( m_vecNew[i].nIndex >= 0 && m_vecNew[i].nIndex < SW_RANKING_NUM ) 
		{
			SSCHOOLWARPVP_RANK_PLAYER_CLIENT sRank = sRankInfo;
			NetMsg.ADDRANK( sRank );
		}		
	}

	if ( NetMsg.wRankNum > 0 )	
	{
		GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSWPVP.m_dwSchoolWarMap, &NetMsg );
		GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:Update Ranking Player RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
	}
}

void GLSchoolWarPVPFieldMan::UpdateRankingSchool( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return;

	GLSchoolWarPVP &sSWPVP = *pSWPVP;

	SCHOOLWARPVP_RANK_SCHOOL_VEC		m_vecNew;
	m_vecNew.reserve( sSWPVP.m_mapScoreSchool.size() );

	SCHOOLWARPVP_RANK_SCHOOL_MAP_ITER	pos = sSWPVP.m_mapScoreSchool.begin();
	SCHOOLWARPVP_RANK_SCHOOL_MAP_ITER	end = sSWPVP.m_mapScoreSchool.end();
	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWARPVP_RANK_SCHOOL&	sRankInfo = pos->second;					
		m_vecNew.push_back( sRankInfo );
	}

	std::sort( m_vecNew.begin(), m_vecNew.end() );	

	int nSize = (int)m_vecNew.size();

	for ( int i = 0; i < nSize; ++i )
	{
		m_vecNew[i].wRanking = i+1;
		m_vecNew[i].nIndex = i;
	}

	for ( int i = nSize-1; i > 0; --i )
	{
		if ( m_vecNew[i] == m_vecNew[i-1] )
		{
			m_vecNew[i-1].wRanking = m_vecNew[i].wRanking;			
		}
	}

	GLMSG::SNET_SCHOOLWARPVP_RANKING_SCHOOL_UPDATE	NetMsg;

	for ( int i = 0; i < (int)m_vecNew.size(); ++i )
	{
		SCHOOLWARPVP_RANK_SCHOOL_MAP_ITER iter = sSWPVP.m_mapScoreSchool.find( m_vecNew[i].wSCHOOL );
		if ( iter == sSWPVP.m_mapScoreSchool.end() )	continue;

		SSCHOOLWARPVP_RANK_SCHOOL&	sRankInfo = iter->second;

		sRankInfo.wRanking = m_vecNew[i].wRanking;
		sRankInfo.nIndex = m_vecNew[i].nIndex;

		if ( m_vecNew[i].nIndex < SW_SCHOOL_RANKING_NUM ) 
		{
			SSCHOOLWARPVP_RANK_SCHOOL_CLIENT sRank = sRankInfo;
			NetMsg.ADDRANK( sRank );
		}		
	}

	if ( NetMsg.wRankNum > 0 )	
	{
		GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSWPVP.m_dwSchoolWarMap, &NetMsg );
		GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:Update Ranking School RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
	}
}

void GLSchoolWarPVPFieldMan::UpdateRankingSelf( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return;

	GLSchoolWarPVP &sSWPVP = *pSWPVP;

	GLMSG::SNET_SCHOOLWARPVP_RANKING_SELF_UPDATE	NetMsgMy;

	SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	pos = sSWPVP.m_mapScorePlayer.begin();
	SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	end = sSWPVP.m_mapScorePlayer.end();

	for ( ; pos != end; pos++ )
	{
		const SSCHOOLWARPVP_RANK_PLAYER&	sRankInfo = pos->second;
		NetMsgMy.sRank.wRanking		= sRankInfo.wRanking;
		NetMsgMy.sRank.wKillNum		= sRankInfo.wKillNum;
		NetMsgMy.sRank.wDeathNum	= sRankInfo.wDeathNum;
		NetMsgMy.sRank.wSCHOOL		= sRankInfo.wSCHOOL;
		NetMsgMy.sRank.bVALID		= TRUE;

		PGLCHAR pCHAR = GLGaeaServer::GetInstance().GetCharID( sRankInfo.dwCharID );
		if ( pCHAR )
		{
			GLGaeaServer::GetInstance().SENDTOCLIENT( pCHAR->m_dwClientID, &NetMsgMy );
		}
	}
}

void GLSchoolWarPVPFieldMan::CheckWinner( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )	return;
	if ( !pSWPVP->IsBattleEndAward() )	return;

	GLSchoolWarPVP &sSWPVP = *pSWPVP;

	sSWPVP.m_mapScoreWinner.clear();
	sSWPVP.m_wSchoolWinner = MAX_SCHOOL;

	{
		//decide the winner
		SCHOOLWARPVP_RANK_SCHOOL_MAP_ITER	pos = sSWPVP.m_mapScoreSchool.begin();
		SCHOOLWARPVP_RANK_SCHOOL_MAP_ITER	end = sSWPVP.m_mapScoreSchool.end();
		for ( ; pos != end; pos++ )
		{
			const SSCHOOLWARPVP_RANK_SCHOOL&	sRankInfo = pos->second;
			if ( sRankInfo.wRanking == 1 )
			{
				sSWPVP.m_wSchoolWinner = sRankInfo.wSCHOOL;
				break;
			}
		}
	}

	WORD wWINNER = sSWPVP.m_wSchoolWinner;

	if ( wWINNER != MAX_SCHOOL )
	{
		SSCHOOLWARPVP_RANK_SCHOOL*  pINFO = sSWPVP.GetRankingSchool( wWINNER );
		if ( pINFO )
		{
			//send winner school to agent
			GLMSG::SNETPC_SERVER_SCHOOLWARPVP_WINNER_AG NetMsgAgentWinner;
			NetMsgAgentWinner.dwID = sSWPVP.m_dwID;
			NetMsgAgentWinner.wSCHOOL = sSWPVP.m_wSchoolWinner;
			GLGaeaServer::GetInstance().SENDTOAGENT ( &NetMsgAgentWinner );

			//send winner info on client
			GLMSG::SNET_SCHOOLWARPVP_WINNER_SCHOOL_UPDATE	NetMsgSchoolWinner;
			NetMsgSchoolWinner.sWinner.wSCHOOL = pINFO->wSCHOOL;
			NetMsgSchoolWinner.sWinner.wRanking = pINFO->wRanking;
			NetMsgSchoolWinner.sWinner.wKillNum = pINFO->wKillNum;
			NetMsgSchoolWinner.sWinner.wDeathNum = pINFO->wDeathNum;
			NetMsgSchoolWinner.sWinner.bVALID = TRUE;
			GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSWPVP.m_dwSchoolWarMap, &NetMsgSchoolWinner );

			//create ranking map of winners and validate their ranking order
			SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	pos = sSWPVP.m_mapScorePlayer.begin();
			SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	end = sSWPVP.m_mapScorePlayer.end();
			SCHOOLWARPVP_RANK_PLAYER_VEC		m_vec;

			for ( ; pos != end; pos++ )
			{
				const SSCHOOLWARPVP_RANK_PLAYER&	sRankInfo = pos->second;
				if ( sRankInfo.wSCHOOL == wWINNER )
					m_vec.push_back( sRankInfo );
			}

			std::sort( m_vec.begin(), m_vec.end() );

			int nSize = (int)m_vec.size();

			if ( nSize > 0 )
			{
				for ( int i = 0; i < nSize; ++i )
				{
					m_vec[i].wRanking = i+1;
					m_vec[i].nIndex = i;
				}

				for ( int i = nSize-1; i > 0; --i )
				{
					if ( m_vec[i] == m_vec[i-1] )
					{
						m_vec[i-1].wRanking = m_vec[i].wRanking;			
					}
				}

				for ( int i = 0; i < nSize; ++i )
				{
					sSWPVP.AddWinner( m_vec[i] );
				}
			}
		}	
	}

	{
		//send the god damn winners list to god damn fucking client!
		//sorry im already tired of this shit, time to get this shit done! --DevLordX
		GLMSG::SNET_SCHOOLWARPVP_WINNER_UPDATE	NetMsg;

		SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	pos = sSWPVP.m_mapScoreWinner.begin();
		SCHOOLWARPVP_RANK_PLAYER_MAP_ITER	end = sSWPVP.m_mapScoreWinner.end();

		for ( ; pos != end; pos++ )
		{
			const SSCHOOLWARPVP_RANK_PLAYER&	sRankInfo = pos->second;

			if ( sRankInfo.nIndex >= 0 && sRankInfo.nIndex < SW_RANKING_NUM ) 
			{
				SSCHOOLWARPVP_RANK_PLAYER_CLIENT sRank = sRankInfo;
				NetMsg.ADDRANK( sRank );	
			}
		}

		if ( NetMsg.wRankNum > 0 )	
		{
			GLGaeaServer::GetInstance().SENDTOCLIENT_ONMAP( sSWPVP.m_dwSchoolWarMap, &NetMsg );
			GLGaeaServer::GetInstance().GetConsoleMsg()->Write( "SCHOOLWARPVPINFO:CheckWinner RankNum:%d PacketSize:%d", NetMsg.wRankNum, NetMsg.nmg.dwSize );
		}
	}
}

void GLSchoolWarPVPFieldMan::SendRewards( DWORD dwID )
{
	GLSchoolWarPVP *pSWPVP = FindSchoolWar ( dwID, FALSE );
	if ( !pSWPVP )						return;
	if ( !pSWPVP->IsBattleEndAward() )	return;
	if ( !pSWPVP->m_bAwardStart )		return;

	GLLandMan *pLandMan = GLGaeaServer::GetInstance().GetByMapID ( pSWPVP->m_dwSchoolWarMap );
	if ( !pLandMan ) return;

	GLCHARNODE* pCharNode = pLandMan->m_GlobPCList.m_pHead;
	for ( ; pCharNode; pCharNode = pCharNode->pNext )
	{
		PGLCHAR pChar = GLGaeaServer::GetInstance().GetChar( pCharNode->Data->m_dwGaeaID );
		if ( !pChar ) continue;

		SCHOOLWARPVP_AWARD_CHAR_ITER iterRewarded = pSWPVP->m_vecAwardChar.find( pChar->m_dwCharID );
		if ( iterRewarded != pSWPVP->m_vecAwardChar.end() )	continue;

		SCHOOLWARPVP_RANK_PLAYER_MAP_ITER iterWinner = pSWPVP->m_mapScoreWinner.find( pChar->m_dwCharID );
		if ( iterWinner != pSWPVP->m_mapScoreWinner.end() ) 
		{
			SSCHOOLWARPVP_RANK_PLAYER& sRankInfo = iterWinner->second;

			int nAwardIndex;
			if ( sRankInfo.wRanking < SW_REWARD_NUM ) nAwardIndex = sRankInfo.wRanking - 1;
			else if ( sRankInfo.wRanking <= pSWPVP->m_sAwardItem.dwAwardLimit && sRankInfo.wKillNum >= pSWPVP->m_sAwardItem.dwKillMin ) nAwardIndex = ( SW_REWARD_NUM -1 );
			else continue;


			SNATIVEID sNativeID = pSWPVP->m_sAwardItem.nAwardItem[nAwardIndex];

			SITEM* pItem = GLItemMan::GetInstance().GetItem( sNativeID );
			if ( !pItem ) continue;

			SITEMCUSTOM sITEM_NEW;
			sITEM_NEW.sNativeID = sNativeID;
			sITEM_NEW.tBORNTIME = CTime::GetCurrentTime().GetTime();
			sITEM_NEW.cGenType = EMGEN_SYSTEM;
			sITEM_NEW.cChnID = (BYTE)GLGaeaServer::GetInstance().GetServerChannel();
			sITEM_NEW.cFieldID = (BYTE)GLGaeaServer::GetInstance().GetFieldSvrID();
			sITEM_NEW.lnGenNum = GLITEMLMT::GetInstance().RegItemGen ( sITEM_NEW.sNativeID, (EMITEMGEN)sITEM_NEW.cGenType );

			CItemDrop cDropItem;
			cDropItem.sItemCustom = sITEM_NEW;
			if ( pChar->IsInsertToInvenEx ( &cDropItem ) )
			{
				pChar->InsertToInvenEx ( &cDropItem );

				GLITEMLMT::GetInstance().ReqItemRoute ( sITEM_NEW, ID_CLUB, pChar->m_dwGuild, ID_CHAR, pChar->m_dwCharID, 
					EMITEM_ROUTE_SYSTEM, sITEM_NEW.wTurnNum );
			}else{
				pLandMan->DropItem ( pChar->m_vPos, &(cDropItem.sItemCustom), EMGROUP_ONE, pChar->m_dwGaeaID );
			}

			pSWPVP->m_vecAwardChar.insert( pChar->m_dwCharID );
		}
	}
}