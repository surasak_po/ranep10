#pragma once

#include "GLSchoolWarPVP.h"

class GLSchoolWarPVPMan
{
public:
	typedef std::vector<GLSchoolWarPVP>	SWPVP_VEC;
	typedef SWPVP_VEC::iterator			SWPVP_VEC_ITER;

protected:
	SWPVP_VEC	m_vecSchoolWar;

public:
	bool Load ( std::vector<std::string> &vecFiles );

public:
	GLSchoolWarPVP* FindSchoolWar ( DWORD dwID, BOOL bCHECK );
	bool IsBattle ();

public:
	GLSchoolWarPVPMan ()
	{
	}

public:
};


class GLSchoolWarPVPFieldMan : public GLSchoolWarPVPMan
{
public:

public:
	bool FrameMove ( float fElaps );

public:
	bool SetMapState ();


	bool ReadyBattle( DWORD dwID );
	bool BeginBattle ( DWORD dwID );
	bool EndBattleAward ( DWORD dwID );
	bool EndBattleField ( DWORD dwID );

	void UpdateRanking( DWORD dwID );
	void UpdateRankingPlayer( DWORD dwID );
	void UpdateRankingSchool( DWORD dwID );
	void UpdateRankingSelf( DWORD dwID );

	void CheckWinner( DWORD dwID );
	void SendRewards( DWORD dwID );


protected:
	GLSchoolWarPVPFieldMan ()
	{
	}

public:
	static GLSchoolWarPVPFieldMan& GetInstance();
};




class GLSchoolWarPVPAgentMan : public GLSchoolWarPVPMan
{
protected:
	float m_fRemainTimer, m_fTimer;
	float m_fEndedTimer;

public:
	bool SetMapState ();

public:
	DWORD GetRemainTime () { return (DWORD)m_fRemainTimer; }

public:
	bool FrameMove ( float fElapsedAppTime );

public:
	GLSchoolWarPVPAgentMan ()
		: m_fRemainTimer(0)
		, m_fTimer(0)
		, m_fEndedTimer( 0.0f )
	{
	}

public:
	static GLSchoolWarPVPAgentMan& GetInstance();
};