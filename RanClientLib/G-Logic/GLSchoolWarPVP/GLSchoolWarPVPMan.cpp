#include "StdAfx.h"
#include "GLSchoolWarPVPMan.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

bool GLSchoolWarPVPMan::Load ( std::vector<std::string> &vecFiles )
{
	std::vector<std::string>::size_type i = 0, j = vecFiles.size();
	for( ; i < j; ++i )
	{
		GLSchoolWarPVP sSchoolWarPVP;
		bool bOK = sSchoolWarPVP.Load( vecFiles[i] );
		if( !bOK )
		{
			CDebugSet::ToLogFile ( "GLSchoolWarPVP.Load() fail, %s", vecFiles[i].c_str() );
		}

		m_vecSchoolWar.push_back ( sSchoolWarPVP );
	}

	std::sort( m_vecSchoolWar.begin(), m_vecSchoolWar.end() );

	return true;
}

GLSchoolWarPVP* GLSchoolWarPVPMan::FindSchoolWar ( DWORD dwID, BOOL bCHECK )
{
	GLSchoolWarPVP cObj;
	cObj.m_dwID = dwID;

	SWPVP_VEC_ITER pos = std::lower_bound ( m_vecSchoolWar.begin(), m_vecSchoolWar.end(), cObj );
	if ( pos==m_vecSchoolWar.end() )
	{
		return NULL;
	}else{
		return &(*pos);
	}
}

bool GLSchoolWarPVPMan::IsBattle ()
{
	for ( SWPVP_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWarPVP &sSWPVP = m_vecSchoolWar[i];
		if ( sSWPVP.IsBattle() )		return true;
	}

	return false;
}
