#include "StdAfx.h"
#include "GLSchoolWarPVPMan.h"
#include "GLAgentServer.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//agent server
GLSchoolWarPVPAgentMan& GLSchoolWarPVPAgentMan::GetInstance()
{
	static GLSchoolWarPVPAgentMan cInstance;
	return cInstance;
}

bool GLSchoolWarPVPAgentMan::SetMapState ()
{
	for ( SWPVP_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWarPVP &sSWPVP= m_vecSchoolWar[i];

		SNATIVEID nidHallMAP ( sSWPVP.m_dwSchoolWarHallMap );
		GLAGLandMan *pLandMan = GLAgentServer::GetInstance().GetByMapID ( nidHallMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarPVPMapID = sSWPVP.m_dwID;
		}

		SNATIVEID nidMAP ( sSWPVP.m_dwSchoolWarMap );
		pLandMan = GLAgentServer::GetInstance().GetByMapID ( nidMAP );
		if ( pLandMan )
		{
			pLandMan->m_dwSchoolWarPVPMapID = sSWPVP.m_dwID;
			pLandMan->m_bSchoolWarPVPMap = true;
		}
	}

	return true;
}

bool GLSchoolWarPVPAgentMan::FrameMove ( float fElapsedAppTime )
{
	if ( GLCONST_CHAR::bPKLESS ) return false;

	for ( SWPVP_VEC::size_type i=0; i<m_vecSchoolWar.size(); ++i )
	{
		GLSchoolWarPVP &sSchoolWarPVP = m_vecSchoolWar[i];

		CTime cCurTime = CTime::GetCurrentTime();
		int nDayOfWeek = cCurTime.GetDayOfWeek ();
		int nDay = cCurTime.GetDay ();
		int nHour = cCurTime.GetHour();
		int nMinute = cCurTime.GetMinute ();

		DWORD dwORDER = sSchoolWarPVP.IsBattleWeekDay(nDayOfWeek, nHour );
		if ( dwORDER!=UINT_MAX )
		{
			if ( sSchoolWarPVP.IsBattleHour ( dwORDER, nHour ) )
			{
				sSchoolWarPVP.DoBattleStart ( dwORDER, nHour );
				m_fRemainTimer = (float)sSchoolWarPVP.m_dwBattleTime;
				m_fTimer = 0.0f;
			}
			else
			{
				sSchoolWarPVP.UpdateNotifyBattle ( dwORDER, nHour, nMinute );
			}
		}

		if ( sSchoolWarPVP.IsBattle() )
		{
			sSchoolWarPVP.m_fBattleTimer += fElapsedAppTime;
			if ( sSchoolWarPVP.m_fBattleTimer > float(sSchoolWarPVP.m_dwBattleTime) )
			{
				sSchoolWarPVP.DoBattleEndAward();
			}
		}

		if ( sSchoolWarPVP.IsBattleEndAward() )
		{
			BOOL bENDED = sSchoolWarPVP.CheckAwardTime( fElapsedAppTime );
			if ( bENDED )
			{
				sSchoolWarPVP.DoBattleEnd();
			}
		}
	}

	if ( !m_vecSchoolWar.empty() )
	{
		if ( m_vecSchoolWar[0].IsBattle() )
		{
			GLMSG::SNET_SCHOOLWARPVP_REMAIN_BRD NetMsgBrd;

			if ( m_fRemainTimer == m_vecSchoolWar[0].m_dwBattleTime )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
			}

			m_fTimer += fElapsedAppTime;
			m_fRemainTimer -= fElapsedAppTime;

			if ( m_fTimer > 600.0f )
			{
				NetMsgBrd.dwTime = (DWORD)m_fRemainTimer;
				GLAgentServer::GetInstance().SENDTOALLCLIENT ( &NetMsgBrd );
				m_fTimer = 0.0f;
			}
		}
	}

	return true;
}