#pragma once

#include "GLSchoolWarPVPData.h"

class GLSchoolWarPVP
{
public:
	enum 
	{		
		MAX_TIME = 56,
	};

	enum EM_BATTLE_STATE 
	{
		BATTLE_NOSTART		= 0,
		BATTLE_READY		= 1,
		BATTLE_START		= 2,
		BATTLE_END_AWARD	= 3,
		BATTLE_END			= 4,
	};

public:
	//common var
	DWORD					m_dwID;	
	std::string				m_strName;
	EM_BATTLE_STATE			m_emBattleState;
	DWORD					m_dwSchoolWarMap;
	DWORD					m_dwSchoolWarMapGate;
	DWORD					m_dwSchoolWarHallMap;
	DWORD					m_dwSchoolWarHallGate;
	float					m_fCHECK_TIMER;
	float					m_fCHECK_TIMER_MAX;
	WORD					m_wSchoolWinner;
	SCHOOLWARPVP_MOBSCORE_MAP		m_mapMobScore;
	WORD					m_wPlayerScoreAdd;
	float					m_fPartyScoreRange;
	//agent server
	SSWPVP_TIME				m_sSWPVPTIME[MAX_TIME];
	DWORD					m_dwLastBattleHour;
	DWORD					m_dwBattleOrder;
	bool					m_bNotifyOneHour;
	bool					m_bNotifyHalfHour;
	bool					m_bNotify10MinHour;
	DWORD					m_dwBattleTime;
	float					m_fBattleTimer;

	//field server
	SCHOOLWARPVP_RANK_PLAYER_MAP		m_mapScorePlayer;
	SCHOOLWARPVP_RANK_SCHOOL_MAP		m_mapScoreSchool;
	SCHOOLWARPVP_RANK_PLAYER_MAP		m_mapScoreWinner;
	float							m_fRankingUpdate;
	float							m_fRankingUpdateTime;
	SSCHOOLWARPVP_AWARD_ITEM			m_sAwardItem;
	SCHOOLWARPVP_AWARD_CHAR			m_vecAwardChar;
	bool							m_bAwardStart;
	
public:
	GLSchoolWarPVP ();

	GLSchoolWarPVP ( const GLSchoolWarPVP &value )
	{
		operator= ( value );
	}

	GLSchoolWarPVP& operator= ( const GLSchoolWarPVP& value );

	bool operator < ( const GLSchoolWarPVP& sSchoolWar )
	{
		return m_dwID < sSchoolWar.m_dwID;
	}

public:
	bool Load ( std::string strFile );


public:
	//common
	void SetBattle( EM_BATTLE_STATE emBattleState )	{ m_emBattleState = emBattleState; }
	bool IsBattle ()		{ return ( m_emBattleState == BATTLE_START ); }
	bool IsBattleReady()	{ return ( m_emBattleState == BATTLE_READY ); }
	bool IsBattleEndAward() { return ( m_emBattleState == BATTLE_END_AWARD); }
	bool IsBattleEnd()		{ return ( m_emBattleState == BATTLE_END); }

	bool IsEnterMap( WORD wSCHOOL );
	BOOL CheckAwardTime( float fElaps );

	//agent server 
	DWORD IsBattleWeekDay ( int nDayOfWeek, int nHour );
	bool IsBattleHour ( DWORD dwORDER, int nHour );
	void UpdateNotifyBattle ( DWORD dwORDER, int nHour, int nMinute );
	void NotifyBattleBroadcast( int nTIME, BOOL bREADY );
	void DoBattleStart ( DWORD dwORDER, int nHour );
	void DoBattleEndAward();
	void DoBattleEnd();

	//field server
	void FieldBattleEnd();
	SSWPVP_MOBSCORE*	GetMobScore( DWORD dwID );
	SSCHOOLWARPVP_RANK_PLAYER* GetRankingPlayer( DWORD dwCharID );
	SSCHOOLWARPVP_RANK_SCHOOL*	GetRankingSchool( WORD wSchool );
	void AddScore( DWORD dwKillChar, DWORD dwDeathChar );
	void AddWinner( SSCHOOLWARPVP_RANK_PLAYER sRANK );
	void AddScoreMob( DWORD dwKillChar, SNATIVEID sMOBID );
	void AddScoreMobSchool( DWORD dwKillChar, SNATIVEID sMOBID );


public:
};
