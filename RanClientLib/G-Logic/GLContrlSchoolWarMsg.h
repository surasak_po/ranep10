#pragma once

#include "GLContrlBaseMsg.h"

namespace GLMSG
{
	#pragma pack(1)

	/*struct SNETPC_SERVER_NEXTWAR
	{
		NET_MSG_GENERIC						nmg;
		float					fProgressTime;
		float					fNextStartTime;
		float					fNextEndTime;
		float					fPreviousStartTime;
		float					fPreviousEndTime;

		SNETPC_SERVER_NEXTWAR ()
			: fProgressTime(0.0f),
		      fNextStartTime(0.0f),
			  fNextEndTime(0.0f),
			  fPreviousStartTime(0.0f),
			  fPreviousEndTime(0.0f)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_NEXTWAR;
		}
	};

	struct SNET_SCHOOLWAR_NEXTWAR_REQ
	{
		NET_MSG_GENERIC						nmg;    
		float					fProgressTime;
		float					fNextStartTime;
		float					fNextEndTime;
		float					fPreviousStartTime;
		float					fPreviousEndTime;

		SNET_SCHOOLWAR_NEXTWAR_REQ () 
			: fProgressTime(0.0f),
		      fNextStartTime(0.0f),
			  fNextEndTime(0.0f),
			  fPreviousStartTime(0.0f),
			  fPreviousEndTime(0.0f)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_NEXTWAR_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_NEXTWAR_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};

	struct SNET_SCHOOLWAR_NEXTWAR_BRD
	{
		NET_MSG_GENERIC			nmg;
		float					fProgressTime;
		float					fNextStartTime;
		float					fNextEndTime;
		float					fPreviousStartTime;
		float					fPreviousEndTime;

		SNET_SCHOOLWAR_NEXTWAR_BRD ()
			: fProgressTime(0.0f),
		      fNextStartTime(0.0f),
			  fNextEndTime(0.0f),
			  fPreviousStartTime(0.0f),
			  fPreviousEndTime(0.0f)
		{
			nmg.dwSize = sizeof ( SNET_SCHOOLWAR_NEXTWAR_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOLWAR_NEXTWAR_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};*/

	struct SNETPC_SERVER_WINNER
	{
		NET_MSG_GENERIC						nmg;
		SSCHOOLWAR_RANK_SCHOOL_WINNER		sWinner;
		SSCHOOLWAR_RANK_PLAYER_CLIENT		sPlayer[SW_RANKING_NUM];
		WORD								wRankNum;

		SNETPC_SERVER_WINNER ():
			wRankNum(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_WINNER;
		}
	};

	struct SNETPC_SERVER_SCHOOLWAR_INFO
	{
		NET_MSG_GENERIC		nmg;
		bool				bSchoolWar;

		SNETPC_SERVER_SCHOOLWAR_INFO () :
		bSchoolWar(false)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SERVER_SCHOOLWAR_INFO;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};


	struct SNET_SCHOOLWAR_START_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWAR_START_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_START_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_START_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		int						nTIME;
		char					szName[TEXT_LEN];
		DWORD					dwID;

		SNET_SCHOOLWAR_START_BRD () 
			: nTIME(0),
			dwID(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_START_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_START_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWAR_READY_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWAR_READY_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_READY_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_READY_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];

		SNET_SCHOOLWAR_READY_BRD () /*:*/ 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_READY_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_READY_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWAR_REMAIN_BRD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwTime;

		SNET_SCHOOLWAR_REMAIN_BRD ()
			: dwTime(0)
		{
			nmg.dwSize = sizeof ( SNET_SCHOOLWAR_REMAIN_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOLWAR_REMAIN_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNETPC_SERVER_SCHOOLWAR_REMAIN_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwGaeaID;

		SNETPC_SERVER_SCHOOLWAR_REMAIN_AG ()
			: dwGaeaID(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_SERVER_SCHOOLWAR_REMAIN_AG;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_END_FLD
	{
		NET_MSG_GENERIC			nmg;
		DWORD					dwID;

		SNET_SCHOOLWAR_END_FLD () 
			: dwID(0)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_END_FLD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_END_FLD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_END_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		char					szName[TEXT_LEN];
		bool					bREWARD;

		SNET_SCHOOLWAR_END_BRD () :
			bREWARD( false )
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_END_BRD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_END_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
			memset(szName, 0, sizeof(char) * TEXT_LEN);
		}
	};

	struct SNET_SCHOOLWAR_RANKING_PLAYER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWAR_RANK_PLAYER_CLIENT	sRank[SW_RANKING_NUM];

		SNET_SCHOOLWAR_RANKING_PLAYER_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_RANKING_PLAYER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWAR_RANK_PLAYER_CLIENT& sRANK )
		{
			if ( SW_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWAR_RANK_PLAYER_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOLWAR_RANKING_SCHOOL_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWAR_RANK_SCHOOL_CLIENT		sRank[SW_SCHOOL_RANKING_NUM];

		SNET_SCHOOLWAR_RANKING_SCHOOL_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_RANKING_SCHOOL_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWAR_RANK_SCHOOL_CLIENT& sRANK )
		{
			if ( SW_SCHOOL_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWAR_RANK_SCHOOL_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_SCHOOLWAR_RANKING_SELF_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		SSCHOOLWAR_RANK_SELF	sRank;		

		SNET_SCHOOLWAR_RANKING_SELF_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_RANKING_SELF_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_RANKING_SELF_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWAR_POINT_UPDATE
	{
		NET_MSG_GENERIC			nmg;
		bool					bKillPoint;

		SNET_SCHOOLWAR_POINT_UPDATE () 
			: bKillPoint(false)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_POINT_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_POINT_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWAR_RANKING_REQ
	{
		NET_MSG_GENERIC		nmg;    
		DWORD				dwMapID;		

		SNET_SCHOOLWAR_RANKING_REQ () 
			: dwMapID(UINT_MAX)		
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_RANKING_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_RANKING_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};


	struct SNET_SCHOOLWAR_WINNER_REQ
	{
		NET_MSG_GENERIC						nmg;    
		DWORD								dwMapID;		
		int									nSchool;

		SNET_SCHOOLWAR_WINNER_REQ () 
			: dwMapID(UINT_MAX)
			, nSchool(-1)
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_WINNER_REQ);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_WINNER_REQ;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}       
	};

	struct SNETPC_SERVER_SCHOOLWAR_WINNER_AG
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwID;
		WORD				wSCHOOL;

		SNETPC_SERVER_SCHOOLWAR_WINNER_AG ()
			: dwID(0)
			, wSCHOOL(0)
		{
			nmg.dwSize = sizeof(*this);
			nmg.nType = NET_MSG_GCTRL_REQ_TO_AGENT_SCHOOLWAR_WINNER;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_WINNER_BRD
	{
		enum { TEXT_LEN = 33, };

		NET_MSG_GENERIC			nmg;
		WORD					wSCHOOL;
		char					szName[TEXT_LEN];


		SNET_SCHOOLWAR_WINNER_BRD ()
			: wSCHOOL(0)
		{
			memset(szName, 0, sizeof(char) * TEXT_LEN);

			nmg.dwSize = sizeof ( SNET_SCHOOLWAR_WINNER_BRD );
			nmg.nType  = NET_MSG_GCTRL_SCHOOLWAR_WINNER_BRD;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_SCHOOLWAR_WINNER_SCHOOL_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		SSCHOOLWAR_RANK_SCHOOL_WINNER		sWinner;		

		SNET_SCHOOLWAR_WINNER_SCHOOL_UPDATE () 
		{
			nmg.dwSize = sizeof(SNET_SCHOOLWAR_WINNER_SCHOOL_UPDATE);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_WINNER_SCHOOL_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_SCHOOLWAR_WINNER_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;		
		SSCHOOLWAR_RANK_PLAYER_CLIENT			sRank[SW_RANKING_NUM];

		SNET_SCHOOLWAR_WINNER_UPDATE () 
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_SCHOOLWAR_WINNER_PLAYER_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDRANK ( const SSCHOOLWAR_RANK_PLAYER_CLIENT& sRANK )
		{
			if ( SW_RANKING_NUM==wRankNum )		return false;

			sRank[wRankNum] = sRANK;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SSCHOOLWAR_RANK_PLAYER_CLIENT)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}

		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};


	// Revert to default structure packing
	#pragma pack()
};