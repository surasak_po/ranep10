#pragma once

#include "GLCharDefine.h"

#define RANKING_PNUM		100
#define CHAR_NAME		33

//sw scoring old
struct SSWP_RANK_EX
{
	EMCHARINDEX	dwClass;
	WORD	wSchool;
	WORD	wCharRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wTimeStomp;
	char	szCharName[CHAR_NAME+1];

	SSWP_RANK_EX()
		: dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, wCharRanking(0)
		, wKillNum (0)
		, wDeathNum(0)
		, wTimeStomp(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	void Init()
	{
		dwClass = GLCI_FIGHTER_M;
		wSchool = 0;
		wCharRanking = 0;
		wKillNum = 0;
		wDeathNum = 0;
		wTimeStomp = 0;
		StringCchCopy( szCharName, CHAR_NAME+1, "" );
	}
};

struct SSWP_RANK_INFO
{
	int		nIndex;   
	EMCHARINDEX	dwClass;
	WORD	wSchool;
	DWORD	dwCharID;
	DWORD	dwGaeaID;
	WORD	wCharRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wTimeStomp;
	char	szCharName[CHAR_NAME+1];	
	
	SSWP_RANK_INFO()
        : nIndex ( -1 )
		, dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, dwCharID( 0 )
		, dwGaeaID( 0 )
		, wCharRanking(0)
		, wKillNum(0)
		, wDeathNum(0)
		, wTimeStomp(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	SSWP_RANK_INFO ( const SSWP_RANK_INFO &value )
	{
		operator=(value);
	}

	SSWP_RANK_INFO& operator = ( const SSWP_RANK_INFO& rvalue )
	{
		nIndex = rvalue.nIndex;
		dwCharID = rvalue.dwCharID;
		dwGaeaID = rvalue.dwGaeaID;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		wCharRanking = rvalue.wCharRanking;
		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );
		
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wTimeStomp = rvalue.wTimeStomp;

		return *this;
	}

	bool operator < ( const SSWP_RANK_INFO& sSwPlayerRank )
	{			
		if ( wKillNum > sSwPlayerRank.wKillNum ) return true;
		else if ( wKillNum == sSwPlayerRank.wKillNum && wDeathNum == sSwPlayerRank.wDeathNum && wTimeStomp < sSwPlayerRank.wTimeStomp )	return true;
		else if ( wKillNum == sSwPlayerRank.wKillNum && wDeathNum < sSwPlayerRank.wDeathNum ) return true;
		
		return  false;
	}

	bool operator == ( const SSWP_RANK_INFO& sSwPlayerRank ) 
	{
		if ( wKillNum == sSwPlayerRank.wKillNum && wDeathNum == sSwPlayerRank.wDeathNum && wTimeStomp == sSwPlayerRank.wTimeStomp ) return true;

		return false;

	}
};

struct SSWP_RANK
{
	int nIndex;
	WORD wCharRanking;
	EMCHARINDEX	dwClass;
	WORD		wSchool;
	char szCharName[CHAR_NAME+1];

	WORD wKillNum;
	WORD wDeathNum;
	WORD wTimeStomp;

	SSWP_RANK() 
		: nIndex(-1)
		, dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, wCharRanking (0)
		, wKillNum(0)
		, wDeathNum(0)
		, wTimeStomp(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	SSWP_RANK ( const SSWP_RANK &value )
	{
		operator=(value);
	}

	SSWP_RANK& operator = ( const SSWP_RANK& rvalue )
	{
		nIndex = rvalue.nIndex;
		wCharRanking = rvalue.wCharRanking;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );
		
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wTimeStomp = rvalue.wTimeStomp;

		return *this;
	}

	SSWP_RANK ( const SSWP_RANK_INFO &value )
	{
		operator=(value);
	}

	SSWP_RANK& operator = ( const SSWP_RANK_INFO& rvalue )
	{
		nIndex = rvalue.nIndex;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		wCharRanking = rvalue.wCharRanking;
		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );
		
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wTimeStomp = rvalue.wTimeStomp;

		return *this;
	}
};

typedef std::vector<SSWP_RANK_INFO>		SWP_RANK_INFO_VEC;
typedef SWP_RANK_INFO_VEC::iterator		SWP_RANK_INFO_VEC_ITER;

typedef std::vector<SSWP_RANK>			SWP_RANK_VEC;
typedef SWP_RANK_VEC::iterator			SWP_RANK_VEC_ITER;

typedef std::map<DWORD,SSWP_RANK_INFO>	SWP_RANK_INFO_MAP;
typedef SWP_RANK_INFO_MAP::iterator		SWP_RANK_INFO_MAP_ITER;

typedef std::set<DWORD>					SWP_AWARD_CHAR;	
typedef SWP_AWARD_CHAR::iterator		SWP_AWARD_CHAR_ITER;

struct SSWP_AWARD_ITEM
{
	SNATIVEID	nAwardItem[10];
	DWORD		dwAwardLimit;

	SSWP_AWARD_ITEM()
		: dwAwardLimit(10)
	{	
		memset( nAwardItem, -1, sizeof( SNATIVEID ) * 10 );
	}
};
//
struct SRR_AWARD_POINT
{
	DWORD		dwPoint[10];
	DWORD		dwAwardLimit;

	SRR_AWARD_POINT()
		: dwAwardLimit(10)
	{	
		memset( dwPoint, -1, sizeof( DWORD ) * 10 );
	}
};

struct STWP_RANK_EX
{
	EMCHARINDEX	dwClass;
	WORD	wSchool;
	WORD	wCharRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wResuNum;
	DWORD	dwDamageNum;
	DWORD	dwHealNum;
	char	szCharName[CHAR_NAME+1];

	STWP_RANK_EX()
		: dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, wCharRanking(0)
		, wKillNum (0)
		, wDeathNum(0)
		, wResuNum(0)
		, dwDamageNum(0)
		, dwHealNum(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	void Init()
	{
		dwClass = GLCI_FIGHTER_M;
		wSchool = 0;
		wCharRanking = 0;
		wKillNum = 0;
		wDeathNum = 0;
		wResuNum = 0;
		dwDamageNum = 0;
		dwHealNum = 0;
		StringCchCopy( szCharName, CHAR_NAME+1, "" );
	}
};

struct STWP_RANK_INFO
{
	int		nIndex;   
	DWORD	dwCharID;
	DWORD	dwGaeaID;
	EMCHARINDEX	dwClass;
	WORD	wSchool;
	WORD	wCharRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wResuNum;
	DWORD	dwDamageNum;
	DWORD	dwHealNum;

	char	szCharName[CHAR_NAME+1];	
	
	STWP_RANK_INFO()
        : nIndex ( -1 )
		, dwCharID( 0 )
		, dwGaeaID( 0 )
		, dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, wCharRanking(0)
		, wKillNum(0)
		, wDeathNum(0)
		, wResuNum(0)
		, dwDamageNum(0)
		, dwHealNum(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	STWP_RANK_INFO ( const STWP_RANK_INFO &value )
	{
		operator=(value);
	}

	STWP_RANK_INFO& operator = ( const STWP_RANK_INFO& rvalue )
	{
		nIndex = rvalue.nIndex;
		dwCharID = rvalue.dwCharID;
		dwGaeaID = rvalue.dwGaeaID;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		wCharRanking = rvalue.wCharRanking;		
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wResuNum = rvalue.wResuNum;
		dwDamageNum = rvalue.dwDamageNum;
		dwHealNum = rvalue.dwHealNum;

		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );

		return *this;
	}

	bool operator < ( const STWP_RANK_INFO& sSwPlayerRank )
	{		
		if( ( wKillNum + wResuNum ) >= ( sSwPlayerRank.wKillNum + sSwPlayerRank.wResuNum ) &&
			( dwDamageNum + dwHealNum ) > ( sSwPlayerRank.dwDamageNum + sSwPlayerRank.dwHealNum ) &&
			 wDeathNum <= sSwPlayerRank.wDeathNum )
			return true;
		
		return  false;
	}

	bool operator == ( const STWP_RANK_INFO& sSwPlayerRank ) 
	{
		if ( wKillNum == sSwPlayerRank.wKillNum && wDeathNum == sSwPlayerRank.wDeathNum &&
			dwDamageNum == sSwPlayerRank.dwDamageNum && dwHealNum == sSwPlayerRank.dwHealNum &&
			wResuNum == sSwPlayerRank.wResuNum ) return true;

		return false;

	}
};

struct STWP_RANK
{
	int nIndex;

	EMCHARINDEX	dwClass;
	WORD	wSchool;
	WORD	wCharRanking;
	WORD	wKillNum;
	WORD	wDeathNum;
	WORD	wResuNum;
	DWORD	dwDamageNum;
	DWORD	dwHealNum;

	char szCharName[CHAR_NAME+1];

	STWP_RANK() 
		: nIndex(-1)
		, dwClass(GLCI_FIGHTER_M)
		, wSchool(0)
		, wCharRanking (0)
		, wKillNum(0)
		, wDeathNum(0)
		, wResuNum(0)
		, dwDamageNum(0)
		, dwHealNum(0)
	{
		memset( szCharName, 0, sizeof(char) * (CHAR_NAME+1) );
	}

	STWP_RANK ( const STWP_RANK &value )
	{
		operator=(value);
	}

	STWP_RANK& operator = ( const STWP_RANK& rvalue )
	{
		nIndex = rvalue.nIndex;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		wCharRanking = rvalue.wCharRanking;
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wResuNum = rvalue.wResuNum;
		dwDamageNum = rvalue.dwDamageNum;
		dwHealNum = rvalue.dwHealNum;

		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );
		return *this;
	}

	STWP_RANK ( const STWP_RANK_INFO &value )
	{
		operator=(value);
	}

	STWP_RANK& operator = ( const STWP_RANK_INFO& rvalue )
	{
		nIndex = rvalue.nIndex;
		dwClass = rvalue.dwClass;
		wSchool = rvalue.wSchool;
		wCharRanking = rvalue.wCharRanking;
		wKillNum = rvalue.wKillNum;
		wDeathNum = rvalue.wDeathNum;
		wResuNum = rvalue.wResuNum;
		dwDamageNum = rvalue.dwDamageNum;
		dwHealNum = rvalue.dwHealNum;

		StringCchCopy( szCharName, CHAR_NAME+1, rvalue.szCharName );
		return *this;
	}
};

typedef std::vector<STWP_RANK_INFO>		TWP_RANK_INFO_VEC;
typedef TWP_RANK_INFO_VEC::iterator		TWP_RANK_INFO_VEC_ITER;

typedef std::vector<STWP_RANK>			TWP_RANK_VEC;
typedef TWP_RANK_VEC::iterator			TWP_RANK_VEC_ITER;

typedef std::map<DWORD,STWP_RANK_INFO>	TWP_RANK_INFO_MAP;
typedef TWP_RANK_INFO_MAP::iterator		TWP_RANK_INFO_MAP_ITER;

typedef std::set<DWORD>					TWP_AWARD_CHAR;	
typedef TWP_AWARD_CHAR::iterator		TWP_AWARD_CHAR_ITER;

struct STWP_AWARD_ITEM
{
	SNATIVEID	nAwardItem[10];
	DWORD		dwAwardLimit;

	STWP_AWARD_ITEM()
		: dwAwardLimit(10)
	{	
		memset( nAwardItem, -1, sizeof( SNATIVEID ) * 10 );
	}
};