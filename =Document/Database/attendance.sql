set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_InsertItem]
	@PurKey	varchar(100),
	@UserId varchar(50),
	@MID int,
	@SID int,
	@nReturn int OUTPUT
AS	

DECLARE	@productNum int;

SET NOCOUNT ON;
	
BEGIN TRAN	

SET @nReturn = -1;
	
SELECT @productNum  = ProductNum FROM ShopItemMap (NOLOCK) 
WHERE ItemMain = @MID and ItemSub = @SID;

-- [x]Twoo 02/12/13
INSERT INTO ShopPurchase 
(
UserUID,
ProductNum,
PurPrice,
PurFlag,
PurDate,
PurChgDate
)
VALUES
(
@UserId,
@productNum,
0,
0,
GETDATE(),
GETDATE()
)

IF @@ERROR = 0 
	BEGIN 
		COMMIT TRAN;
		SET @nReturn = 0;
	END

SET NOCOUNT OFF;

RETURN @nReturn



