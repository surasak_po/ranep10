///////////////////////////////////////////////////////////////////////////////
// s_NetClientMsg.cpp
//
// class CNetClient
//
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "s_NetClient.h"
#include "s_CClientConsoleMsg.h"
#include <stdlib.h>
#include "GLContrlMsg.h"
#include "china_md5.h"
#include <string.h>
#include <wchar.h>
#include "s_CRandomNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


int CNetClient::SndRegister( 
				const TCHAR* szUserID,
		        const TCHAR* szUserPassword,
				const TCHAR* szUserPassword2,
				const TCHAR* szUserEmail,
				const TCHAR* szCaptcha )
{

	CHINA_NET_REGISTER_DATA nld;
		

	CHINA_MD5::MD5 md5;
	CHINA_MD5::MD5 md52;

	size_t nLength = 0;
	size_t nLength2 = 0;
	HRESULT sRet = StringCchLength( szUserPassword, USR_PASS_LENGTH, &nLength );

	HRESULT sRet2 = StringCchLength( szUserPassword2, USR_PASS_LENGTH, &nLength2 );
	
	if (sRet == STRSAFE_E_INVALID_PARAMETER)
		return NET_ERROR;
	
	if (sRet2 == STRSAFE_E_INVALID_PARAMETER)
		return NET_ERROR;

	md5.update( reinterpret_cast<const unsigned char*> (szUserPassword), nLength );	
	md52.update( reinterpret_cast<const unsigned char*> (szUserPassword2), nLength2 );	

	std::string strPass = md5.asString();
	std::string strPass2 = md52.asString();

	StringCchCopy( nld.szCaptcha,   USR_CAPTCHA+1, szCaptcha ); 
	StringCchCopy( nld.szUserid,   USR_ID_LENGTH+1, szUserID ); 
	StringCchCopy( nld.szUserEmail,   USR_EMAIL+1, szUserEmail ); 
	StringCchCopy( nld.szPassword, USR_PASS_LENGTH, strPass.c_str() );
	StringCchCopy( nld.szPassword2, USR_PASS_LENGTH, strPass2.c_str() );
	
	m_Tea.encrypt( nld.szCaptcha, USR_CAPTCHA+1 );
	m_Tea.encrypt( nld.szUserid, USR_ID_LENGTH+1 );
	m_Tea.encrypt( nld.szUserEmail, USR_EMAIL+1 );
	m_Tea.encrypt( nld.szPassword, USR_PASS_LENGTH );
	m_Tea.encrypt( nld.szPassword2, USR_PASS_LENGTH );

	return Send((char *) &nld);
}