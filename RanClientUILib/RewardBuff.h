#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"
class CItemImage;

class CRewardBuff : public CUIGroup
{
protected:
	enum
	{
		REWARD_BUFF0 = NO_ID + 1,
		REWARD_BUFF1,
		REWARD_BUFF2,
		REWARD_BUFF3,
	};

public:
	CRewardBuff();
	virtual ~CRewardBuff();

	void	CreateSubControl ();
	virtual	void	TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	void	SetSwBonus( bool bWinner );

private:
	CItemImage*			m_pRewardBuff[4];
	CUIControl*			m_pRewardBuffBack[4];
	CBasicButton*       m_pRewardDummy;
};