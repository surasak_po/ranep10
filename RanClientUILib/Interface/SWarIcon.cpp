#include "StdAfx.h"
#include "../EngineUILib/GUInterface/BasicButton.h"
#include "GLGaeaClient.h"
#include "GLCharacter.h"
#include "InnerInterface.h"
#include ".\SWarIcon.h"
#include "GameTextControl.h"
#include "UITextControl.h"
#include "../EngineUIlib/GUInterface/BasicProgressBar.h"
#include "BasicLineBoxEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSWarIcon::CSWarIcon(void) :
	m_pSWarIcon(NULL)
{
}

CSWarIcon::~CSWarIcon(void)
{
}
void CSWarIcon::CreateSubControl ()
{
	m_pSWarIcon = new CBasicButton;
	m_pSWarIcon->CreateSub ( this, "COMPETITION_NOTIFY_BUTTON_IMAGE", UI_FLAG_DEFAULT, SWarIconOnMouse );
	m_pSWarIcon->SetUseGlobalAction ( TRUE );
	RegisterControl ( m_pSWarIcon );
}
void CSWarIcon::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}
void CSWarIcon::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
switch ( ControlID )
	{
	case SWarIconOnMouse:
		{
		PLANDMANCLIENT pLand = GLGaeaClient::GetInstance().GetActiveMap();

			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{	
				//  ranking caller
				CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "�Ԩ�����ç俿��(Tyranny)(BattlefieldUI).", NS_UITEXTCOLOR::WHITE  );
	            //CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( " School War / Superior War Ranking.", NS_UITEXTCOLOR::WHITE  );
	
				if ( dwMsg & UIMSG_LB_UP && pLand && pLand->m_bSchoolWar )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SCHOOLWAR_RANKING_DISPLAY ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SCHOOLWAR_RANKING_DISPLAY );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SCHOOLWAR_RANKING_DISPLAY );
					}
	
				}
				else if ( dwMsg & UIMSG_LB_UP && pLand && pLand->m_bSuperiorWar )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SUPERIORWAR_RANK ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SUPERIORWAR_RANK );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SUPERIORWAR_RANK );
					}
				}
			    else if  ( dwMsg & UIMSG_LB_UP && pLand && pLand->m_bSchoolWarPVP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SCHOOLWARPVP_RANKING_DISPLAY ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SCHOOLWARPVP_RANKING_DISPLAY );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SCHOOLWARPVP_RANKING_DISPLAY );
					}
	
				}
			    else if  ( dwMsg & UIMSG_LB_UP && pLand && pLand->m_bTowerWars )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( TOWER_RANKING_DISPLAY ) )
					{
						   GLGaeaClient::GetInstance().ReqTowerWarsCTFInfo();
				           CInnerInterface::GetInstance().VisibleCTFRanking( true );
				           CInnerInterface::GetInstance().RefreshCTFRanking();
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( TOWER_RANKING_DISPLAY );
					}
	
				}
				else if  ( dwMsg & UIMSG_LB_UP && pLand && !pLand->m_bSuperiorWar && !pLand->m_bSchoolWar && !pLand->m_bSchoolWarPVP )
				{
					/*if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SCHOOLWAR_CTF_DISPLAY ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SCHOOLWAR_CTF_DISPLAY );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SCHOOLWAR_CTF_DISPLAY );
					}*/

					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( BATTLEFIELDUI_WINDOW ) )
					{

						CInnerInterface::GetInstance().ShowGroupFocus ( BATTLEFIELDUI_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( BATTLEFIELDUI_WINDOW );
					}
				}
			}

		}
		break;
	}
}