#include "StdAfx.h"
#include "SuperiorWarRankSlot.h"
#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "../../RanClientLib/G-Logic/GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSuperiorWarRankSlot::CSuperiorWarRankSlot () :
	m_pRankNum( NULL ),
	m_pRankName( NULL ),
	m_pRankPoints( NULL ),
	m_pSelf( NULL )
{
	for ( int i=0; i<3; ++i )
	{
		m_pRankSchool[i] = NULL;
	}

	for ( int i=0; i < GLCI_NUM_2012; ++ i )
	{
		m_pRankClass[i] = NULL;
	}
}

CSuperiorWarRankSlot::~CSuperiorWarRankSlot ()
{
}

void CSuperiorWarRankSlot::CreateSubControl ()
{
	CD3DFontPar* pFont8 = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 8, D3DFONT_SHADOW | D3DFONT_ASCII );

	CBasicLineBox* pLineBox = new CBasicLineBox;
	pLineBox->CreateSub ( this, "BASIC_LINE_BOX_MINIPARTY", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox->CreateBaseBoxQuestList( "SPW_RANKSLOT_LINE" );
	RegisterControl ( pLineBox );

	m_pSelf = new CUIControl;
	m_pSelf->CreateSub ( this, "SPW_RANKSLOT_SELF", UI_FLAG_DEFAULT );	
	m_pSelf->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pSelf );

	CString strSchool[3] = 
	{
		"SPW_RANKSLOT_IMAGE_SCHOOL0",
		"SPW_RANKSLOT_IMAGE_SCHOOL1",
		"SPW_RANKSLOT_IMAGE_SCHOOL2"
	};

	CString strClass[GLCI_NUM_2012] = 
	{
		"SPW_RANKSLOT_IMAGE_BRAWLER_MALE",
		"SPW_RANKSLOT_IMAGE_SWORDSMAN_MALE",
		"SPW_RANKSLOT_IMAGE_ARCHER_FEMALE",
		"SPW_RANKSLOT_IMAGE_SHAMAN_FEMALE",
		"SPW_RANKSLOT_IMAGE_EXTREME_MALE",
		"SPW_RANKSLOT_IMAGE_EXTREME_FEMALE",
		"SPW_RANKSLOT_IMAGE_BRAWLER_FEMALE",
		"SPW_RANKSLOT_IMAGE_SWORDSMAN_FEMALE",
		"SPW_RANKSLOT_IMAGE_ARCHER_MALE",
		"SPW_RANKSLOT_IMAGE_SHAMAN_MALE",
	};	

	for ( int i = 0; i < 3; ++ i )
	{
		m_pRankSchool[i] = CreateControl( strSchool[i].GetString() );
	}

	for ( int i=0; i < GLCI_NUM_2012; ++ i )
	{
		m_pRankClass[i] = CreateControl( strClass[i].GetString() );;
	}

	m_pRankNum = CreateStaticControl ( "SPW_RANKSLOT_NUM_TEXT", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
	m_pRankName = CreateStaticControl ( "SPW_RANKSLOT_NAME", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
	m_pRankPoints = CreateStaticControl ( "SPW_RANKSLOT_POINT_TEXT", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
}

CUIControl*	CSuperiorWarRankSlot::CreateControl ( const char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}

CBasicTextBox* CSuperiorWarRankSlot::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}

void CSuperiorWarRankSlot::DataUpdate( int nNUM, std::string strNAME, int nSCHOOL, int nCLASS, int nPOINTS , DWORD dwCHARID /*= UINT_MAX*/ )
{
	DataReset();

	if ( nSCHOOL < 0 || nSCHOOL >=3 )	return;
	if ( nCLASS < 0 || nCLASS >=GLCI_NUM_2012 )	return;

	if ( m_pRankSchool[nSCHOOL] )
		m_pRankSchool[nSCHOOL]->SetVisibleSingle( TRUE );

	if ( m_pRankClass[nCLASS] )
		m_pRankClass[nCLASS]->SetVisibleSingle( TRUE );

	if ( m_pRankNum )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nNUM );
		m_pRankNum->SetText( strTEXT.GetString(), NS_UITEXTCOLOR::WHITE );
	}

	if ( m_pRankName )
	{
		m_pRankName->SetText( strNAME.c_str(), NS_UITEXTCOLOR::WHITE );
	}


	if ( m_pRankPoints )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nPOINTS );
		m_pRankPoints->SetText( strTEXT.GetString(), NS_UITEXTCOLOR::RED );
	}

	if ( dwCHARID != UINT_MAX )
	{
		DWORD dwCharID = GLGaeaClient::GetInstance().GetCharacter()->m_dwCharID;

		BOOL bSELF = ( dwCHARID == dwCharID );

		if ( m_pSelf )
			m_pSelf->SetVisibleSingle( bSELF );
	}
}

void CSuperiorWarRankSlot::DataReset()
{
	for ( int i = 0; i < 3; ++ i )
	{
		if ( m_pRankSchool[i] )
			m_pRankSchool[i]->SetVisibleSingle( FALSE );
	}

	for ( int i=0; i < GLCI_NUM_2012; ++ i )
	{
		if ( m_pRankClass[i] )
			m_pRankClass[i]->SetVisibleSingle( FALSE );
	}

	if ( m_pRankNum )	m_pRankNum->ClearText();
	if ( m_pRankName )	m_pRankName->ClearText();
	if ( m_pRankPoints )	m_pRankPoints->ClearText();
}