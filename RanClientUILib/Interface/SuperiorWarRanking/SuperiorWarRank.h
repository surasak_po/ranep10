#pragma	once

#include "../UIWindowEx.h"

class CSuperiorWarRankSlot;
class CBasicScrollBarEx;

class	CSuperiorWarRank : public CUIWindowEx
{
protected:
	enum
	{
		RANKPLAYER_SCROLLBAR = ET_CONTROL_NEXT,
	};

	enum
	{
		SPW_PLAYER_SLOT_MAX = 10,
	};
public:
	CSuperiorWarRank ();
	virtual	~CSuperiorWarRank ();

public:
	void CreateSubControl ();

private:
	int						m_nActivePage;
	CUIControl*				m_pBackGround;
	CSuperiorWarRankSlot*	m_pRankSlot[SPW_PLAYER_SLOT_MAX];
	CBasicScrollBarEx*		m_pScrollBar;
	CSuperiorWarRankSlot*	m_pRankSelf;
	
	int		m_nStart;
	int		m_nTotal;

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual	void SetVisibleSingle ( BOOL bVisible );

public:
	void	RefreshRanking();
	void	RenderView();
};