#include "StdAfx.h"
#include "SuperiorWarRank.h"
#include "SuperiorWarRankSlot.h"
#include "../InnerInterface.h"
#include "../BasicTextButton.h"
#include "../GameTextControl.h"
#include "../BasicScrollBarEx.h"
#include "../../EngineUiLib/GUInterface/BasicScrollThumbFrame.h"
#include "../RanClientLib/G-Logic/GLSuperiorWar/GLSuperiorWarData.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSuperiorWarRank::CSuperiorWarRank () :
	m_pBackGround( NULL )
	, m_pScrollBar(NULL)
	, m_pRankSelf( NULL )
	, m_nStart( 0 )
	, m_nTotal( 0 )
{
	for ( int i=0; i < SPW_PLAYER_SLOT_MAX; ++ i )
	{
		m_pRankSlot[i] = NULL;
	}
}

CSuperiorWarRank::~CSuperiorWarRank ()
{
}

void CSuperiorWarRank::CreateSubControl ()
{	
	m_pBackGround = new CUIControl;
	m_pBackGround->CreateSub ( this, "SPW_RANKING_BG", UI_FLAG_DEFAULT );	
	m_pBackGround->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pBackGround );

	m_pScrollBar = new CBasicScrollBarEx;
	m_pScrollBar->CreateSub ( this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE, RANKPLAYER_SCROLLBAR );
	m_pScrollBar->CreateBaseScrollBar ( "SPW_RANKING_SCROLL" );
	m_pScrollBar->GetThumbFrame()->SetState ( 1, 10 );
	RegisterControl ( m_pScrollBar );


	std::string strSLOT_PLAYER[SPW_PLAYER_SLOT_MAX] = 
	{
		"SPW_RANKING_SLOT_00",
		"SPW_RANKING_SLOT_01",
		"SPW_RANKING_SLOT_02",
		"SPW_RANKING_SLOT_03",
		"SPW_RANKING_SLOT_04",
		"SPW_RANKING_SLOT_05",
		"SPW_RANKING_SLOT_06",
		"SPW_RANKING_SLOT_07",
		"SPW_RANKING_SLOT_08",
		"SPW_RANKING_SLOT_09",
	};

	for ( int i=0; i < SPW_PLAYER_SLOT_MAX; ++ i )
	{
		m_pRankSlot[i] = new CSuperiorWarRankSlot;
		m_pRankSlot[i]->CreateSub ( this, strSLOT_PLAYER[i].c_str() );
		m_pRankSlot[i]->CreateSubControl ();
		RegisterControl ( m_pRankSlot[i] );	
		m_pRankSlot[i]->SetVisibleSingle( FALSE );
	}

	m_pRankSelf = new CSuperiorWarRankSlot;
	m_pRankSelf->CreateSub ( this, "SPW_RANKING_SLOT_SELF" );
	m_pRankSelf->CreateSubControl ();
	RegisterControl ( m_pRankSelf );
}

void CSuperiorWarRank::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	switch ( ControlID )
	{
	case ET_CONTROL_TITLE:
	case ET_CONTROL_TITLE_F:
		{
			if ( (dwMsg & UIMSG_LB_DUP) && CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( SCHOOLWAR_RANKING_DISPLAY );
			}
		}break;
	case ET_CONTROL_BUTTON:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				CInnerInterface::GetInstance().HideGroup ( GetWndID () );
			}
		}break;
	}

	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );
}

void CSuperiorWarRank::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SuperiorWarRankingPlayerVec.size();

	if ( m_pScrollBar )
	{
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal();

			if( nTotal != m_nTotal )
				m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SPW_PLAYER_SLOT_MAX );	
		}
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal ();
			if ( nTotal <= SPW_PLAYER_SLOT_MAX ) return ;
			const int nViewPerPage = pThumbFrame->GetViewPerPage ();

			if ( nViewPerPage < nTotal )
			{
				int nCurPos = 0;
				const int nMovableLine = nTotal - nViewPerPage;
				float fPercent = pThumbFrame->GetPercent ();
				nCurPos = (int)floor(fPercent * nMovableLine);
				if ( nCurPos < 0 ) nCurPos = 0;
				if ( m_nStart == nCurPos ) return;
				m_nStart = nCurPos;
				RenderView();
			}
		}
	}
}

void CSuperiorWarRank::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindowEx::SetVisibleSingle( bVisible );
}

void CSuperiorWarRank::RefreshRanking()
{
	m_nTotal = (int)GLGaeaClient::GetInstance().m_SuperiorWarRankingPlayerVec.size();

	RenderView();
}

void CSuperiorWarRank::RenderView()
{
	for ( int i=0; i < SPW_PLAYER_SLOT_MAX; ++ i )
	{
		if ( m_pRankSlot[i] )
		{
			m_pRankSlot[i]->DataReset();
			m_pRankSlot[i]->SetVisibleSingle( FALSE );
		}
	}

	if ( m_pRankSelf )
	{
		m_pRankSelf->SetVisibleSingle( FALSE );
		m_pRankSelf->DataReset();
	}

	SUPERIORWAR_RANK_PLAYER_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SuperiorWarRankingPlayerVec;

	int nEnd = m_nStart + SPW_PLAYER_SLOT_MAX;
	int nSlot = 0;

	for ( int i = m_nStart; i < nEnd; ++ i )
	{
		if ( i >= (int)vecRank.size() )	continue;
		if ( m_pRankSlot[nSlot] )
		{
			SSUPERIORWAR_RANK_PLAYER_CLIENT sRANK = vecRank[i];
			m_pRankSlot[nSlot]->SetVisibleSingle( TRUE );
			m_pRankSlot[nSlot]->DataUpdate( sRANK.nRanking, sRANK.szCharName, sRANK.nSCHOOL, sRANK.nCLASS, sRANK.wPoints );
			nSlot ++ ;
		}
	}

	const SSUPERIORWAR_RANK_SELF& sMyRank = GLGaeaClient::GetInstance().m_SuperiorWarSelfRank;

	if ( sMyRank.bVALID )
	{
		CString strNAME = GLGaeaClient::GetInstance().GetCharacter()->m_szName;
		DWORD dwCHARID = GLGaeaClient::GetInstance().GetCharacter()->m_dwCharID;
		if ( m_pRankSelf )
		{
			m_pRankSelf->SetVisibleSingle( TRUE );
			m_pRankSelf->DataUpdate( sMyRank.nRanking, strNAME.GetString(), sMyRank.nSCHOOL, sMyRank.nCLASS, sMyRank.wPoints, dwCHARID );
		}
	}
}

