#pragma	once

#include "../EngineUiLib/GUInterface/UIGroup.h"
#include "GLCharDefine.h"
#include "GLParty.h"
#include "../EngineUiLib/GUInterface/BasicTextBox.h"
#include "../EngineUiLib/GUInterface/BasicProgressBar.h"

class	CBasicTextBox;
class	CBasicProgressBar;
class	CBasicLineBox;

class	CSuperiorWarRankSlot : public CUIGroup
{
public:
	CSuperiorWarRankSlot ();
	virtual	~CSuperiorWarRankSlot ();

public:
	void	CreateSubControl ();

private:
	CBasicTextBox*		m_pRankNum;
	CUIControl*			m_pRankSchool[3];
	CUIControl*			m_pRankClass[GLCI_NUM_2012];
	CBasicTextBox*		m_pRankName;
	CBasicTextBox*		m_pRankPoints;
	CUIControl*			m_pSelf;

protected:
	CUIControl*		CreateControl ( const char* szControl );
	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR D3DCOLOR, int nAlign );

public:
	void	DataUpdate( int nNUM, std::string strNAME, int nSCHOOL, int nCLASS, int nPOINTS , DWORD dwCHARID = UINT_MAX );
	void	DataReset();
};