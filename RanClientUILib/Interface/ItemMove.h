#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "GLDefine.h"
#include "../RanClientLib/G-Logic/GLItem.h"

class	CItemImage;

class CItemMove : public CUIGroup
{
protected:
static const DWORD	dwDEFAULT_TRANSPARENCY;
static const float	fDEFAULT_MOUSE_INTERPOLIATION;

public:
	CItemMove ();
	virtual	~CItemMove ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

public:
	void	SetItemIcon ( SNATIVEID sICONINDEX, const char* szTexture, SITEMCUSTOM sCustom );
	SNATIVEID	GetItemIcon ();
	const CString&	GetItemIconTexture () const;
	void	ResetItemIcon ();
	void	SetItemImageEx();
	void	ReSetItemImageEx();

public:
	SNATIVEID	GetItem ();

private:
	CItemImage*	m_pItemImage;
	CUIControl* m_pWrap;
	CUIControl*		m_pItemImageEx;

private:
	BOOL	m_bUseSnap;
};