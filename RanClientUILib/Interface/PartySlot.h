#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "GLCharDefine.h"
#include "GLParty.h"

const DWORD UIMSG_MOUSEIN_LBUP_EVENT = UIMSG_USER1;
const DWORD UIMSG_MOUSEIN_RBUP_EVENT = UIMSG_USER2;
const DWORD UIMSG_MOUSEIN_LBUP_AUTH = UIMSG_USER3;

class	CBasicTextBox;
class	CBasicTextButton;
class	CBasicProgressBar;
class	CBasicLineBox;

class	CPartySlot : public CUIGroup
{
protected:
	enum
	{
		PARTY_QUIT_BUTTON = NO_ID + 1,
		PARTY_AUTH_BUTTON,
		PARTY_HP_BAR,
	};

public:
	CPartySlot ();
	virtual	~CPartySlot ();

public:
	void	CreateSubControl ();

public:
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );

public:
	void	SetPlayerName ( CString strName );
	void	SetPlayerPos ( CString strPos );
	void	SetHP ( float fPercent );
	void	SetMapName ( CString strMapName );
	void	SetClass ( int nClassType );
	void	SetVisibleQuitButton ( BOOL bVisible );
	void	SetVisibleAuthButton( BOOL bVisible );

public:
	void	SetMaster ( BOOL bMaster );

public:
	void	SetSameMap ( BOOL bSameMap );
	void	SetNumber ( int nSlotNumber );

protected:
	CUIControl*		CreateControl ( const char* szControl );
	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR D3DCOLOR, int nAlign );
	CBasicTextButton*	 CreateTextButton24 ( char* szButton, UIGUID ControlID, char* szText );

private:
	CBasicTextBox*	m_pPlayerName;
	CBasicTextBox*	m_pPosText;
	CBasicTextButton*	m_pQuitButton;
	CBasicTextButton*	m_pAuthorityButton;

	CBasicProgressBar*	m_pHP;
	CUIControl*			m_pHPLineBack;
	
	CBasicTextBox*		m_pMapText;
	CUIControl*			m_pMapLineBack;
	//add class
	CUIControl*			m_pClass[GLCI_NUM_2012];

	BOOL	m_bMaster;

	CUIControl*			m_pMaster;
	CBasicTextBox*		m_pNumberSlot;
	CBasicLineBox*  m_pBackLine1;


};