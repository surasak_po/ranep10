#include "StdAfx.h"
#include "BasicEscWindow.h"

#include "../EngineUiLib/GUInterface/BasicTextBox.h"
#include "../EngineUiLib/GUInterface/BasicProgressBar.h"
#include "../EngineUiLib/GUInterface/BasicButton.h"
#include "../EngineLib/DxCommon/d3dfont.h"

#include "BasicTextButton.h"
#include "UITextControl.h"
#include "GameTextControl.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"
#include "InnerInterface.h"
#include "BasicEscMenu.h"
#include "BasicLineBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CBasicEscWindow::CBasicEscWindow () :
	m_pEscMenu(NULL)

{
}

CBasicEscWindow::~CBasicEscWindow ()
{
}

void CBasicEscWindow::CreateSubControl ()
{
	m_pEscMenu = new CBasicEscMenu;
	m_pEscMenu->CreateSub ( this, "ESC_MENU_POS", UI_FLAG_CENTER_X | UI_FLAG_CENTER_Y, PAGE_MENU );
	m_pEscMenu->CreateSubControl ();
	RegisterControl ( m_pEscMenu );
}

CBasicTextButton*  CBasicEscWindow::CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE23;
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON23", UI_FLAG_XSIZE|UI_FLAG_YSIZE, ControlID );
	pTextButton->CreateBaseButtonEx ( szButton, nBUTTONSIZE, CBasicButton::RADIO_FLIP, szText );
	RegisterControl ( pTextButton );
	return pTextButton;
}

void CBasicEscWindow::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );

	switch ( ControlID )
	{
	case PAGE_MENU:
		{
			if ( (dwMsg & UIMSG_LB_DUP) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( ESC_WINDOW );
			}
		}
		break;
	}
}


void CBasicEscWindow::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{	
	CUIWindow::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}

void CBasicEscWindow::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindow::SetVisibleSingle( bVisible );

}