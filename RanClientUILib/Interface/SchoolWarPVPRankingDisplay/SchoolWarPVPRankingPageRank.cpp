#include "StdAfx.h"
#include "SchoolWarPVPRankingPageRank.h"
#include "SchoolWarPVPRankingSlot.h"

#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "../BasicScrollBarEx.h"
#include "../EngineUiLib/GUInterface/BasicScrollThumbFrame.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarPVPRankingPageRank::CSchoolWarPVPRankingPageRank () :
	m_pSlotOwn( NULL )
	, m_pScrollBar( NULL )
	, m_nStart( 0 )
	, m_nTotal( 0 )
{
	for ( int i=0; i < SCHOOLWARPVP_SCHOOL_SLOT_MAX; ++ i )
	{
		m_pSlotSchool[i] = NULL;
	}

	for ( int i=0; i < SCHOOLWARPVP_PLAYER_SLOT_MAX; ++ i )
	{
		m_pSlotPlayer[i] = NULL;
	}
}

CSchoolWarPVPRankingPageRank::~CSchoolWarPVPRankingPageRank ()
{
}

void CSchoolWarPVPRankingPageRank::CreateSubControl ()
{
	CBasicLineBox* pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_RANKING_PAGE_SCHOOL_LINE" );
	RegisterControl ( pBasicLineBox );

	pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_RANKING_PAGE_PLAYER_LINE" );
	RegisterControl ( pBasicLineBox );

	pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_RANKING_PAGE_SELF_LINE" );
	RegisterControl ( pBasicLineBox );

	std::string strSLOT_SCHOOL[SCHOOLWARPVP_SCHOOL_SLOT_MAX] = 
	{
		"SCHOOLWARPVP_RANKING_SCHOOL_SLOT_00",
		"SCHOOLWARPVP_RANKING_SCHOOL_SLOT_01",
		"SCHOOLWARPVP_RANKING_SCHOOL_SLOT_02",
	};

	std::string strSLOT_PLAYER[SCHOOLWARPVP_PLAYER_SLOT_MAX] = 
	{
		"SCHOOLWARPVP_RANKING_SLOT_00",
		"SCHOOLWARPVP_RANKING_SLOT_01",
		"SCHOOLWARPVP_RANKING_SLOT_02",
		"SCHOOLWARPVP_RANKING_SLOT_03",
		"SCHOOLWARPVP_RANKING_SLOT_04",
		"SCHOOLWARPVP_RANKING_SLOT_05",
		"SCHOOLWARPVP_RANKING_SLOT_06",
		"SCHOOLWARPVP_RANKING_SLOT_07",
		"SCHOOLWARPVP_RANKING_SLOT_08",
		"SCHOOLWARPVP_RANKING_SLOT_09",
	};

	for ( int i=0; i < SCHOOLWARPVP_SCHOOL_SLOT_MAX; ++ i )
	{
		m_pSlotSchool[i] = new CSchoolWarPVPRankingSlot;
		m_pSlotSchool[i]->CreateSub ( this, strSLOT_SCHOOL[i].c_str() );
		m_pSlotSchool[i]->CreateSubControl ();
		RegisterControl ( m_pSlotSchool[i] );	
		m_pSlotSchool[i]->SetVisibleSingle( FALSE );
	}

	for ( int i=0; i < SCHOOLWARPVP_PLAYER_SLOT_MAX; ++ i )
	{
		m_pSlotPlayer[i] = new CSchoolWarPVPRankingSlot;
		m_pSlotPlayer[i]->CreateSub ( this, strSLOT_PLAYER[i].c_str() );
		m_pSlotPlayer[i]->CreateSubControl ();
		RegisterControl ( m_pSlotPlayer[i] );	
		m_pSlotPlayer[i]->SetVisibleSingle( FALSE );
	}

	m_pSlotOwn =  new CSchoolWarPVPRankingSlot;
	m_pSlotOwn->CreateSub ( this, "SCHOOLWARPVP_RANKING_SLOT_MYSCORE" );
	m_pSlotOwn->CreateSubControl ();
	RegisterControl ( m_pSlotOwn );	
	m_pSlotOwn->SetVisibleSingle( FALSE );

	m_pScrollBar= new CBasicScrollBarEx;
	m_pScrollBar->CreateSub ( this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE );
	m_pScrollBar->CreateBaseScrollBar ( "SCHOOLWARPVP_RANKING_SLOT_SCROLL" );
	m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWARPVP_PLAYER_SLOT_MAX );
	RegisterControl ( m_pScrollBar );
}

void CSchoolWarPVPRankingPageRank::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarPVPRankingPlayerVec.size();

	if ( m_pScrollBar )
	{
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal();

			if( nTotal != m_nTotal )
				m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWARPVP_PLAYER_SLOT_MAX );	
		}
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal ();
			if ( nTotal <= SCHOOLWARPVP_PLAYER_SLOT_MAX ) return ;
			const int nViewPerPage = pThumbFrame->GetViewPerPage ();

			if ( nViewPerPage < nTotal )
			{
				int nCurPos = 0;
				const int nMovableLine = nTotal - nViewPerPage;
				float fPercent = pThumbFrame->GetPercent ();
				nCurPos = (int)floor(fPercent * nMovableLine);
				if ( nCurPos < 0 ) nCurPos = 0;
				if ( m_nStart == nCurPos ) return;
				m_nStart = nCurPos;
				RenderView();
			}
		}
	}
}

void CSchoolWarPVPRankingPageRank::RefreshRanking()
{
	if ( m_pSlotOwn )
	{
		m_pSlotOwn->DataReset();
		m_pSlotOwn->SetVisibleSingle( FALSE );
	}

	for ( int i=0; i < SCHOOLWARPVP_SCHOOL_SLOT_MAX; ++ i )
	{
		if ( m_pSlotSchool[i] )
			m_pSlotSchool[i]->SetVisibleSingle( FALSE );
	}

	int nRankNum = (int)GLGaeaClient::GetInstance().m_SchoolWarPVPRankingSchoolVec.size();
	SCHOOLWARPVP_RANK_SCHOOL_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SchoolWarPVPRankingSchoolVec;

	for ( int i = 0; i < nRankNum; ++i )
	{
		if ( i >= SCHOOLWARPVP_SCHOOL_SLOT_MAX )	continue;

		if ( m_pSlotSchool[i] )
		{
			SSCHOOLWARPVP_RANK_SCHOOL_CLIENT sRANK = vecRank[i];
			m_pSlotSchool[i]->SetVisibleSingle( TRUE );
			m_pSlotSchool[i]->DataUpdate( sRANK.wRanking, GLCONST_CHAR::strSCHOOLNAME[sRANK.wSCHOOL].c_str(), sRANK.wSCHOOL, sRANK.wKillNum, sRANK.wDeathNum );
		}
	}

	if ( m_pSlotOwn )
	{
		const SSCHOOLWARPVP_RANK_SELF& sMyRank = GLGaeaClient::GetInstance().m_SchoolWarPVPSelfRank;

		if ( sMyRank.bVALID )
		{
			CString strNAME = GLGaeaClient::GetInstance().GetCharacter()->m_szName;
			m_pSlotOwn->SetVisibleSingle( TRUE );
			m_pSlotOwn->DataUpdate( sMyRank.wRanking, strNAME.GetString(), sMyRank.wSCHOOL, sMyRank.wKillNum, sMyRank.wDeathNum );
		}
	}

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarPVPRankingPlayerVec.size();

	RenderView();
}

void CSchoolWarPVPRankingPageRank::RenderView()
{
	for ( int i=0; i < SCHOOLWARPVP_PLAYER_SLOT_MAX; ++ i )
	{
		if ( m_pSlotPlayer[i] )
		{
			m_pSlotPlayer[i]->DataReset();
			m_pSlotPlayer[i]->SetVisibleSingle( FALSE );
		}
	}

	SCHOOLWARPVP_RANK_PLAYER_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SchoolWarPVPRankingPlayerVec;

	int nEnd = m_nStart + SCHOOLWARPVP_PLAYER_SLOT_MAX;
	int nSlot = 0;

	for ( int i = (int)m_nStart; i < nEnd; ++ i )
	{
		if ( i >= (int)vecRank.size() )	continue;
		if ( m_pSlotPlayer[nSlot] )
		{
			SSCHOOLWARPVP_RANK_PLAYER_CLIENT sRANK = vecRank[i];
			m_pSlotPlayer[nSlot]->SetVisibleSingle( TRUE );
			m_pSlotPlayer[nSlot]->DataUpdate( sRANK.wRanking, sRANK.szCharName, sRANK.wSCHOOL, sRANK.wKillNum, sRANK.wDeathNum, sRANK.dwCharID );
			nSlot ++ ;
		}
	}
}
