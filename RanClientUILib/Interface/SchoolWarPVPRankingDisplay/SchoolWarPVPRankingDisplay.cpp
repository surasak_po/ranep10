#include "StdAfx.h"
#include "SchoolWarPVPRankingDisplay.h"
#include "SchoolWarPVPRankingPageRank.h"
#include "SchoolWarPVPRankingPageWinner.h"

#include "../InnerInterface.h"
#include "../BasicTextButton.h"
#include "../GameTextControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarPVPRankingDisplay::CSchoolWarPVPRankingDisplay () :
	m_nActivePage(0),
	m_pBackGround( NULL ),
	m_pButtonRank( NULL ),
	m_pButtonWinner( NULL ),
	m_pPageRank( NULL ),
	m_pPageWinner( NULL )
{
	
}

CSchoolWarPVPRankingDisplay::~CSchoolWarPVPRankingDisplay ()
{
}

CBasicTextButton*  CSchoolWarPVPRankingDisplay::CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE23;
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON23", UI_FLAG_XSIZE|UI_FLAG_YSIZE, ControlID );
	pTextButton->CreateBaseButtonEx ( szButton, nBUTTONSIZE, CBasicButton::RADIO_FLIP, szText );
	RegisterControl ( pTextButton );
	return pTextButton;
}

void CSchoolWarPVPRankingDisplay::CreateSubControl ()
{	
	m_pBackGround = new CUIControl;
	m_pBackGround->CreateSub ( this, "SCHOOLWARPVP_RANKING_WINDOW_REGION", UI_FLAG_DEFAULT );	
	m_pBackGround->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pBackGround );

	m_pButtonRank = CreateTextButton23 ( "SCHOOLWARPVP_RANKING_BUTTON_RANK", SCHOOLWARPVP_RANKING_BUTTON_RANK, (char*)ID2GAMEWORD ( "SCHOOLWARPVP_RANKING_WINDOW_TEXTS", 1 ) );
	m_pButtonWinner = CreateTextButton23 ( "SCHOOLWARPVP_RANKING_BUTTON_WINNER", SCHOOLWARPVP_RANKING_BUTTON_WINNER, (char*)ID2GAMEWORD ( "SCHOOLWARPVP_RANKING_WINDOW_TEXTS", 2 ) );

	m_pPageRank = new CSchoolWarPVPRankingPageRank;
	m_pPageRank->CreateSub ( this, "SCHOOLWARPVP_RANKING_PAGE" );
	m_pPageRank->CreateSubControl ();
	RegisterControl ( m_pPageRank );

	m_pPageWinner = new CSchoolWarPVPRankingPageWinner;
	m_pPageWinner->CreateSub ( this, "SCHOOLWARPVP_RANKING_PAGE" );
	m_pPageWinner->CreateSubControl ();
	RegisterControl ( m_pPageWinner );
}

void CSchoolWarPVPRankingDisplay::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	switch ( ControlID )
	{
	case ET_CONTROL_TITLE:
	case ET_CONTROL_TITLE_F:
		{
			if ( (dwMsg & UIMSG_LB_DUP) && CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( SCHOOLWARPVP_RANKING_DISPLAY );
			}
		}break;
	case ET_CONTROL_BUTTON:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				CInnerInterface::GetInstance().HideGroup ( GetWndID () );
			}
		}break;

	case SCHOOLWARPVP_RANKING_BUTTON_RANK:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 1 );
		}break;

	case SCHOOLWARPVP_RANKING_BUTTON_WINNER:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 2 );
		}break;

	}

	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );
}

void CSchoolWarPVPRankingDisplay::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindowEx::SetVisibleSingle( bVisible );

	if( bVisible )
	{
		OpenPage( 1 );
	}
}

void CSchoolWarPVPRankingDisplay::RefreshRanking()
{
	if ( m_pPageRank )
	{
		m_pPageRank->RefreshRanking();
	}

	if ( m_pPageWinner )
	{
		m_pPageWinner->RefreshWinners();
	}
}

void CSchoolWarPVPRankingDisplay::OpenPage( int nPage )
{
	//if ( nPage == m_nActivePage )	return;

	if ( m_pButtonRank )	m_pButtonRank->SetFlip ( FALSE );
	if ( m_pButtonWinner )	m_pButtonWinner->SetFlip ( FALSE );

	if ( m_pPageRank )		m_pPageRank->SetVisibleSingle( FALSE );
	if ( m_pPageWinner )	m_pPageWinner->SetVisibleSingle( FALSE );


	switch( nPage )
	{
	case 1:
		{
			if ( m_pButtonRank )	m_pButtonRank->SetFlip ( TRUE );

			if ( m_pPageRank )	
			{
				m_pPageRank->SetVisibleSingle( TRUE );
				m_pPageRank->RefreshRanking();
			}
			//m_nActivePage = nPage;
		}break;
	case 2:
		{
			if ( m_pButtonWinner )	m_pButtonWinner->SetFlip ( TRUE );
			if ( m_pPageWinner )	
			{
				m_pPageWinner->SetVisibleSingle( TRUE );
				m_pPageWinner->RefreshWinners();
			}
			//m_nActivePage = nPage;
		}break;
	};
}