#include "StdAfx.h"
#include "SchoolWarPVPRankingSlot.h"
#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarPVPRankingSlot::CSchoolWarPVPRankingSlot () :
	m_pRankNum( NULL ),
	m_pRankName( NULL ),
	m_pRankKillIcon( NULL ),
	m_pRankKill( NULL ),
	m_pRankDeathIcon( NULL ),
	m_pRankDeath( NULL ),
	m_pSelf( NULL )
{
	for ( int i=0; i<3; ++i )
	{
		m_pRankSchool[i] = NULL;
	}
}

CSchoolWarPVPRankingSlot::~CSchoolWarPVPRankingSlot ()
{
}

void CSchoolWarPVPRankingSlot::CreateSubControl ()
{
	CD3DFontPar* pFont8 = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 8, D3DFONT_SHADOW | D3DFONT_ASCII );

	CBasicLineBox* pLineBox = new CBasicLineBox;
	pLineBox->CreateSub ( this, "BASIC_LINE_BOX_MINIPARTY", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_RANKING_SLOT_LINE" );
	RegisterControl ( pLineBox );

	m_pSelf = new CUIControl;
	m_pSelf->CreateSub ( this, "SCHOOLWARPVP_RANKING_SLOT_SELF", UI_FLAG_DEFAULT );	
	m_pSelf->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pSelf );

	CString strSchool[3] = 
	{
		"SCHOOLWARPVP_RANKING_SLOT_SCHOOL_SM",
		"SCHOOLWARPVP_RANKING_SLOT_SCHOOL_HA",
		"SCHOOLWARPVP_RANKING_SLOT_SCHOOL_BH"
	};

	for ( int i = 0; i < 3; ++ i )
	{
		m_pRankSchool[i] = CreateControl( strSchool[i].GetString() );
	}

	m_pRankNum = CreateStaticControl ( "SCHOOLWARPVP_RANKING_SLOT_NUM_TEXT", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
	m_pRankName = CreateStaticControl ( "SCHOOLWARPVP_RANKING_SLOT_NAME", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X );
	m_pRankKill = CreateStaticControl ( "SCHOOLWARPVP_RANKING_SLOT_KILL_TEXT", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
	m_pRankDeath = CreateStaticControl ( "SCHOOLWARPVP_RANKING_SLOT_DEATH_TEXT", pFont8, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
	m_pRankKillIcon = CreateControl ( "SCHOOLWARPVP_RANKING_SLOT_KILL_ICON" );
	m_pRankDeathIcon = CreateControl ( "SCHOOLWARPVP_RANKING_SLOT_DEATH_ICON" );
}

CUIControl*	CSchoolWarPVPRankingSlot::CreateControl ( const char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}

CBasicTextBox* CSchoolWarPVPRankingSlot::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}

void CSchoolWarPVPRankingSlot::DataUpdate( int nNUM, std::string strNAME, int nSCHOOL, int nKILL, int nDEATH , DWORD dwCHARID /*= UINT_MAX*/ )
{
	DataReset();

	if ( nSCHOOL < 0 || nSCHOOL >=3 )	return;

	if ( m_pRankSchool[nSCHOOL] )
		m_pRankSchool[nSCHOOL]->SetVisibleSingle( TRUE );

	if ( m_pRankNum )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nNUM );
		m_pRankNum->SetText( strTEXT.GetString(), NS_UITEXTCOLOR::WHITE );
	}

	if ( m_pRankName )
	{
		m_pRankName->SetText( strNAME.c_str(), NS_UITEXTCOLOR::WHITE );
	}

	if ( m_pRankKill )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nKILL );
		m_pRankKill->SetText( strTEXT.GetString(), NS_UITEXTCOLOR::BRIGHTGREEN );
	}

	if ( m_pRankDeath )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nDEATH );
		m_pRankDeath->SetText( strTEXT.GetString(), NS_UITEXTCOLOR::RED );
	}

	if ( dwCHARID != UINT_MAX )
	{
		DWORD dwCharID = GLGaeaClient::GetInstance().GetCharacter()->m_dwCharID;

		BOOL bSELF = ( dwCHARID == dwCharID );

		if ( m_pSelf )
			m_pSelf->SetVisibleSingle( bSELF );
	}
}

void CSchoolWarPVPRankingSlot::DataReset()
{
	for ( int i = 0; i < 3; ++ i )
	{
		if ( m_pRankSchool[i] )
			m_pRankSchool[i]->SetVisibleSingle( FALSE );
	}

	if ( m_pRankNum )	m_pRankNum->ClearText();
	if ( m_pRankName )	m_pRankName->ClearText();
	if ( m_pRankKill )	m_pRankKill->ClearText();
	if ( m_pRankDeath ) m_pRankDeath->ClearText();
}