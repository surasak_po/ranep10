#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"

#include "GLSchoolWarPVP/GLSchoolWarPVPData.h"
#include "GLSchoolWarPVP/GLSchoolWarPVP.h"
#include "GLSchoolWarPVP/GLSchoolWarPVPMan.h"

class	CSchoolWarPVPRankingSlot;
class	CBasicScrollBarEx;

class	CSchoolWarPVPRankingPageWinner : public CUIGroup
{
public:
	enum
	{
		SCHOOLWARPVP_WINNER_SLOT_MAX = 15,
	};

	CSchoolWarPVPRankingPageWinner ();
	virtual	~CSchoolWarPVPRankingPageWinner ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CSchoolWarPVPRankingSlot*		m_pWinnerSchool;
	CSchoolWarPVPRankingSlot*		m_pWinnerSlot[SCHOOLWARPVP_WINNER_SLOT_MAX];
	CBasicScrollBarEx*			m_pScrollBar;

	int		m_nStart;
	int		m_nTotal;

public:
	void	RefreshWinners();
	void	RenderView();
};