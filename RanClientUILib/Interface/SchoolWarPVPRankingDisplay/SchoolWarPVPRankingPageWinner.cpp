#include "StdAfx.h"
#include "SchoolWarPVPRankingPageWinner.h"
#include "SchoolWarPVPRankingSlot.h"

#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "../BasicScrollBarEx.h"
#include "../EngineUiLib/GUInterface/BasicScrollThumbFrame.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarPVPRankingPageWinner::CSchoolWarPVPRankingPageWinner () :
	m_pWinnerSchool( NULL )
	, m_pScrollBar( NULL )
	, m_nStart( 0 )
	, m_nTotal( 0 )
{
	for ( int i=0; i < SCHOOLWARPVP_WINNER_SLOT_MAX; ++ i )
	{
		m_pWinnerSlot[i] = NULL;
	}
}

CSchoolWarPVPRankingPageWinner::~CSchoolWarPVPRankingPageWinner ()
{
}

void CSchoolWarPVPRankingPageWinner::CreateSubControl ()
{
	CBasicLineBox* pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_WINNER_PAGE_LINE" );
	RegisterControl ( pBasicLineBox );

	pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWARPVP_WINNER_PAGE_LINE_SCHOOL" );
	RegisterControl ( pBasicLineBox );

	std::string strSLOT[SCHOOLWARPVP_WINNER_SLOT_MAX] = 
	{
		"SCHOOLWARPVP_WINNER_SLOT_00",
		"SCHOOLWARPVP_WINNER_SLOT_01",
		"SCHOOLWARPVP_WINNER_SLOT_02",
		"SCHOOLWARPVP_WINNER_SLOT_03",
		"SCHOOLWARPVP_WINNER_SLOT_04",
		"SCHOOLWARPVP_WINNER_SLOT_05",
		"SCHOOLWARPVP_WINNER_SLOT_06",
		"SCHOOLWARPVP_WINNER_SLOT_07",
		"SCHOOLWARPVP_WINNER_SLOT_08",
		"SCHOOLWARPVP_WINNER_SLOT_09",
		"SCHOOLWARPVP_WINNER_SLOT_10",
		"SCHOOLWARPVP_WINNER_SLOT_11",
		"SCHOOLWARPVP_WINNER_SLOT_12",
		"SCHOOLWARPVP_WINNER_SLOT_13",
		"SCHOOLWARPVP_WINNER_SLOT_14",
	};

	for ( int i=0; i < SCHOOLWARPVP_WINNER_SLOT_MAX; ++ i )
	{
		m_pWinnerSlot[i] = new CSchoolWarPVPRankingSlot;
		m_pWinnerSlot[i]->CreateSub ( this, strSLOT[i].c_str() );
		m_pWinnerSlot[i]->CreateSubControl ();
		RegisterControl ( m_pWinnerSlot[i] );	
		m_pWinnerSlot[i]->SetVisibleSingle( FALSE );
	}

	m_pWinnerSchool =  new CSchoolWarPVPRankingSlot;
	m_pWinnerSchool->CreateSub ( this, "SCHOOLWARPVP_WINNER_SCHOOL" );
	m_pWinnerSchool->CreateSubControl ();
	RegisterControl ( m_pWinnerSchool );	
	m_pWinnerSchool->SetVisibleSingle( FALSE );

	m_pScrollBar= new CBasicScrollBarEx;
	m_pScrollBar->CreateSub ( this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE );
	m_pScrollBar->CreateBaseScrollBar ( "SCHOOLWARPVP_WINNER_SLOT_SCROLL" );
	m_pScrollBar->GetThumbFrame()->SetState ( 0, SCHOOLWARPVP_WINNER_SLOT_MAX );
	RegisterControl ( m_pScrollBar );
}

void CSchoolWarPVPRankingPageWinner::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarPVPWinnerPlayerVec.size();

	if ( m_pScrollBar )
	{
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal();

			if( nTotal != m_nTotal )
				m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWARPVP_WINNER_SLOT_MAX );	
		}
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal ();
			if ( nTotal <= SCHOOLWARPVP_WINNER_SLOT_MAX ) return ;
			const int nViewPerPage = pThumbFrame->GetViewPerPage ();

			if ( nViewPerPage < nTotal )
			{
				int nCurPos = 0;
				const int nMovableLine = nTotal - nViewPerPage;
				float fPercent = pThumbFrame->GetPercent ();
				nCurPos = (int)floor(fPercent * nMovableLine);
				if ( nCurPos < 0 ) nCurPos = 0;
				if ( m_nStart == nCurPos ) return;
				m_nStart = nCurPos;
				RenderView();
			}
		}
	}
}

void CSchoolWarPVPRankingPageWinner::RefreshWinners()
{
	if ( m_pWinnerSchool )
	{
		m_pWinnerSchool->DataReset();
		m_pWinnerSchool->SetVisibleSingle( FALSE );
		const SSCHOOLWARPVP_RANK_SCHOOL_WINNER& sWinner = GLGaeaClient::GetInstance().m_SchoolWarPVPSchoolWinner;
		if ( sWinner.bVALID )
		{
			m_pWinnerSchool->SetVisibleSingle( TRUE );
			CString strSCHOOL = GLCONST_CHAR::strSCHOOLNAME[sWinner.wSCHOOL].c_str();
			m_pWinnerSchool->DataUpdate( sWinner.wRanking, strSCHOOL.GetString(), sWinner.wSCHOOL, sWinner.wKillNum, sWinner.wDeathNum );
		}
	}

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarPVPWinnerPlayerVec.size();

	RenderView();
}

void CSchoolWarPVPRankingPageWinner::RenderView()
{
	for ( int i=0; i < SCHOOLWARPVP_WINNER_SLOT_MAX; ++ i )
	{
		if ( m_pWinnerSlot[i] )
		{
			m_pWinnerSlot[i]->DataReset();
			m_pWinnerSlot[i]->SetVisibleSingle( FALSE );
		}
	}

	SCHOOLWARPVP_RANK_PLAYER_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SchoolWarPVPWinnerPlayerVec;

	int nEnd = m_nStart + SCHOOLWARPVP_WINNER_SLOT_MAX;
	int nSlot = 0;

	for ( int i = m_nStart; i < nEnd; ++ i )
	{
		if ( i >= (int)vecRank.size() )	continue;
		if ( m_pWinnerSlot[nSlot] )
		{
			SSCHOOLWARPVP_RANK_PLAYER_CLIENT sRANK = vecRank[i];
			m_pWinnerSlot[nSlot]->SetVisibleSingle( TRUE );
			m_pWinnerSlot[nSlot]->DataUpdate( sRANK.wRanking, sRANK.szCharName, sRANK.wSCHOOL, sRANK.wKillNum, sRANK.wDeathNum, sRANK.dwCharID );
			nSlot ++ ;
		}
	}
}
