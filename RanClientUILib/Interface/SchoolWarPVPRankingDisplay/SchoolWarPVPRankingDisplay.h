#pragma	once

#include "../UIWindowEx.h"

class	CBasicTextButton;
class	CSchoolWarPVPRankingPageRank;
class	CSchoolWarPVPRankingPageWinner;

class	CSchoolWarPVPRankingDisplay : public CUIWindowEx
{
protected:
	enum
	{
		SCHOOLWARPVP_RANKING_BACKGROUND = ET_CONTROL_NEXT,
		SCHOOLWARPVP_RANKING_PAGE_RANK,
		SCHOOLWARPVP_RANKING_PAGE_WINNER,
		SCHOOLWARPVP_RANKING_BUTTON_RANK,
		SCHOOLWARPVP_RANKING_BUTTON_WINNER,
	};

public:
	CSchoolWarPVPRankingDisplay ();
	virtual	~CSchoolWarPVPRankingDisplay ();

public:
	CBasicTextButton*	CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText );
	void CreateSubControl ();

private:
	int								m_nActivePage;
	CUIControl*						m_pBackGround;
	CBasicTextButton*				m_pButtonRank;
	CBasicTextButton*				m_pButtonWinner;
	CSchoolWarPVPRankingPageRank*		m_pPageRank;
	CSchoolWarPVPRankingPageWinner*	m_pPageWinner;

public:
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual	void SetVisibleSingle ( BOOL bVisible );

public:
	void	RefreshRanking();
	void	OpenPage( int nPage );

};