#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"

#include "GLSchoolWarPVP/GLSchoolWarPVPData.h"
#include "GLSchoolWarPVP/GLSchoolWarPVP.h"
#include "GLSchoolWarPVP/GLSchoolWarPVPMan.h"

class	CSchoolWarPVPRankingSlot;
class	CBasicScrollBarEx;

class	CSchoolWarPVPRankingPageRank : public CUIGroup
{
public:
	enum
	{
		SCHOOLWARPVP_SCHOOL_SLOT_MAX = 3,
		SCHOOLWARPVP_PLAYER_SLOT_MAX = 10,
	};

	CSchoolWarPVPRankingPageRank ();
	virtual	~CSchoolWarPVPRankingPageRank ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CSchoolWarPVPRankingSlot*	m_pSlotSchool[SCHOOLWARPVP_SCHOOL_SLOT_MAX];
	CSchoolWarPVPRankingSlot*	m_pSlotPlayer[SCHOOLWARPVP_PLAYER_SLOT_MAX];
	CSchoolWarPVPRankingSlot*	m_pSlotOwn;
	CBasicScrollBarEx*		m_pScrollBar;

	int		m_nStart;
	int		m_nTotal;
	
public:
	void	RefreshRanking();
	void	RenderView();
};