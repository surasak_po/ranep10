#include "StdAfx.h"
#include "BasicOpenMenu.h"

#include "InnerInterface.h"
#include "BasicVarTextBox.h"
#include "UITextControl.h"
#include "GameTextControl.h"
#include "RANPARAM.h"
#include "ModalWindow.h"
#include "InventoryWindow.h"
#include "BasicLineBox.h"
#include "../EngineLib/DxCommon/d3dfont.h"
#include "../EngineUILib/GUInterface/BasicButtonText.h"
#include "BasicTextBoxEx.h"
#include "BasicTextButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CBasicOpenMenu::CBasicOpenMenu ()
	: m_Button(NULL)
	, m_ButtonSet (NULL)
	, m_ButtonText (NULL)
	
{

}

CBasicOpenMenu::~CBasicOpenMenu ()
{
}

CBasicTextButton* CBasicOpenMenu::CreateTextButton ( char* szButton, UIGUID ControlID , char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE19;
	CBasicTextButton* pButton = new CBasicTextButton;
	pButton->CreateSub( this, "BASIC_TEXT_BUTTON19", UI_FLAG_XSIZE, ControlID );
	pButton->CreateBaseButton( szButton, nBUTTONSIZE, CBasicButton::CLICK_FLIP, szText );
	RegisterControl( pButton );

	return pButton;
}
void CBasicOpenMenu::CreateSubControl ()
{
	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 11, _DEFAULT_FONT_FLAG );

	const int nAlignLeft = TEXT_ALIGN_LEFT;
	const int nAlignCenter = TEXT_ALIGN_CENTER_X;
	const int nAlignCenterBoth = TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y;
	const int nAlignRight = TEXT_ALIGN_RIGHT;

	CBasicLineBox* pLineBox = new CBasicLineBox;
	pLineBox->CreateSub ( this, "BASIC_LINE_BOX_SKILL", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox->CreateBaseBoxOptionNoBody ( "OPEN_LINEBOX" );
	RegisterControl ( pLineBox );


	CBasicLineBox* pLineBox2 = new CBasicLineBox;
	pLineBox2->CreateSub ( this, "BASIC_LINE_BOX_SKILL", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox2->CreateBaseBoxOptionNewTop ( "OPEN_BACKBOX" );
	RegisterControl ( pLineBox2 );

	m_Button = new CUIControl;
	m_Button->CreateSub ( this, "GAME_MENU_OPEN", UI_FLAG_DEFAULT, GAME_MENU_OPEN_BUTTON );
	m_Button->SetVisibleSingle ( TRUE );
	m_Button->SetTransparentOption( TRUE );
	RegisterControl ( m_Button );

	m_ButtonSet = new CUIControl;
	m_ButtonSet->CreateSub ( this, "GAME_MENU_OPEN_F", UI_FLAG_DEFAULT );	
	m_ButtonSet->SetVisibleSingle ( FALSE );
	m_ButtonSet->SetTransparentOption( TRUE );
	RegisterControl ( m_ButtonSet );

	m_ButtonText = new CBasicTextBox;
	m_ButtonText->CreateSub ( this, "GAME_MENU_OPEN_BUTTON_TEXT", UI_FLAG_DEFAULT );
	m_ButtonText->SetFont ( pFont );
	m_ButtonText->SetTextAlign ( TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );	
	m_ButtonText->AddText ( "MENU", NS_UITEXTCOLOR::WHITESMOKE );
	RegisterControl ( m_ButtonText );

}

void CBasicOpenMenu::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}

void CBasicOpenMenu::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	switch ( ControlID )
	{
	case GAME_MENU_OPEN_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet->SetVisibleSingle(TRUE);
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet->SetVisibleSingle(FALSE);
			}
		}
	break;

	}
}


