#pragma	once

#include "UIWindowEx.h"
#include "GLCharDefine.h"

class	CD3DFontPar;
class	CBasicTextButton;
class	CBasicEscMenu;


class CBasicEscWindow : public CUIWindowEx
{
protected:
	enum
	{
		PAGE_MENU = ET_CONTROL_NEXT,
	};

public:
    CBasicEscWindow ();
	virtual	~CBasicEscWindow ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual	void SetVisibleSingle ( BOOL bVisible );

public:
	void	CreateSubControl ();
	CBasicTextButton*	CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText );

private:
	CBasicEscMenu*			m_pEscMenu;

};