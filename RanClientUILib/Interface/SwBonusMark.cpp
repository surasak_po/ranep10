#include "StdAfx.h"
#include "SwBonusMark.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CSwBonusMark::CSwBonusMark()
	: m_pImage( NULL )
{
}

CSwBonusMark::~CSwBonusMark()
{
}

void CSwBonusMark::CreateSubControl ()
{
	m_pImage =  new CUIControl;
	m_pImage->CreateSub ( this, "SW_BONUS_IMAGE", UI_FLAG_DEFAULT );	
	RegisterControl ( m_pImage );
	m_pImage->SetVisibleSingle(FALSE);
}

void CSwBonusMark::SetSwBonus( bool isWinner )
{
	if ( isWinner )		m_pImage->SetVisibleSingle( TRUE );
	else				m_pImage->SetVisibleSingle( FALSE );
}