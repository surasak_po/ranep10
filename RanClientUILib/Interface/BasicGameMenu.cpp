#include "StdAfx.h"
#include "BasicGameMenu.h"

#include "InnerInterface.h"
#include "BasicVarTextBox.h"
#include "UITextControl.h"
#include "GameTextControl.h"
#include "RANPARAM.h"
#include "ModalWindow.h"
#include "InventoryWindow.h"
#include "BasicLineBox.h"
#include "../EngineLib/DxCommon/d3dfont.h"
#include "../EngineUILib/GUInterface/BasicButtonText.h"
#include "BasicTextBoxEx.h"
#include "BasicTextButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CBasicGameMenu::CBasicGameMenu ()
	: m_pRunButton(NULL)
	, m_pInfo(NULL)
	, m_nOverMenu( NO_ID )
	//, m_pButton1(NULL)
	//, m_pButton2(NULL)
	//, m_pButton3(NULL)
	//, m_pButton4(NULL)
	//, m_pButton5(NULL)
	//, m_pButton6(NULL)
	//, m_pButton7(NULL)
	//, m_pButton8(NULL)
	//, m_pButton9(NULL)
	//, m_pButton10(NULL)
	//, m_pButton11(NULL)
	//, m_pButton12(NULL)
{
	memset ( m_Button, 0, sizeof ( CUIControl* ) * 12 );	
	memset ( m_ButtonSet, 0, sizeof ( CUIControl* ) * 12 );	
	memset ( m_ButtonText, 0, sizeof ( CBasicTextBox* ) * 12 );	
}

CBasicGameMenu::~CBasicGameMenu ()
{
}

CBasicTextButton* CBasicGameMenu::CreateTextButton ( char* szButton, UIGUID ControlID , char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE19;
	CBasicTextButton* pButton = new CBasicTextButton;
	pButton->CreateSub( this, "BASIC_TEXT_BUTTON19", UI_FLAG_XSIZE, ControlID );
	pButton->CreateBaseButton( szButton, nBUTTONSIZE, CBasicButton::CLICK_FLIP, szText );
	RegisterControl( pButton );

	return pButton;
}
void CBasicGameMenu::CreateSubControl ()
{
	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, _DEFAULT_FONT_FLAG );

	const int nAlignLeft = TEXT_ALIGN_LEFT;
	const int nAlignCenter = TEXT_ALIGN_CENTER_X;
	const int nAlignCenterBoth = TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y;
	const int nAlignRight = TEXT_ALIGN_RIGHT;

	InitShotCutString();



	CBasicLineBox* pLineBox = new CBasicLineBox;
	pLineBox->CreateSub ( this, "BASIC_LINE_BOX_SKILL", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox->CreateBaseBoxOptionNoBody ( "MENU_LINEBOX" );
	RegisterControl ( pLineBox );


	CBasicLineBox* pLineBox2 = new CBasicLineBox;
	pLineBox2->CreateSub ( this, "BASIC_LINE_BOX_SKILL", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox2->CreateBaseBoxOptionNewTop ( "MENU_BACKBOX" );
	RegisterControl ( pLineBox2 );
	

	

	CBasicButton* pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_INVENTORY_BUTTON", UI_FLAG_DEFAULT, MENU_INVENTORY_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_CHARACTER_BUTTON", UI_FLAG_DEFAULT, MENU_CHARACTER_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );
	
	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_SKILL_BUTTON", UI_FLAG_DEFAULT, MENU_SKILL_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_PARTY_BUTTON", UI_FLAG_DEFAULT, MENU_PARTY_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_GUILD_BUTTON", UI_FLAG_DEFAULT, MENU_GUILD_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_QUEST_BUTTON", UI_FLAG_DEFAULT, MENU_QUEST_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_FRIEND_BUTTON", UI_FLAG_DEFAULT, MENU_FRIEND_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_LARGEMAP_BUTTON", UI_FLAG_DEFAULT, MENU_LARGEMAP_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_CHATMACRO_BUTTON", UI_FLAG_DEFAULT, MENU_CHATMACRO_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );
	
	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_ITEMBANK_BUTTON", UI_FLAG_DEFAULT, MENU_ITEMBANK_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	pButton = new CBasicButton;
	pButton->CreateSub ( this, "MENU_ITEMSHOP_BUTTON", UI_FLAG_DEFAULT, MENU_ITEMSHOP_BUTTON );
	//pButton->CreateFlip ( "MENU_OVER_IMAGE", CBasicButton::MOUSEIN_FLIP );
	pButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( pButton );

	m_pRunButton = new CBasicButton;
	m_pRunButton->CreateSub ( this, "MENU_RUN_BUTTON", UI_FLAG_DEFAULT, MENU_RUN_BUTTON );
	m_pRunButton->CreateFlip ( "MENU_RUN_BUTTON_F", CBasicButton::RADIO_FLIP );
	//m_pRunButton->CreateMouseOver( "MENU_OVER_IMAGE" );
	m_pRunButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( m_pRunButton );

	m_pInfo = new CBasicVarTextBox;
	m_pInfo->CreateSub ( this, "BASIC_VAR_TEXT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	m_pInfo->CreateSubControl ();
	m_pInfo->SetMousePointGap ( D3DXVECTOR2(-40.0f,-40.0f) );
	m_pInfo->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pInfo );


	/*m_pButton1 = CreateTextButton ( "MENU_INVENTORY_BUTTON2", MENU_INVENTORY_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 0 ) );
	m_pButton2 = CreateTextButton ( "MENU_CHARACTER_BUTTON2", MENU_CHARACTER_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 1 ) );
	m_pButton3 = CreateTextButton ( "MENU_SKILL_BUTTON2", MENU_SKILL_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 2 ) );
	m_pButton4 = CreateTextButton ( "MENU_PARTY_BUTTON2", MENU_PARTY_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 3 ) );
	m_pButton5 = CreateTextButton ( "MENU_GUILD_BUTTON2", MENU_GUILD_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 4 ) );
	m_pButton6 = CreateTextButton ( "MENU_QUEST_BUTTON2", MENU_QUEST_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 5 ) );
	m_pButton7 = CreateTextButton ( "MENU_FRIEND_BUTTON2", MENU_FRIEND_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 6 ) );
	m_pButton8 = CreateTextButton ( "MENU_LARGEMAP_BUTTON2", MENU_LARGEMAP_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 7 ) );
	m_pButton9 = CreateTextButton ( "MENU_CHATMACRO_BUTTON2", MENU_CHATMACRO_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 8 ) );
	m_pButton10 = CreateTextButton ( "MENU_ITEMBANK_BUTTON2", MENU_ITEMBANK_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 9 ) );
	m_pButton11 = CreateTextButton ( "MENU_ITEMSHOP_BUTTON2", MENU_ITEMSHOP_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 10 ) );
	m_pButton12 = CreateTextButton ( "MENU_RUN_BUTTON2", MENU_RUN_BUTTON2, (char*)ID2GAMEWORD ( "GAME_MENU_BUTTON", 11 ) );*/

	CString strButton[12] =
	{
		"MENU_INVENTORY_BUTTON2",
		"MENU_CHARACTER_BUTTON2",
		"MENU_SKILL_BUTTON2",
		"MENU_PARTY_BUTTON2",
		"MENU_GUILD_BUTTON2",
		"MENU_QUEST_BUTTON2",
		"MENU_FRIEND_BUTTON2",
		"MENU_LARGEMAP_BUTTON2",
		"MENU_CHATMACRO_BUTTON2",
		"MENU_ITEMBANK_BUTTON2",
		"MENU_ITEMSHOP_BUTTON2",
		"MENU_RUN_BUTTON2"
	};

	CString strButtonSet[12] =
	{
		"MENU_INVENTORY_BUTTON2_F",
		"MENU_CHARACTER_BUTTON2_F",
		"MENU_SKILL_BUTTON2_F",
		"MENU_PARTY_BUTTON2_F",
		"MENU_GUILD_BUTTON2_F",
		"MENU_QUEST_BUTTON2_F",
		"MENU_FRIEND_BUTTON2_F",
		"MENU_LARGEMAP_BUTTON2_F",
		"MENU_CHATMACRO_BUTTON2_F",
		"MENU_ITEMBANK_BUTTON2_F",
		"MENU_ITEMSHOP_BUTTON2_F",
		"MENU_RUN_BUTTON2_F"
	};

	CString strButtonText[12] =
	{
		"MENU_INVENTORY_BUTTON2_TEXT",
		"MENU_CHARACTER_BUTTON2_TEXT",
		"MENU_SKILL_BUTTON2_TEXT",
		"MENU_PARTY_BUTTON2_TEXT",
		"MENU_GUILD_BUTTON2_TEXT",
		"MENU_QUEST_BUTTON2_TEXT",
		"MENU_FRIEND_BUTTON2_TEXT",
		"MENU_LARGEMAP_BUTTON2_TEXT",
		"MENU_CHATMACRO_BUTTON2_TEXT",
		"MENU_ITEMBANK_BUTTON2_TEXT",
		"MENU_ITEMSHOP_BUTTON2_TEXT",
		"MENU_RUN_BUTTON2_TEXT"
	};

	for ( int i = 0; i < 12 ; i++ )
	{
		m_Button[i] = new CUIControl;
		m_Button[i]->CreateSub ( this, strButton[i].GetString (), UI_FLAG_DEFAULT, MENU_INVENTORY_BUTTON2 + i );
		m_Button[i]->SetVisibleSingle ( TRUE );
		m_Button[i]->SetTransparentOption( TRUE );
		RegisterControl ( m_Button[i] );

		m_ButtonSet[i] = new CUIControl;
		m_ButtonSet[i]->CreateSub ( this, strButtonSet[i].GetString (), UI_FLAG_DEFAULT );	
		m_ButtonSet[i]->SetVisibleSingle ( FALSE );
		m_ButtonSet[i]->SetTransparentOption( TRUE );
		RegisterControl ( m_ButtonSet[i] );

		m_ButtonText[i] = new CBasicTextBox;
		m_ButtonText[i]->CreateSub ( this, strButtonText[i].GetString (), UI_FLAG_DEFAULT );
		m_ButtonText[i]->SetFont ( pFont );
		m_ButtonText[i]->SetTextAlign ( TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );	
		m_ButtonText[i]->AddText ( ID2GAMEWORD ( "GAME_MENU_BUTTON", i ), NS_UITEXTCOLOR::WHITESMOKE );
		RegisterControl ( m_ButtonText[i] );
	}
}

void CBasicGameMenu::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	m_pInfo->SetVisibleSingle( FALSE );
	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}

void CBasicGameMenu::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	switch ( ControlID )
	{
	case MENU_INVENTORY_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[0], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( INVENTORY_WINDOW )
						&&  !CInnerInterface::GetInstance().IsVisibleGroup ( TRADEINVENTORY_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( INVENTORY_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( INVENTORY_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_CHARACTER_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[1], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CHARACTER_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( CHARACTER_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CHARACTER_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_SKILL_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[2], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SKILL_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SKILL_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SKILL_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_PARTY_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
			/*	if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[3], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( PARTY_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( PARTY_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( PARTY_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_QUEST_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[4], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( QUEST_WINDOW ) )
					{
						DWORD dwEventQuestID = CInnerInterface::GetInstance().GetEventQuestID ();
						if ( NATIVEID_NULL().dwID == dwEventQuestID )
						{
							CInnerInterface::GetInstance().ShowGroupFocus ( QUEST_WINDOW );
							CInnerInterface::GetInstance().REFRESH_QUEST_WINDOW ();							
						}
						else
						{
							CInnerInterface::GetInstance().SetQuestWindowOpen ( dwEventQuestID );
						}
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( QUEST_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_GUILD_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[5], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CLUB_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( CLUB_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CLUB_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_FRIEND_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[6], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( FRIEND_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( FRIEND_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( FRIEND_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_LARGEMAP_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[7], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( LARGEMAP_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( LARGEMAP_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( LARGEMAP_WINDOW );
					}
				}
			}
		}
		break;
	case MENU_CHATMACRO_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[11], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CHATMACRO_WINDOW ) )
					{
						CInnerInterface::GetInstance().GetChatMacro();
						CInnerInterface::GetInstance().ShowGroupFocus ( CHATMACRO_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CHATMACRO_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_ITEMBANK_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[8], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( INVENTORY_WINDOW ) )
					{
						CInnerInterface::GetInstance().SetItemBankWindowOpen ();
					}
					/*else
					{
						CInnerInterface::GetInstance().HideGroup ( ITEMBANK_WINDOW );
					}*/
				}
			}
		}
		break;

	case MENU_ITEMSHOP_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[10], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( ITEMSHOP_WINDOW ) )
					{
						// itemShopAuth
						if ( CInnerInterface::GetInstance().ItemShopAuth() )
						{
							CInnerInterface::GetInstance().ShowGroupFocus ( ITEMSHOP_WINDOW );
						}
						
						CInnerInterface::GetInstance().HideGroup ( HELP_WINDOW );								
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( ITEMSHOP_WINDOW );
					}
				}
			}
		}
		break;

	case MENU_RUN_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				/*if ( m_nOverMenu != ControlID )
				{
					m_pInfo->SetTextNoSplit ( m_ShotcutText[9], NS_UITEXTCOLOR::SILVER );
				}
				m_pInfo->SetVisibleSingle ( TRUE );
				m_nOverMenu = ControlID;*/

				if ( UIMSG_LB_UP & dwMsg )
				{
					CInnerInterface::GetInstance().ReqToggleRun ();
				}
			}
		}
		break;

	case MENU_INVENTORY_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[0]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( INVENTORY_WINDOW )
						&&  !CInnerInterface::GetInstance().IsVisibleGroup ( TRADEINVENTORY_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( INVENTORY_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( INVENTORY_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[0]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_CHARACTER_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[1]->SetVisibleSingle(TRUE);
				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CHARACTER_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( CHARACTER_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CHARACTER_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[1]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_SKILL_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[2]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( SKILL_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( SKILL_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( SKILL_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[2]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_PARTY_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[3]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( PARTY_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( PARTY_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( PARTY_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[3]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_QUEST_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[5]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( QUEST_WINDOW ) )
					{
						DWORD dwEventQuestID = CInnerInterface::GetInstance().GetEventQuestID ();
						if ( NATIVEID_NULL().dwID == dwEventQuestID )
						{
							CInnerInterface::GetInstance().ShowGroupFocus ( QUEST_WINDOW );
							CInnerInterface::GetInstance().REFRESH_QUEST_WINDOW ();							
						}
						else
						{
							CInnerInterface::GetInstance().SetQuestWindowOpen ( dwEventQuestID );
						}
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( QUEST_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[5]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_GUILD_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[4]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CLUB_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( CLUB_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CLUB_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[4]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_FRIEND_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[6]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( FRIEND_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( FRIEND_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( FRIEND_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[6]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_LARGEMAP_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[7]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( LARGEMAP_WINDOW ) )
					{
						CInnerInterface::GetInstance().ShowGroupFocus ( LARGEMAP_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( LARGEMAP_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[7]->SetVisibleSingle(FALSE);
			}
		}
		break;
	case MENU_CHATMACRO_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[8]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CHATMACRO_WINDOW ) )
					{
						CInnerInterface::GetInstance().GetChatMacro();
						CInnerInterface::GetInstance().ShowGroupFocus ( CHATMACRO_WINDOW );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CHATMACRO_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[8]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_ITEMBANK_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[9]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( INVENTORY_WINDOW ) )
					{
						CInnerInterface::GetInstance().SetItemBankWindowOpen ();
					}
					/*else
					{
						CInnerInterface::GetInstance().HideGroup ( ITEMBANK_WINDOW );
					}*/
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[9]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_ITEMSHOP_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[10]->SetVisibleSingle(TRUE);

				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( ITEMSHOP_WINDOW ) )
					{
						// itemShopAuth
						if ( CInnerInterface::GetInstance().ItemShopAuth() )
						{
							CInnerInterface::GetInstance().ShowGroupFocus ( ITEMSHOP_WINDOW );
						}
						
						CInnerInterface::GetInstance().HideGroup ( HELP_WINDOW );								
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( ITEMSHOP_WINDOW );
					}
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[10]->SetVisibleSingle(FALSE);
			}
		}
		break;

	case MENU_RUN_BUTTON2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				m_ButtonSet[11]->SetVisibleSingle(TRUE);

				if ( UIMSG_LB_UP & dwMsg )
				{
					CInnerInterface::GetInstance().ReqToggleRun ();
				}
			}
			else if ( dwMsg & UIMSG_MOUSEOUT )
			{
				m_ButtonSet[11]->SetVisibleSingle(FALSE);
			}
		}
		break;
		
	//case MENU_CLOSE_BUTTON:
	//	{
	//		if ( CHECK_MOUSE_IN ( dwMsg ) )
	//		{
	//			if ( CHECK_LB_UP_LIKE ( dwMsg ) )
	//			{
	//				CInnerInterface::GetInstance().HideGroup ( GetWndID () );
	//				CInnerInterface::GetInstance().ShowGroupBottom ( GAME_MENU_OPEN_BUTTON, true );
	//			}
	//		}
	//	}
	//	break;
	}
}

void CBasicGameMenu::InitShotCutString()
{
	int i=0;
	int nIndex = 0;
	CString strTemp;
	
	for ( i=0; i< BASIC_MENU_NUM; ++i){
		m_ShotcutText[i] = ID2GAMEWORD ("GAMEMENU",i);
		
		nIndex = RANPARAM::BasicMenuToRanparam[i];
		strTemp = CInnerInterface::GetInstance().GetdwKeyToString(RANPARAM::MenuShotcut[nIndex]);
		
		SetShotcutText(i,strTemp);
	}
 
}

void CBasicGameMenu::SetShotcutText ( DWORD nID, CString& strTemp )
{
	CString ShotcutTemp = m_ShotcutText[nID];

	ShotcutTemp = ShotcutTemp.Left(ShotcutTemp.Find("("));
	ShotcutTemp += "(" + strTemp + ")";
	m_ShotcutText[nID] = ShotcutTemp;	
}

