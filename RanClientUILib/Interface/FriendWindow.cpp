#include "StdAfx.h"
#include "./FriendWindow.h"

#include "FriendWindowBlockPage.h"
#include "FriendWindowNormalPage.h"
#include "GameTextControl.h"
#include "BasicTextButton.h"
#include "InnerInterface.h"
#include "BasicLineBox.h"
#include "BasicLineBoxEx.h"

#include "InBoxWindow.h"
#include "SentWindow.h"
#include "ReceiveNoteWindow.h"
#include "WriteNoteWindow.h"
#include "GLGaeaClient.h"

#include "../EngineUILib/GUInterface/BasicButton.h"
#include "../EngineLib/DxCommon/DxFontMan.h"
#include "../EngineUILib/GUInterface/BasicTextBox.h"
#include "BasicTextButton.h"
#include "d3dfont.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFriendWindow::CFriendWindow ()
	: m_pNORMAL_TAB_BUTTON( NULL )
    , m_pBLOCK_TAB_BUTTON( NULL )
	, m_pBlockPage( NULL )
	, m_pNormalPage( NULL )
	, m_pALL( NULL )
{
}

CFriendWindow::~CFriendWindow ()
{
}

void CFriendWindow::CreateSubControl ()
{
	CD3DFontPar* pFont9 = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, _DEFAULT_FONT_FLAG );

	{	// 텍스트 박스 배경
		CBasicLineBox* pBasicLineBox = new CBasicLineBox;
		pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_QUEST_LIST", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBox->CreateBaseBoxQuestList ( "NEW_FRIEND_LINEBOX" );
		RegisterControl ( pBasicLineBox );
	}
		m_pBoxUp = new CBasicLineBoxEx;
		m_pBoxUp->CreateSub ( this, "BLACKCATYB_DTDX_GIUA", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		m_pBoxUp->BlackCatYBDTDXBox ( "NEW_FRIEND_BOX_UP" );
		m_pBoxUp->SetVisibleSingle( TRUE );
		RegisterControl ( m_pBoxUp );
	
		m_pEditBoxBack = new CBasicLineBoxEx;
		m_pEditBoxBack->CreateSub ( this, "BLACKCATYB_DTDX_GIUA", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		m_pEditBoxBack->BlackCatYBDTDXBox ( "NEW_FRIEND_EDIT_BOX_BACK" );
		m_pEditBoxBack->SetVisibleSingle( TRUE );
		RegisterControl ( m_pEditBoxBack );
	
		m_pNORMAL_TAB_BUTTON = CreateTextButton ( "NEW_FRIEND_NORMAL_TAB_BUTTON", FRIEND_NORMAL_TAB_BUTTON, (char*)ID2GAMEWORD("NEW_FRIEND_TEXT_BYINDRA",0) );
		m_pBLOCK_TAB_BUTTON	 = CreateTextButton ( "NEW_FRIEND_BLOCK_TAB_BUTTON", FRIEND_BLOCK_TAB_BUTTON, (char*)ID2GAMEWORD("NEW_FRIEND_TEXT_BYINDRA",1) );
	
		m_pNormalPage = new CFriendWindowNormalPage;
		m_pNormalPage->CreateSub ( this, "NEW_FRIEND_NORMAL_PAGE", UI_FLAG_DEFAULT, FRIEND_NORMAL_PAGE );
		m_pNormalPage->CreateSubControl ();
		RegisterControl ( m_pNormalPage );
	
		m_pBlockPage = new CFriendWindowBlockPage;
		m_pBlockPage->CreateSub ( this, "NEW_FRIEND_BLOCK_PAGE", UI_FLAG_DEFAULT, FRIEND_BLOCK_PAGE );
		m_pBlockPage->CreateSubControl ();
		RegisterControl ( m_pBlockPage );
	
	
	
	
	
	
	
	
	
	
	
		ChangePage ( FRIEND_NORMAL_TAB_BUTTON );

		m_pALL = new CUIControl;
		m_pALL->CreateSub ( this, "NEW_FRIEND_BUTTON_SELECT", UI_FLAG_DEFAULT );	
		m_pALL->SetVisibleSingle ( TRUE );
		RegisterControl ( m_pALL );

{
		m_pTextBox1 = new CBasicTextBox;
		m_pTextBox1->CreateSub ( this, "NEW_FRIEND_REGISTER_TEXT" , UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		m_pTextBox1->SetFont ( pFont9 );
		m_pTextBox1->SetTextAlign ( TEXT_ALIGN_LEFT );
		m_pTextBox1->SetVisibleSingle( TRUE );
		RegisterControl ( m_pTextBox1 );
	}

	{
		m_pTextBox2 = new CBasicTextBox;
		m_pTextBox2->CreateSub ( this, "NEW_FRIEND_SELECT_TEXT" , UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		m_pTextBox2->SetFont ( pFont9 );
		m_pTextBox2->SetTextAlign ( TEXT_ALIGN_LEFT );
		m_pTextBox2->SetVisibleSingle( TRUE );
		RegisterControl ( m_pTextBox2 );
	}

	{
		m_pTextBox3 = new CBasicTextBox;
		m_pTextBox3->CreateSub ( this, "NEW_FRIEND_ALL_TEXT" , UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		m_pTextBox3->SetFont ( pFont9 );
		m_pTextBox3->SetTextAlign ( TEXT_ALIGN_LEFT );
		m_pTextBox3->SetVisibleSingle( TRUE );
		RegisterControl ( m_pTextBox3 );
	}
}

CBasicTextButton*  CFriendWindow::CreateTextButton ( const char* szButton, UIGUID ControlID, const char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE23;
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON23", UI_FLAG_XSIZE|UI_FLAG_YSIZE, ControlID );
	pTextButton->CreateBaseButton ( szButton, nBUTTONSIZE, CBasicButton::RADIO_FLIP, szText, D3DFONT_SHADOW );
	RegisterControl ( pTextButton );
	return pTextButton;
}

void CFriendWindow::TranslateUIMessage ( UIGUID cID, DWORD dwMsg )
{
	m_pTextBox1->AddText( ID2GAMEWORD ( "NEW_FRIEND_TEXT_BYINDRA", 2 ) , NS_UITEXTCOLOR::WHITE);
	m_pTextBox2->AddText( ID2GAMEWORD ( "NEW_FRIEND_TEXT_BYINDRA", 3 ) , NS_UITEXTCOLOR::WHITE);
	m_pTextBox3->AddText( ID2GAMEWORD ( "NEW_FRIEND_TEXT_BYINDRA", 4 ) , NS_UITEXTCOLOR::WHITE);

	CUIWindowEx::TranslateUIMessage ( cID, dwMsg );

	switch ( cID )
	{
	case ET_CONTROL_TITLE:
	case ET_CONTROL_TITLE_F:
		{
			if ( (dwMsg & UIMSG_LB_DUP) && CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( FRIEND_WINDOW );
			}
		}
		break;
	case FRIEND_NORMAL_TAB_BUTTON:
	case FRIEND_BLOCK_TAB_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				if ( UIMSG_LB_DOWN & dwMsg ) ChangePage ( cID );
			}
		}
		break;
	}
}

void CFriendWindow::ChangePage ( const UIGUID& cClickButton )
{
	{
		m_pNORMAL_TAB_BUTTON->SetFlip( FALSE );
		m_pBLOCK_TAB_BUTTON->SetFlip( FALSE );
	}

	{
		m_pNormalPage->SetVisibleSingle( FALSE );
		m_pBlockPage->SetVisibleSingle( FALSE );
	}

	CBasicTextButton* pVisibleButton( NULL );
	CUIControl* pVisiblePage( NULL );

	switch ( cClickButton )
	{
	case FRIEND_NORMAL_TAB_BUTTON:
		{
			pVisibleButton = m_pNORMAL_TAB_BUTTON;
			pVisiblePage = m_pNormalPage;
		}
		break;

	case FRIEND_BLOCK_TAB_BUTTON:
		{
			pVisibleButton = m_pBLOCK_TAB_BUTTON;
			pVisiblePage = m_pBlockPage;
		}
		break;
	}

	if ( pVisibleButton && pVisiblePage )
	{
		pVisibleButton->SetFlip( TRUE );
		pVisiblePage->SetVisibleSingle( TRUE );
	}
}

void  CFriendWindow::LoadFriendList ()
{
	if ( m_pNormalPage->IsVisible () )	m_pNormalPage->LoadFriendList ();
	else								m_pBlockPage->LoadBlockList ();
}

void CFriendWindow::ADD_FRIEND_NAME_TO_EDITBOX ( const CString& strName )
{
	m_pNormalPage->ADD_FRIEND_NAME_TO_EDITBOX ( strName );
	m_pBlockPage->ADD_NAME_TO_EDITBOX ( strName );
}

void CFriendWindow::ADD_FRIEND ( const CString& strName )
{
	m_pNormalPage->ADD_FRIEND ( strName );
}

const CString& CFriendWindow::GET_FRIEND_NAME () const
{
	return m_pNormalPage->GET_FRIEND_NAME ();
}

const CString& CFriendWindow::GET_BLOCK_NAME () const
{
	return m_pBlockPage->GET_BLOCK_NAME ();
}

void CFriendWindow::EDIT_END ()
{
	m_pNormalPage->EDIT_END ();
	m_pBlockPage->EDIT_END ();
}

void CFriendWindow::SetVisibleSingle ( BOOL bVisible )
{
	CUIGroup::SetVisibleSingle ( bVisible );

	if( bVisible )
	{
		GLGaeaClient::GetInstance().GetCharacter()->ReqFriendWindowOpen( true );

		if( m_pNormalPage->IsVisible() )		m_pNormalPage->SetVisibleSingle( TRUE );
		else if( m_pBlockPage->IsVisible() )	m_pBlockPage->SetVisibleSingle( TRUE );
	}
	else
	{
		GLGaeaClient::GetInstance().GetCharacter()->ReqFriendWindowOpen( false );
	}
}

SFRIEND & CFriendWindow::GetFriendSMSInfo()
{ 
	return m_pNormalPage->GetFriendInfo(); 
}