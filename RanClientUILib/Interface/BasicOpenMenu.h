#pragma once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "../EngineUILib/GUInterface/BasicButton.h"

class CBasicButton;
class CBasicVarTextBox;
class CBasicTextBox;
class CBasicButtonText;
class CBasicTextButton;

#define BASIC_MENU_NUM	12

class	CBasicOpenMenu : public CUIGroup
{
protected:
	enum
	{
		GAME_MENU_OPEN_BUTTON = NO_ID + 1,
	};

private:
	CBasicTextButton*	CreateTextButton ( char* szButton, UIGUID ControlID, char* szText );
	CBasicButtonText*	CreateButtonBlackLong ( char* szButton, char* szTextBox, CD3DFontPar* pFont, int nAlign, UIGUID ControlID, CString strText );
	CUIControl*	m_Button;
	CUIControl*	m_ButtonSet;
	CBasicTextBox*	m_ButtonText;


public:
    CBasicOpenMenu ();
	virtual	~CBasicOpenMenu ();

public:
	void CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
};