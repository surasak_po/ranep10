#include "StdAfx.h"
#include "./UIOuterWindow.h"
#include "./OuterInterface.h"
#include "./BasicLineBox.h"
#include "../EngineUIlib/GUInterface/BasicTextBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CUIOuterWindow::CUIOuterWindow ()
{
}

CUIOuterWindow::~CUIOuterWindow ()
{
}

void CUIOuterWindow::CreateBaseWidnow ( char* szWindowKeyword, char* szWindowName )
{
	CreateTitle ( "NEW_OUTER_WINDOW_TITLE", "NEW_OUTER_WINDOW_TITLE_LEFT", "NEW_OUTER_WINDOW_TITLE_MID", "NEW_OUTER_WINDOW_TITLE_RIGHT", "NEW_OUTER_WINDOW_TEXTBOX", szWindowName );
	CreateBody ( "NEW_OUTER_WINDOW_BODY", "NEW_OUTER_WINDOW_BODY_LEFT", "NEW_OUTER_WINDOW_BODY_UP", "NEW_OUTER_WINDOW_BODY_MAIN", "NEW_OUTER_WINDOW_BODY_DOWN", "NEW_OUTER_WINDOW_BODY_RIGHT" );	

	ResizeControl ( szWindowKeyword );

	SetTitleAlign ( TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y | TEXT_ALIGN_TOP );
}

CBasicTextBox*	CUIOuterWindow::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );	
	RegisterControl ( pStaticText );
	return pStaticText;
}

CUIControl*	CUIOuterWindow::CreateControl ( char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}


void CUIOuterWindow::ResizeControl ( char* szWindowKeyword )
{
    CUIControl TempControl;
	TempControl.Create ( 1, szWindowKeyword );
	const UIRECT& rcParentOldPos = GetLocalPos ();
	const UIRECT& rcParentNewPos = TempControl.GetLocalPos ();
	AlignSubControl ( rcParentOldPos, rcParentNewPos );

	SetLocalPos ( D3DXVECTOR2 ( rcParentNewPos.left, rcParentNewPos.top ) );
}

void CUIOuterWindow::TranslateUIMessage( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindow::TranslateUIMessage ( ControlID, dwMsg );

	switch ( ControlID )
	{
	case ET_CONTROL_BUTTON:
		{
			if( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				COuterInterface::GetInstance().HideGroup( GetWndID() );
			}
		}
		break;
	}
}