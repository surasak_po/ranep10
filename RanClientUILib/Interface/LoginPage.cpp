#include "StdAfx.h"

#include "LoginPage.h"
#include "BasicTextButton.h"
#include "GameTextControl.h"
#include "BasicTextBoxEx.h"
#include "UITextControl.h"
#include "BasicLineBox.h"
#include "BasicLineBoxEx.h"
#include "OuterInterface.h"
#include "ModalWindow.h"
#include "s_NetClient.h"
#include "RANPARAM.h"
#include "DxGlobalStage.h"
#include "DxInputString.h"
#include "DebugSet.h"

#include "../EngineUiLib/GUInterface/BasicButtonText.h"
#include "../enginelib/Common/StringUtils.h"
#include "../EngineLib/DxCommon/DxFontMan.h"
#include "../EngineUIlib/GUInterface/UIEditBoxMan.h"
#include "../EngineUIlib/GUInterface/UIKeyCheck.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int	CLoginPage::nLIMIT_ID = 15;
int	CLoginPage::nLIMIT_PW = 19;
int	CLoginPage::nLIMIT_RP = 6;

CLoginPage::CLoginPage ()
	: m_pIDSaveButton(NULL)
	, m_pRandTextBox(NULL)
	, m_nRandPassNumber(0)
	, m_nRPUpdateCnt(0)
{
}

CLoginPage::~CLoginPage ()
{
}

void CLoginPage::CreateSubControl ()
{
	CD3DFontPar* pFont9 = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, _DEFAULT_FONT_FLAG );
	const int nAlignCenterBoth = TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y;	
	const int nBUTTONSIZE = CBasicTextButton::SIZE18;
	CreateControl ( "NEW_LOGIN_PAGE_BACK_DOWN" );
	{	// ��en Tr??ng ��en ��en Background
		CBasicLineBoxEx* pBasicLineBoxEx = new CBasicLineBoxEx;
		pBasicLineBoxEx->CreateSub ( this, "BLACKCATYB_DXDX_GIUA", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBoxEx->BlackCatYBDXDXBox ( "NEW_LOGIN_PAGE_BACK" );
		RegisterControl ( pBasicLineBoxEx );
	}
	{	// ��en Tr??ng ��en Xa?m Background
		CBasicLineBoxEx* pBasicLineBoxEx = new CBasicLineBoxEx;
		pBasicLineBoxEx->CreateSub ( this, "BLACKCATYB_DTDD_GIUA", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBoxEx->BlackCatYBDTDDBox ( "NEW_LOGIN_PAGE_ID_BACK" );
		RegisterControl ( pBasicLineBoxEx );
	}
	{	// ��en Tr??ng ��en Xa?m Background
		CBasicLineBoxEx* pBasicLineBoxEx = new CBasicLineBoxEx;
		pBasicLineBoxEx->CreateSub ( this, "BLACKCATYB_DTDD_GIUA", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBoxEx->BlackCatYBDTDDBox ( "NEW_LOGIN_PAGE_PW_BACK" );
		RegisterControl ( pBasicLineBoxEx );
	}







	CBasicTextBox* pTextBox = NULL;
	pTextBox = CreateStaticControl ( "NEW_LOGIN_PAGE_ID", pFont9, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
	pTextBox->SetOneLineText ( (char*)ID2GAMEWORD ( "LOGIN_PAGE_IDPW", 0 ) );
	pTextBox = CreateStaticControl ( "NEW_LOGIN_PAGE_PW", pFont9, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
	pTextBox->SetOneLineText ( (char*)ID2GAMEWORD ( "LOGIN_PAGE_IDPW", 1 ) );

	{
		CUIEditBoxMan* pEditBoxMan = new CUIEditBoxMan;
		pEditBoxMan->CreateSub ( this, "NEW_LOGIN_EDITMAN", UI_FLAG_DEFAULT, LOGIN_EDIT_MAN );
		pEditBoxMan->CreateEditBox ( LOGIN_EDIT_ID, "NEW_LOGIN_EDITMAN_ID", "NEW_LOGIN_CARRAT", TRUE, UINT_MAX, pFont9, nLIMIT_ID );
		pEditBoxMan->CreateEditBox ( LOGIN_EDIT_PW, "NEW_LOGIN_EDITMAN_PW", "NEW_LOGIN_CARRAT", TRUE, UINT_MAX, pFont9, nLIMIT_PW );
		pEditBoxMan->SetHide ( LOGIN_EDIT_PW, TRUE );
		RegisterControl( pEditBoxMan );
		m_pEditBoxMan = pEditBoxMan;
	}

	{
		m_pIDSaveButton = CreateFlipButton ( "NEW_LOGIN_PAGE_IDSAVE_BUTTON", "NEW_LOGIN_PAGE_IDSAVE_BUTTON_F", LOGIN_PAGE_IDSAVE_BUTTON );
		pTextBox = CreateStaticControl ( "NEW_LOGIN_PAGE_IDSAVE_BACK", pFont9, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_LEFT );
		pTextBox->SetOneLineText ( (char*)ID2GAMEWORD ( "LOGIN_PAGE_IDSAVE_BACK" ) );
	}

	m_pOKButton = CreateButtonBlue ( "NEW_LOGIN_BUTTON_OK", "NEW_LOGIN_BUTTON_OK_TEXT",pFont9, nAlignCenterBoth,LOGIN_OK,"��ŧ" );
	m_pOKButton->SetShortcutKey ( DIK_RETURN, DIK_NUMPADENTER );
	RegisterControl ( m_pOKButton );

	m_pQuitButton = CreateButtonBlue ( "NEW_LOGIN_BUTTON_CANCEL", "NEW_LOGIN_BUTTON_CANCEL_TEXT",pFont9, nAlignCenterBoth,LOGIN_CANCEL,"¡��ԡ" );
	m_pQuitButton->SetShortcutKey ( DIK_ESCAPE );
	RegisterControl ( m_pQuitButton );

	pRegisterButton = CreateButtonBlue ( "NEW_LOGIN_BUTTON_REGISTER", "NEW_LOGIN_BUTTON_REGISTER_TEXT",pFont9, nAlignCenterBoth,LOGIN_REGISTER,"��Ѥ��ʹ�" );
	pRegisterButton->SetShortcutKey ( DIK_ESCAPE );
	RegisterControl ( pRegisterButton );

	LoadIDSaveButton ();
}	
CBasicButtonText* CLoginPage::CreateButtonBlue ( char* szButton, char* szTextBox, CD3DFontPar* pFont, int nAlign, UIGUID ControlID, CString strText )
{
	CBasicButtonText* pButton = new CBasicButtonText;
	pButton->CreateRanButtonBlue ( this, szButton, ControlID );
	pButton->CreateTextBox ( szTextBox, pFont, nAlign );
	pButton->SetOneLineText( strText.GetString() );
	RegisterControl ( pButton );

	return pButton;
}
CBasicTextButton*	 CLoginPage::CreateTextButton ( char* szButton, UIGUID ControlID, char* szText )
{
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON24", UI_FLAG_XSIZE, ControlID );
	pTextButton->CreateBaseButton ( szButton, 
									CBasicTextButton::SIZE24, 
									CBasicButton::CLICK_FLIP, 
									szText,
									_DEFAULT_FONT_SHADOW_EX_FLAG );
	pTextButton->SetFlip ( TRUE );
	RegisterControl ( pTextButton );

	return pTextButton;
}

void CLoginPage::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{	
	CUIWindow::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	CNetClient* pNetClient = DxGlobalStage::GetInstance().GetNetClient ();
	if ( !pNetClient->IsOnline() )		//��Ʈ��ũ ������ ������ ���
	{
		if ( !COuterInterface::GetInstance().IsVisibleGroup ( MODAL_WINDOW_OUTER ) )
		{
			if( !COuterInterface::GetInstance().IsLoginCancel() )
			{
				DoModalOuter ( ID2GAMEEXTEXT ("LOGINSTAGE_1"), MODAL_INFOMATION, OK, OUTER_MODAL_RECONNECT );
			}
		}
	}
	else
	{
	}

	//	�� �̵�
	if ( m_pEditBoxMan )
	{
		CUIControl* pParent = m_pEditBoxMan->GetTopParent ();
		if ( !pParent )	pParent = this;	//	���� ��Ŭ������ �ֻ��� ��Ʈ���� ���
		BOOL bFocus = ( pParent->IsFocusControl() );

		if ( bFocus )
		{
			if ( UIKeyCheck::GetInstance()->Check ( DIK_TAB, DXKEY_DOWN ) )
			{
				m_pEditBoxMan->GoNextTab ();
			}
		}
	}
}

void CLoginPage::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindow::TranslateUIMessage ( ControlID, dwMsg );

	switch ( ControlID )
	{
	case LOGIN_OK:
		{
			if ( CHECK_KEYFOCUSED ( dwMsg ) || CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				SetMessageEx( dwMsg &= ~UIMSG_KEY_FOCUSED ); // Focus ������ �޼��� ����
				
				CString strID = m_pEditBoxMan->GetEditString ( LOGIN_EDIT_ID );
				CString strPW = m_pEditBoxMan->GetEditString ( LOGIN_EDIT_PW );
				CString strRP;

				if ( !strID.GetLength () || !strPW.GetLength () )
				{
					DoModalOuter ( ID2GAMEEXTEXT ( "LOGINSTAGE_5" ) );
					return ;
				}

#ifndef CH_PARAM
				if ( !CheckString ( strID ) )
				{
					DoModalOuter ( ID2GAMEEXTEXT ( "LOGIN_PAGE_ID_ERROR" ) );
					return ;
				}

				if ( !CheckString ( strPW ) )
				{
					DoModalOuter ( ID2GAMEEXTEXT ( "LOGIN_PAGE_PW_ERROR" ) );
					return ;
				}
#endif

				//	��Ʈ�ѷ� ���� ���̵�� ��ȣ�� ������ ������ ����
				CNetClient* pNetClient = DxGlobalStage::GetInstance().GetNetClient ();

				int nServerGroup, nServerChannel;
				COuterInterface::GetInstance().GetConnectServerInfo( nServerGroup, nServerChannel );

				switch ( RANPARAM::emSERVICE_TYPE )
				{
				case EMSERVICE_THAILAND:
					pNetClient->ThaiSndLogin( strID, strPW, nServerChannel );
					break;

				case EMSERVICE_CHINA:
					pNetClient->ChinaSndLogin( strID, strPW, strRP, nServerChannel );
					break;
//		Japan �α��� ��� ����
				case EMSERVICE_JAPAN:
					pNetClient->JapanSndLogin( strID, strPW, nServerChannel );
                    break;

				case EMSERVICE_GLOBAL:
					pNetClient->GsSndLogin( strID, strPW, nServerChannel );
					break;

				default:
					pNetClient->ChinaSndLogin( strID, strPW, strRP, nServerChannel );		
					break;
				};

				//	Note : ���� id ����. ( ���� ���� ����, id ������ ���ؼ� �ʿ���. )
				//
				RANPARAM::SETUSERID ( strID );
				RANPARAM::SAVE();
				
				//	��ȣ ���� ��� �޽��� ǥ��
				DoModalOuter ( ID2GAMEEXTEXT ("LOGINSTAGE_6"), MODAL_INFOMATION, CANCEL, OUTER_MODAL_WAITCONFIRM );
				COuterInterface::GetInstance().SetModalCallWindowID( GetWndID() );

				//COuterInterface::GetInstance().HideGroup ( GetWndID () );
				//COuterInterface::GetInstance().ShowGroupFocus ( SELECT_CHARACTER_PAGE );
			}
		}
		break;

	case LOGIN_REGISTER:
		{
			if ( CHECK_KEYFOCUSED ( dwMsg ) || CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				CNetClient* pNetClient = DxGlobalStage::GetInstance().GetNetClient();
				if( pNetClient->IsOnline() == true )
				{
					COuterInterface::GetInstance().ToRegisterPage ( GetWndID () );
				}

			}
		}break;

	case LOGIN_CANCEL:
		{
			if ( CHECK_KEYFOCUSED ( dwMsg ) || CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				CNetClient* pNetClient = DxGlobalStage::GetInstance().GetNetClient();
				if( pNetClient->IsOnline() == true )
				{
					pNetClient->CloseConnect();
					COuterInterface::GetInstance().SetCancelToLogin();
				}

				COuterInterface::GetInstance().ToSelectServerPage ( GetWndID () );
			}
		}
		break;

	case LOGIN_PAGE_IDSAVE_BUTTON:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
			{
				if ( m_pIDSaveButton )
				{
					RANPARAM::bSAVE_USERID = !RANPARAM::bSAVE_USERID;
					RANPARAM::SAVE ();
					LoadIDSaveButton ();
				}
			}
		}
		break;
	}
}

BOOL CLoginPage::CheckString( CString strTemp )
{	
	strTemp = strTemp.Trim();

	// ���ڿ� üũ - ���� �ȵǴ� Ư������ : ~!@#$%^&*+|":?><\=`',.;[]{}()
	if( STRUTIL::CheckString( strTemp ) )
	{
		DoModalOuter ( ID2GAMEEXTEXT ( "LOGINSTAGE_4" ), MODAL_INFOMATION, OK );		
		return FALSE;
	}

    return TRUE;
}

void CLoginPage::ResetAll ()
{
	m_pEditBoxMan->EndEdit ();

	m_pEditBoxMan->ClearEdit ( LOGIN_EDIT_ID );
	m_pEditBoxMan->ClearEdit ( LOGIN_EDIT_PW );

	m_pEditBoxMan->Init();
	m_pEditBoxMan->BeginEdit();

	if( m_pEditBoxMan->IsMODE_NATIVE() )
	{
		m_pEditBoxMan->DoMODE_TOGGLE();
	}

	std::string& strSavedUserID = RANPARAM::GETUSERID_DEC();
	if ( strSavedUserID.size() )
	{
		m_pEditBoxMan->SetEditString ( LOGIN_EDIT_ID, CString ( strSavedUserID.c_str() ) );
		m_pEditBoxMan->GoNextTab ();
	}
}

void CLoginPage::SetVisibleSingle ( BOOL bVisible )
{
	CUIGroup::SetVisibleSingle( bVisible );

	if ( bVisible )
	{
		if( GLCONST_CHAR::nUI_KEYBOARD == 2 )
		{
			m_pEditBoxMan->DisableKeyInput();
			DXInputString::GetInstance().DisableKeyInput();
		}

		ResetAll();

		if( m_pRandTextBox ) m_pRandTextBox->ClearText();
		COuterInterface::GetInstance().ResetCancelToLogin();
	}
	else
	{
		m_pEditBoxMan->EndEdit ();

		if( GLCONST_CHAR::nUI_KEYBOARD == 2 )
		{
			m_pEditBoxMan->UsableKeyInput();
			DXInputString::GetInstance().UsableKeyInput();
		}

		CNetClient* pNetClient = DxGlobalStage::GetInstance().GetNetClient();
		pNetClient->ResetRandomPassNumber();
	}
}

CBasicButton* CLoginPage::CreateFlipButton ( char* szButton, char* szButtonFlip, UIGUID ControlID )
{
	CBasicButton* pButton = new CBasicButton;
	pButton->CreateSub ( this, szButton, UI_FLAG_DEFAULT, ControlID );
	pButton->CreateFlip ( szButtonFlip, CBasicButton::RADIO_FLIP );
	pButton->SetControlNameEx ( szButton );
	RegisterControl ( pButton );

	return pButton;
}

void CLoginPage::LoadIDSaveButton ()
{
	BOOL bIDSave = RANPARAM::bSAVE_USERID;
	m_pIDSaveButton->SetFlip ( bIDSave );	
}

void CLoginPage::SetCharToEditBox( TCHAR cKey )
{
	if( !m_pEditBoxMan ) return;
	
	CString strTemp;

	UIGUID nID = m_pEditBoxMan->GetBeginEditBox();

	strTemp = m_pEditBoxMan->GetEditString( nID );
	strTemp += cKey;

	m_pEditBoxMan->SetEditString( nID, strTemp );
}

void CLoginPage::DelCharToEditBox()
{
	if( !m_pEditBoxMan ) return;

	CString strTemp;

	UIGUID nID = m_pEditBoxMan->GetBeginEditBox();

	strTemp = m_pEditBoxMan->GetEditString( nID );
	INT nLenth = strTemp.GetLength();
	strTemp = strTemp.Left( nLenth - 1 );

	m_pEditBoxMan->SetEditString( nID, strTemp );
}

void CLoginPage::GoNextTab()
{
	if( m_pEditBoxMan )
		m_pEditBoxMan->GoNextTab();
}