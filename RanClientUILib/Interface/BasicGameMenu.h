#pragma once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "../EngineUILib/GUInterface/BasicButton.h"

class CBasicButton;
class CBasicVarTextBox;
class CBasicTextBox;
class CBasicButtonText;
class CBasicTextButton;

#define BASIC_MENU_NUM	12

class	CBasicGameMenu : public CUIGroup
{
protected:
	enum
	{
		MENU_INVENTORY_BUTTON = NO_ID + 1,
		MENU_CHARACTER_BUTTON,
		MENU_SKILL_BUTTON,
		MENU_PARTY_BUTTON,
		MENU_GUILD_BUTTON,
		MENU_QUEST_BUTTON,
		MENU_FRIEND_BUTTON,
		MENU_LARGEMAP_BUTTON,
		MENU_CHATMACRO_BUTTON,
		MENU_ITEMBANK_BUTTON,
		MENU_ITEMSHOP_BUTTON,
		MENU_RUN_BUTTON,
		
		MENU_CLOSE_BUTTON,
		MENU_INVENTORY_BUTTON2,
		MENU_CHARACTER_BUTTON2,
		MENU_SKILL_BUTTON2,
		MENU_PARTY_BUTTON2,
		MENU_GUILD_BUTTON2,
		MENU_QUEST_BUTTON2,
		MENU_FRIEND_BUTTON2,
		MENU_LARGEMAP_BUTTON2,
		MENU_CHATMACRO_BUTTON2,
		MENU_ITEMBANK_BUTTON2,
		MENU_ITEMSHOP_BUTTON2,
		MENU_RUN_BUTTON2
	};

private:
	int	m_nOverMenu;
	CBasicVarTextBox* m_pInfo;
	CBasicTextBox*		m_pTextTitle;
	CBasicTextButton*	CreateTextButton ( char* szButton, UIGUID ControlID, char* szText );
	CBasicButtonText*	CreateButtonBlackLong ( char* szButton, char* szTextBox, CD3DFontPar* pFont, int nAlign, UIGUID ControlID, CString strText );
	CBasicTextButton*	m_pButton1;
	CBasicTextButton*	m_pButton2;
	CBasicTextButton*	m_pButton3;
	CBasicTextButton*	m_pButton4;
	CBasicTextButton*	m_pButton5;
	CBasicTextButton*	m_pButton6;
	CBasicTextButton*	m_pButton7;
	CBasicTextButton*	m_pButton8;
	CBasicTextButton*	m_pButton9;
	CBasicTextButton*	m_pButton10;
	CBasicTextButton*	m_pButton11;
	CBasicTextButton*	m_pButton12;
	CUIControl*	m_Button[12];
	CUIControl*	m_ButtonSet[12];
	CBasicTextBox*	m_ButtonText[12];



	CBasicButton* m_pRunButton;
	CString m_ShotcutText[BASIC_MENU_NUM];

public:
    CBasicGameMenu ();
	virtual	~CBasicGameMenu ();

public:
	void CreateSubControl ();
	void SetFlipRunButton( BOOL bRun )			{ m_pRunButton->SetFlip( bRun ); }
	void SetShotcutText ( DWORD nID, CString& strTemp );
	void InitShotCutString();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
};