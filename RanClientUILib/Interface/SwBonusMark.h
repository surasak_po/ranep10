#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"


class CSwBonusMark : public CUIGroup
{
public:
	CUIControl*		m_pImage;

public:
	CSwBonusMark();
	virtual ~CSwBonusMark();

	void	CreateSubControl ();
	void	SetSwBonus( bool isWinner );
};