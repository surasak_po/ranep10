#include "StdAfx.h"
#include "./CharacterWindow.h"

#include "../EngineUiLib/GUInterface/BasicTextBox.h"
#include "../EngineUiLib/GUInterface/BasicProgressBar.h"
#include "../EngineUiLib/GUInterface/BasicButton.h"
#include "../EngineLib/DxCommon/d3dfont.h"

#include "../BasicTextButton.h"
#include "../UITextControl.h"
#include "../GameTextControl.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"
#include "../InnerInterface.h"
#include "CharacterWindowChar.h"
#include "CharacterWindowVehicle.h"
//#include "CharacterWindowPet.h"
#include "../BasicLineBox.h"
#include "../BasicLineBoxEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCharacterWindow::CCharacterWindow () :
	nActivePage(0)
	, m_pPageChar( NULL )
	, m_pPageVehicle( NULL )
	, m_pPagePet( NULL )
	, m_pBackGround( NULL )
	, m_pButtonChar( NULL )
	, m_pButtonVehicle( NULL )
	, m_pButtonPet( NULL )
	, m_pAddInfoButton(NULL)
	, m_pAddInfoButtonOver(NULL)
{
}

CCharacterWindow::~CCharacterWindow ()
{
}

void CCharacterWindow::CreateSubControl ()
{

	/*{	// �en X�m �en Background
		CBasicLineBoxEx* pBasicLineBoxEx0 = new CBasicLineBoxEx;
		pBasicLineBoxEx0->CreateSub ( this, "BASIC_LINE_BOX_EX_BODY_DENXAM", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBoxEx0->CreateBaseBoxDenXamBox ( "AP_CHARACTER_WINDOW_BACKGROUND" );
		RegisterControl ( pBasicLineBoxEx0 );
	}*/
	{
		CBasicLineBox* pBasicLineBox = new CBasicLineBox;
		pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_QUEST_LIST", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBox->CreateBaseBoxQuestList( "AP_CHARACTER_WINDOW_WEAR_BACK" );
		RegisterControl ( pBasicLineBox );
	}
	{
		CBasicLineBox* pBasicLineBox = new CBasicLineBox;
		pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_QUEST_LIST", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
		pBasicLineBox->CreateBaseBoxQuestList( "AP_CHARACTER_WINDOW_STAT_BACK" );
		RegisterControl ( pBasicLineBox );
	}
	m_pButtonChar= CreateTextButton23 ( "AP_CHARACTER_WINDOW_CHAR_TAP", BUTTON_CHARACTER, (char*)ID2GAMEWORD ( "CHARACTER_TAP_NAME", 0 ) );
	m_pButtonChar->SetVisibleSingle ( FALSE );


	m_pPageChar = new CCharacterWindowChar;
	m_pPageChar->CreateSub ( this, "AP_CHARACTER_WINDOW_REGION", UI_FLAG_YSIZE, PAGE_CHARACTER );
	m_pPageChar->CreateSubControl ();
	RegisterControl ( m_pPageChar );

	m_pAddInfoButton = new CBasicButton;
	m_pAddInfoButton->CreateSub ( this, "AP_ADDITIONAL_BUTTON", UI_FLAG_DEFAULT, ADDITIONAL_BUTTON );
	m_pAddInfoButton->CreateMouseOver( "AP_ADDITIONAL_BUTTON_F" );
	m_pAddInfoButton->SetUseGlobalAction ( TRUE );
	RegisterControl ( m_pAddInfoButton );

}
CBasicTextButton*  CCharacterWindow::CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE23;
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON23", UI_FLAG_XSIZE|UI_FLAG_YSIZE, ControlID );
	pTextButton->CreateBaseButton ( szButton, nBUTTONSIZE, CBasicButton::RADIO_FLIP, szText ,_DEFAULT_FONT_SHADOW_FLAG );
	RegisterControl ( pTextButton );
	return pTextButton;
}

void CCharacterWindow::SetGlobalPos(const D3DXVECTOR2& vPos)
{
	CUIGroup::SetGlobalPos( vPos );
}

void CCharacterWindow::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );

	switch ( ControlID )
	{
	case ET_CONTROL_BUTTON:
			{
				if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
				{
					CInnerInterface::GetInstance().HideGroup ( CHARACTER_WINDOW );
					CInnerInterface::GetInstance().HideGroup ( CHARACTER_ADDITIONAL_WINDOW );
				}
				break;
			}
	case ET_CONTROL_TITLE:
	case ET_CONTROL_TITLE_F:
		{
			if ( (dwMsg & UIMSG_LB_DUP) && CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( CHARACTER_ADDITIONAL_WINDOW );
			}
		}
		break;
	case BUTTON_CHARACTER:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	
			{
				if ( nActivePage == CHARPAGE_CHARACTER )	return;
				OpenPage( CHARPAGE_CHARACTER );
			}
		}break;
	case ADDITIONAL_BUTTON:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				if ( dwMsg & UIMSG_LB_UP )
				{
					if ( !CInnerInterface::GetInstance().IsVisibleGroup ( CHARACTER_ADDITIONAL_WINDOW ) )
					{
						//m_pAddInfoButtonR->SetVisibleSingle ( TRUE );
						CInnerInterface::GetInstance().ShowGroupFocus ( CHARACTER_ADDITIONAL_WINDOW );
						//m_pAddInfoButtonR->SetVisibleSingle ( TRUE );
//						m_pAddInfoButtonL->SetVisibleSingle ( FALSE );
					}
					else
					{
						CInnerInterface::GetInstance().HideGroup ( CHARACTER_ADDITIONAL_WINDOW );
						//m_pAddInfoButtonL->SetVisibleSingle ( TRUE );
						//m_pAddInfoButtonR->SetVisibleSingle ( FALSE );
					}
				}
			}
		}	
		break;
	}
}


void CCharacterWindow::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{	
	CUIWindow::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}

void CCharacterWindow::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindow::SetVisibleSingle( bVisible );

	if( bVisible )
	{
		OpenPage( 0 );
	}
	else
	{
		if ( m_pPageChar )
			m_pPageChar->ClearRender();
	}
}

void CCharacterWindow::OpenPage( int nPage )
{
	const GLCHARLOGIC& sCharData = GLGaeaClient::GetInstance().GetCharacterLogic ();

	//Add Character Window Title Name by Indra
	CString strCombineName;
	strCombineName.Format ( "[%s] ข้อมลูตัวละคร", sCharData.m_szName );
	SetTitleName ( strCombineName ); 

	m_pButtonChar->SetFlipYellow ( FALSE );


	m_pPageChar->SetVisibleSingle ( FALSE );

	switch( nPage )
	{
	case CHARPAGE_CHARACTER:
		{
			m_pPageChar->SetVisibleSingle ( TRUE );
			m_pButtonChar->SetFlipYellow ( TRUE );
			nActivePage = nPage;
		}break;
	};
}

void CCharacterWindow::SetArmSwapTabButton( BOOL bArmSub )
{
	if( m_pPageChar )
	{
		m_pPageChar->SetArmSwapTabButton( bArmSub );
	}
}