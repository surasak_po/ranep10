#include "StdAfx.h"
#include "SchoolWarRankingDisplay.h"
#include "SchoolWarRankingPageRank.h"
#include "SchoolWarRankingPageWinner.h"
#include "../BasicLineBoxEx.h"

#include "../InnerInterface.h"
#include "../BasicTextButton.h"
#include "../GameTextControl.h"
#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarRankingDisplay::CSchoolWarRankingDisplay () :
	m_nActivePage(0),
	m_pBackGround( NULL ),
	m_pButtonRankAll( NULL ),
	m_pPageRank( NULL ),
	m_pButtonClose( NULL ),
	m_pLineBoxUpper( NULL ),
	m_pLineBoxBottom( NULL ),
	m_pLineBoxTopWhite ( NULL ),
	m_pLineBoxBottomWhite ( NULL )
{
	
}

CSchoolWarRankingDisplay::~CSchoolWarRankingDisplay ()
{
}

CBasicTextButton*  CSchoolWarRankingDisplay::CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE23;
	CBasicTextButton* pTextButton = new CBasicTextButton;
	pTextButton->CreateSub ( this, "BASIC_TEXT_BUTTON23", UI_FLAG_XSIZE|UI_FLAG_YSIZE, ControlID );
	pTextButton->CreateBaseButtonEx ( szButton, nBUTTONSIZE, CBasicButton::RADIO_FLIP, szText );
	RegisterControl ( pTextButton );
	return pTextButton;
}

CBasicTextButton* CSchoolWarRankingDisplay::CreateTextButton ( char* szButton, UIGUID ControlID , char* szText )
{
	const int nBUTTONSIZE = CBasicTextButton::SIZE19;
	CBasicTextButton* pButton = new CBasicTextButton;
	pButton->CreateSub( this, "BASIC_TEXT_BUTTON19", UI_FLAG_XSIZE, ControlID );
	pButton->CreateBaseButton( szButton, nBUTTONSIZE, CBasicButton::CLICK_FLIP, szText );
	RegisterControl( pButton );

	return pButton;
}

void CSchoolWarRankingDisplay::CreateSubControl ()
{	
	m_pLineBoxUpper = new CBasicLineBoxEx;
	m_pLineBoxUpper->CreateSub ( this, "BASIC_LINE_BOX_EX_DIALOGUE_WHITE", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	m_pLineBoxUpper->CreateBaseBoxDialogueWhite ( "SCHOOLWAR_RANKING_LINEBOX_UPPER" );
	m_pLineBoxUpper->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pLineBoxUpper );
	
	m_pLineBoxBottom = new CBasicLineBoxEx;
	m_pLineBoxBottom->CreateSub ( this, "BASIC_LINE_BOX_EX_DIALOGUE_WHITE", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	m_pLineBoxBottom->CreateBaseBoxDialogueWhite ( "SCHOOLWAR_RANKING_LINEBOX_BOTTOM" );
	m_pLineBoxBottom->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pLineBoxBottom );
	
	m_pLineBoxTopWhite = new CBasicLineBoxEx;
	m_pLineBoxTopWhite->CreateSub ( this, "BASIC_LINE_BOX_EX_DIALOGUE_WHITE", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	m_pLineBoxTopWhite->CreateBaseBoxDialogueWhiteBack ( "SCHOOLWAR_RANKING_LINEBOX_TOP_WHITE" );
	m_pLineBoxTopWhite->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pLineBoxTopWhite );
	
	m_pLineBoxBottomWhite = new CBasicLineBoxEx;
	m_pLineBoxBottomWhite->CreateSub ( this, "BASIC_LINE_BOX_EX_DIALOGUE_WHITE", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	m_pLineBoxBottomWhite->CreateBaseBoxDialogueWhiteBack ( "SCHOOLWAR_RANKING_LINEBOX_BOTTOM_WHITE" );
	m_pLineBoxBottomWhite->SetVisibleSingle ( TRUE );
	RegisterControl ( m_pLineBoxBottomWhite );

	m_pButtonRankAll = CreateTextButton23 ( "SCHOOLWAR_RANKING_BUTTON_RANK", SCHOOLWAR_RANKING_BUTTON_RANK, (char*)ID2GAMEWORD ( "SCHOOLWAR_RANKING_WINDOW_TEXTS", 1 ) );
	CString strRank[3] = 
	{
		"SCHOOLWAR_RANKING_BUTTON_RANK_SG",
		"SCHOOLWAR_RANKING_BUTTON_RANK_MP",
		"SCHOOLWAR_RANKING_BUTTON_RANK_PHX"
	};

	for(int i = 0; i < 3; i++ ) m_pButtonRank[i] = CreateTextButton23	((char*)strRank[i].GetString(),
																	 SCHOOLWAR_RANKING_BUTTON_RANK_SG + i,
																	 (char*)ID2GAMEWORD ( "SCHOOLWAR_RANKING_WINDOW_TEXTS_SCHOOL",i)
																	);
	
	m_pPageRank = new CSchoolWarRankingPageRank;
	m_pPageRank->CreateSub ( this, "SCHOOLWAR_RANKING_PAGE" );
	m_pPageRank->CreateSubControl ();
	RegisterControl ( m_pPageRank );

	m_pButtonClose = CreateTextButton( "SCHOOLWAR_BUTTON_CLOSE", SCHOOLWAR_BUTTON_CLOSE, (char*)ID2GAMEWORD("SCHOOLWAR_RANKING_WINDOW_TEXTS",0)); 

	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, D3DFONT_SHADOW | D3DFONT_ASCII );
	D3DCOLOR dwColor = NS_UITEXTCOLOR::WHITE;

	CString strText[5] = 
	{
		"SCHOOLWAR_RANKING_SLOT_TEXT_RANK",
		"SCHOOLWAR_RANKING_SLOT_TEXT_SCHOOL",
		"SCHOOLWAR_RANKING_SLOT_TEXT_NAME",
		"SCHOOLWAR_RANKING_SLOT_TEXT_KILL",
		"SCHOOLWAR_RANKING_SLOT_TEXT_DEATH"
	};

	for(int i = 0; i < 5; i++ )
	{
		m_pText[i] = CreateStaticControl( (char*)strText[i].GetString(), pFont, dwColor, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
		m_pText[i]->SetText( ID2GAMEWORD("SCHOOLWAR_RANKING_SLOT_TEXT_STATIC",i), dwColor );
	}
}

void CSchoolWarRankingDisplay::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	switch ( ControlID )
	{
	case ET_CONTROL_TITLE:
	case ET_CONTROL_TITLE_F:
		{
			if ( (dwMsg & UIMSG_LB_DUP) && CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SetDefaultPosInterface( SCHOOLWAR_RANKING_DISPLAY );
			}
		}break;
	case ET_CONTROL_BUTTON:
	case SCHOOLWAR_BUTTON_CLOSE:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )
			{
				CInnerInterface::GetInstance().HideGroup ( GetWndID () );
			}
		}break;

	case SCHOOLWAR_RANKING_BUTTON_RANK:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 1 );
		}break;

	case SCHOOLWAR_RANKING_BUTTON_RANK_SG:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 2 );
		}break;
	case SCHOOLWAR_RANKING_BUTTON_RANK_MP:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 3 );
		}break;
	case SCHOOLWAR_RANKING_BUTTON_RANK_PHX:
		{
			if ( CHECK_MOUSEIN_LBUPLIKE ( dwMsg ) )	OpenPage( 4 );
		}break;
	}

	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );
}

void CSchoolWarRankingDisplay::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindowEx::SetVisibleSingle( bVisible );

	if( bVisible )
	{
		OpenPage( 1 );
	}
}

void CSchoolWarRankingDisplay::RefreshRanking()
{
	if ( m_pPageRank )
	{
		m_pPageRank->RefreshRanking();
	}
}

void CSchoolWarRankingDisplay::OpenPage( int nPage )
{
	if ( m_pButtonRankAll )	m_pButtonRankAll->SetFlip ( FALSE );
	for(int i = 0; i<3; i++) m_pButtonRank[i]->SetFlip ( FALSE );

	switch( nPage )
	{
	case 1:
		{
			if ( m_pButtonRankAll )	m_pButtonRankAll->SetFlip ( TRUE );

			if ( m_pPageRank )	
				m_pPageRank->RefreshRanking();
		}break;
	case 2:
	case 3:
	case 4:
		{
			m_pButtonRank[nPage-2]->SetFlip ( TRUE );

			if ( m_pPageRank )	
				m_pPageRank->RefreshRanking(nPage-2);
		}
		break;
	};
}

CBasicTextBox* CSchoolWarRankingDisplay::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}