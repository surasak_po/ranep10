#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"

#include "GLSchoolWar/GLSchoolWarData.h"
#include "GLSchoolWar/GLSchoolWar.h"
#include "GLSchoolWar/GLSchoolWarMan.h"

class	CSchoolWarRankingSlot;
class	CBasicScrollBarEx;

class	CSchoolWarRankingPageWinner : public CUIGroup
{
public:
	enum
	{
		SCHOOLWAR_WINNER_SLOT_MAX = 15,
	};

	CSchoolWarRankingPageWinner ();
	virtual	~CSchoolWarRankingPageWinner ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CSchoolWarRankingSlot*		m_pWinnerSchool;
	CSchoolWarRankingSlot*		m_pWinnerSlot[SCHOOLWAR_WINNER_SLOT_MAX];
	CBasicScrollBarEx*			m_pScrollBar;

	int		m_nStart;
	int		m_nTotal;

public:
	void	RefreshWinners();
	void	RenderView();
};