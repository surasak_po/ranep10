#include "StdAfx.h"
#include "SchoolWarCTFAcquire.h"
#include "SchoolWarCTFSlot.h"
#include "../EngineLib/DxCommon/DxFontMan.h"
#include "../EngineUILib/GUInterface/BasicTextBox.h"
#include "../EngineLib/DxCommon/d3dfont.h"
#include "../Interface/BasicLineBox.h"
#include "../RanClientUILib/Interface/GameTextControl.h"
#include "../RanClientUILib/Interface/UITextControl.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarCTFAcquire::CSchoolWarCTFAcquire ()
	: m_pTimeTextUnder( NULL )
{
	for(int i = 0; i < CSchoolWarCTFSlot::MAX_SCHOOL_SLOT; i++ )
	{
		m_pTowerSlot[i] = NULL;
	}
}

CSchoolWarCTFAcquire::~CSchoolWarCTFAcquire ()
{
}

void CSchoolWarCTFAcquire::CreateSubControl ()
{	
	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, _DEFAULT_FONT_FLAG );

	const int nAlignLeft = TEXT_ALIGN_LEFT;
	const int nAlignCenter = TEXT_ALIGN_CENTER_X;	

	m_pBackLeft		= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_LEFT_EX" );
	m_pBackBody		= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_BODY_EX" );
	m_pBackRight	= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_RIGHT_EX" );
	m_pBackGray	= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_GRAY_EX" );

	CBasicTextBox* pTextBox = NULL;
	pTextBox = CreateStaticControl ( "SCHOOLWAR_CTFACQUIRE_NAME0", pFont, nAlignLeft );
	pTextBox->AddText ( ID2GAMEWORD("SCHOOLWAR_CTFACQUIRE_SLOT_TEXT_STATIC", 0) );
	pTextBox = CreateStaticControl ( "SCHOOLWAR_CTFACQUIRE_NAME1", pFont, nAlignLeft );
	pTextBox->AddText ( ID2GAMEWORD("SCHOOLWAR_CTFACQUIRE_SLOT_TEXT_STATIC", 1) );
	pTextBox = CreateStaticControl ( "SCHOOLWAR_CTFACQUIRE_NAME2", pFont, nAlignLeft );
	pTextBox->AddText ( ID2GAMEWORD("SCHOOLWAR_CTFACQUIRE_SLOT_TEXT_STATIC", 2) );

	CBasicLineBox* pBasicLineBox1 = new CBasicLineBox;
	pBasicLineBox1->CreateSub ( this, "BASIC_LINE_BOX_QUEST_LIST", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox1->CreateBaseBoxQuestList( "INFO_CTF_DISPLAY_BACKGROUND_LINE" );
	RegisterControl ( pBasicLineBox1 );

	CBasicLineBox* pBasicLineBox2 = new CBasicLineBox;
	pBasicLineBox2->CreateSub ( this, "BASIC_LINE_BOX_QUEST_LIST", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox2->CreateBaseBoxQuestList( "INFO_CTF_DISPLAY_BACKGROUND_LINE_EX" );
	RegisterControl ( pBasicLineBox2 );

	CString strKeyword[CSchoolWarCTFSlot::MAX_SCHOOL_SLOT] = 
	{
		"SCHOOLWAR_CTFACQUIRE_SLOT0",
		"SCHOOLWAR_CTFACQUIRE_SLOT1",
		"SCHOOLWAR_CTFACQUIRE_SLOT2"
	};
	for(int i = 0; i < CSchoolWarCTFSlot::MAX_SCHOOL_SLOT; i++ )
	{
		m_pTowerSlot[i] = CreateTowerSlot( strKeyword[i].GetString() );
	}

	m_pTimeTextUnder = CreateStaticControl( "SCHOOLWAR_CTFACQUIRE_TIMER_UNDER_TEXT_EX", pFont, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
}

void CSchoolWarCTFAcquire::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	SetAcquireInfo();

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
}

void CSchoolWarCTFAcquire::SetTimer( float fElapsedTime )
{

	CString strCombine;
	int nSec = (int)fElapsedTime%60;
	int nMin = (int)(fElapsedTime/60)%60;
	int nHour = (int)(fElapsedTime/60)/60;

	if ( fElapsedTime > 0.1f )
	{
		strCombine.Format( "Tyranny Under Way                 %02d : %02d : %02d", nHour, nMin, nSec);
		m_pTimeTextUnder->SetOneLineText( strCombine, NS_UITEXTCOLOR::GOLD );
	}

}

void CSchoolWarCTFAcquire::SetAcquireInfo()
{
	for(int i = 0; i < CSchoolWarCTFSlot::MAX_SCHOOL_SLOT; i++ )
	{
		WORD wSchool = GLGaeaClient::GetInstance().m_nCTFCapture[i];
		m_pTowerSlot[i]->SetAcquire(wSchool);
	}
}

CBasicTextBox* CSchoolWarCTFAcquire::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}

CSchoolWarCTFSlot* CSchoolWarCTFAcquire::CreateTowerSlot ( const char* szControlKeyword )
{
	CSchoolWarCTFSlot* pSlot = new CSchoolWarCTFSlot;
	pSlot->CreateSub ( this, szControlKeyword, UI_FLAG_DEFAULT );
	pSlot->CreateSubControl (); 
	RegisterControl ( pSlot );
	return pSlot;
}