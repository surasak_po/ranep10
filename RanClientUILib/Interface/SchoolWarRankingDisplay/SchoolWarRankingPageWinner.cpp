#include "StdAfx.h"
#include "SchoolWarRankingPageWinner.h"
#include "SchoolWarRankingSlot.h"

#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "../BasicScrollBarEx.h"
#include "../EngineUiLib/GUInterface/BasicScrollThumbFrame.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarRankingPageWinner::CSchoolWarRankingPageWinner () :
	m_pWinnerSchool( NULL )
	, m_pScrollBar( NULL )
	, m_nStart( 0 )
	, m_nTotal( 0 )
{
	for ( int i=0; i < SCHOOLWAR_WINNER_SLOT_MAX; ++ i )
	{
		m_pWinnerSlot[i] = NULL;
	}
}

CSchoolWarRankingPageWinner::~CSchoolWarRankingPageWinner ()
{
}

void CSchoolWarRankingPageWinner::CreateSubControl ()
{
	CBasicLineBox* pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWAR_WINNER_PAGE_LINE" );
	RegisterControl ( pBasicLineBox );

	pBasicLineBox = new CBasicLineBox;
	pBasicLineBox->CreateSub ( this, "BASIC_LINE_BOX_EDIT", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pBasicLineBox->CreateBaseBoxQuestList( "SCHOOLWAR_WINNER_PAGE_LINE_SCHOOL" );
	RegisterControl ( pBasicLineBox );

	std::string strSLOT[SCHOOLWAR_WINNER_SLOT_MAX] = 
	{
		"SCHOOLWAR_WINNER_SLOT_00",
		"SCHOOLWAR_WINNER_SLOT_01",
		"SCHOOLWAR_WINNER_SLOT_02",
		"SCHOOLWAR_WINNER_SLOT_03",
		"SCHOOLWAR_WINNER_SLOT_04",
		"SCHOOLWAR_WINNER_SLOT_05",
		"SCHOOLWAR_WINNER_SLOT_06",
		"SCHOOLWAR_WINNER_SLOT_07",
		"SCHOOLWAR_WINNER_SLOT_08",
		"SCHOOLWAR_WINNER_SLOT_09",
		"SCHOOLWAR_WINNER_SLOT_10",
		"SCHOOLWAR_WINNER_SLOT_11",
		"SCHOOLWAR_WINNER_SLOT_12",
		"SCHOOLWAR_WINNER_SLOT_13",
		"SCHOOLWAR_WINNER_SLOT_14",
	};

	for ( int i=0; i < SCHOOLWAR_WINNER_SLOT_MAX; ++ i )
	{
		m_pWinnerSlot[i] = new CSchoolWarRankingSlot;
		m_pWinnerSlot[i]->CreateSub ( this, strSLOT[i].c_str() );
		m_pWinnerSlot[i]->CreateSubControl ();
		RegisterControl ( m_pWinnerSlot[i] );	
		m_pWinnerSlot[i]->SetVisibleSingle( FALSE );
	}

	m_pWinnerSchool =  new CSchoolWarRankingSlot;
	m_pWinnerSchool->CreateSub ( this, "SCHOOLWAR_WINNER_SCHOOL" );
	m_pWinnerSchool->CreateSubControl ();
	RegisterControl ( m_pWinnerSchool );	
	m_pWinnerSchool->SetVisibleSingle( FALSE );

	m_pScrollBar= new CBasicScrollBarEx;
	m_pScrollBar->CreateSub ( this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE );
	m_pScrollBar->CreateBaseScrollBar ( "SCHOOLWAR_WINNER_SLOT_SCROLL" );
	m_pScrollBar->GetThumbFrame()->SetState ( 0, SCHOOLWAR_WINNER_SLOT_MAX );
	RegisterControl ( m_pScrollBar );
}

void CSchoolWarRankingPageWinner::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarWinnerPlayerVec.size();

	if ( m_pScrollBar )
	{
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal();

			if( nTotal != m_nTotal )
				m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWAR_WINNER_SLOT_MAX );	
		}
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal ();
			if ( nTotal <= SCHOOLWAR_WINNER_SLOT_MAX ) return ;
			const int nViewPerPage = pThumbFrame->GetViewPerPage ();

			if ( nViewPerPage < nTotal )
			{
				int nCurPos = 0;
				const int nMovableLine = nTotal - nViewPerPage;
				float fPercent = pThumbFrame->GetPercent ();
				nCurPos = (int)floor(fPercent * nMovableLine);
				if ( nCurPos < 0 ) nCurPos = 0;
				if ( m_nStart == nCurPos ) return;
				m_nStart = nCurPos;
				RenderView();
			}
		}
	}
}

void CSchoolWarRankingPageWinner::RefreshWinners()
{
	if ( m_pWinnerSchool )
	{
		m_pWinnerSchool->DataReset();
		m_pWinnerSchool->SetVisibleSingle( FALSE );
		const SSCHOOLWAR_RANK_SCHOOL_WINNER& sWinner = GLGaeaClient::GetInstance().m_SchoolWarSchoolWinner;
		if ( sWinner.bVALID )
		{
			m_pWinnerSchool->SetVisibleSingle( TRUE );
			CString strSCHOOL = GLCONST_CHAR::strSCHOOLNAME[sWinner.wSCHOOL].c_str();
			m_pWinnerSchool->DataUpdate( sWinner.wRanking, strSCHOOL.GetString(), sWinner.wSCHOOL, sWinner.wKillNum, sWinner.wDeathNum );
		}
	}

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarWinnerPlayerVec.size();

	RenderView();
}

void CSchoolWarRankingPageWinner::RenderView()
{
	for ( int i=0; i < SCHOOLWAR_WINNER_SLOT_MAX; ++ i )
	{
		if ( m_pWinnerSlot[i] )
		{
			m_pWinnerSlot[i]->DataReset();
			m_pWinnerSlot[i]->SetVisibleSingle( FALSE );
		}
	}

	SCHOOLWAR_RANK_PLAYER_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SchoolWarWinnerPlayerVec;

	int nEnd = m_nStart + SCHOOLWAR_WINNER_SLOT_MAX;
	int nSlot = 0;

	for ( int i = m_nStart; i < nEnd; ++ i )
	{
		if ( i >= (int)vecRank.size() )	continue;
		if ( m_pWinnerSlot[nSlot] )
		{
			SSCHOOLWAR_RANK_PLAYER_CLIENT sRANK = vecRank[i];
			m_pWinnerSlot[nSlot]->SetVisibleSingle( TRUE );
			m_pWinnerSlot[nSlot]->DataUpdate( sRANK.wRanking, sRANK.szCharName, sRANK.wSCHOOL, sRANK.wKillNum, sRANK.wDeathNum, sRANK.dwCharID );
			nSlot ++ ;
		}
	}
}
