#include "StdAfx.h"
#include "SchoolWarCTFSlot.h"

#include "../RanClientUILib/Interface/GameTextControl.h"
#include "../RanClientUILib/Interface/UITextControl.h"

#include "../EngineUILib/GUInterface/BasicTextBox.h"

#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../EngineLib/DxCommon/d3dfont.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarCTFSlot::CSchoolWarCTFSlot ()
	: m_pTowerName( NULL )
{
	for(int i = 0; i < MAX_SCHOOL_SLOT; i++ )
		m_pSchoolIcon[i] = NULL;
}

CSchoolWarCTFSlot::~CSchoolWarCTFSlot ()
{
}

void CSchoolWarCTFSlot::CreateSubControl ()
{
	m_pSchoolNone = CreateControl( "SCHOOLWAR_SLOT_NONE" );

	CString strSchool[MAX_SCHOOL_SLOT] = 
	{
		"SCHOOLWAR_SLOT_SG",
		"SCHOOLWAR_SLOT_MP",
		"SCHOOLWAR_SLOT_PHX"
	};

	for(int i = 0; i < MAX_SCHOOL_SLOT; i++ )
	{
		m_pSchoolIcon[i] = CreateControl( strSchool[i].GetString() );
	}

	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, D3DFONT_SHADOW | D3DFONT_ASCII );
	m_pTowerName = CreateStaticControl( "SCHOOLWAR_SLOT_NAME", pFont, TEXT_ALIGN_RIGHT );
}

void CSchoolWarCTFSlot::SetAcquire( WORD wSchool )
{
	for(int i = 0; i < MAX_SCHOOL_SLOT; i++ )
		m_pSchoolIcon[i]->SetVisibleSingle ( FALSE );
	m_pSchoolNone->SetVisibleSingle( FALSE );

	if ( wSchool < 0 || wSchool >= MAX_SCHOOL_SLOT ) 
		m_pSchoolNone->SetVisibleSingle( TRUE );
	else
		m_pSchoolIcon[wSchool]->SetVisibleSingle( TRUE );
}

CUIControl*	CSchoolWarCTFSlot::CreateControl ( const char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}

CBasicTextBox* CSchoolWarCTFSlot::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}