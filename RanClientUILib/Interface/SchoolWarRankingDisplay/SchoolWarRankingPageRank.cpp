#include "StdAfx.h"
#include "SchoolWarRankingPageRank.h"
#include "SchoolWarRankingSlot.h"

#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "../BasicScrollBarEx.h"
#include "../EngineUiLib/GUInterface/BasicScrollThumbFrame.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarRankingPageRank::CSchoolWarRankingPageRank () :
	m_pSlotOwn( NULL )
	, m_pScrollBar( NULL )
	, m_nStart( 0 )
	, m_nTotal( 0 )
{
	for ( int i=0; i < SCHOOLWAR_PLAYER_SLOT_MAX; ++ i )
	{
		m_pSlotPlayer[i] = NULL;
	}
}

CSchoolWarRankingPageRank::~CSchoolWarRankingPageRank ()
{
}

void CSchoolWarRankingPageRank::CreateSubControl ()
{
	std::string strSLOT_PLAYER[SCHOOLWAR_PLAYER_SLOT_MAX] = 
	{
		"SCHOOLWAR_RANKING_SLOT_00",
		"SCHOOLWAR_RANKING_SLOT_01",
		"SCHOOLWAR_RANKING_SLOT_02",
		"SCHOOLWAR_RANKING_SLOT_03",
		"SCHOOLWAR_RANKING_SLOT_04",
		"SCHOOLWAR_RANKING_SLOT_05",
		"SCHOOLWAR_RANKING_SLOT_06",
		"SCHOOLWAR_RANKING_SLOT_07",
		"SCHOOLWAR_RANKING_SLOT_08",
		"SCHOOLWAR_RANKING_SLOT_09",
	};

	for ( int i=0; i < SCHOOLWAR_PLAYER_SLOT_MAX; ++ i )
	{
		m_pSlotPlayer[i] = new CSchoolWarRankingSlot;
		m_pSlotPlayer[i]->CreateSub ( this, strSLOT_PLAYER[i].c_str() );
		m_pSlotPlayer[i]->CreateSubControl ();
		m_pSlotPlayer[i]->SetSelf(false);
		RegisterControl ( m_pSlotPlayer[i] );	
		m_pSlotPlayer[i]->SetVisibleSingle( FALSE );
	}

	m_pSlotOwn =  new CSchoolWarRankingSlot;
	m_pSlotOwn->CreateSub ( this, "SCHOOLWAR_RANKING_SLOT_MYSCORE" );
	m_pSlotOwn->CreateSubControl ();
	m_pSlotOwn->SetSelf(true);
	RegisterControl ( m_pSlotOwn );	
	m_pSlotOwn->SetVisibleSingle( FALSE );

	m_pScrollBar= new CBasicScrollBarEx;
	m_pScrollBar->CreateSub ( this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE );
	m_pScrollBar->CreateBaseScrollBar ( "SCHOOLWAR_RANKING_SLOT_SCROLL" );
	m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWAR_PLAYER_SLOT_MAX );
	RegisterControl ( m_pScrollBar );
}

void CSchoolWarRankingPageRank::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarRankingPlayerVec.size();

	if ( m_pScrollBar )
	{
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal();

			if( nTotal != m_nTotal )
				m_pScrollBar->GetThumbFrame()->SetState ( m_nTotal, SCHOOLWAR_PLAYER_SLOT_MAX );	
		}
		{
			CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame ();
			int nTotal = pThumbFrame->GetTotal ();
			if ( nTotal <= SCHOOLWAR_PLAYER_SLOT_MAX ) return ;
			const int nViewPerPage = pThumbFrame->GetViewPerPage ();

			if ( nViewPerPage < nTotal )
			{
				int nCurPos = 0;
				const int nMovableLine = nTotal - nViewPerPage;
				float fPercent = pThumbFrame->GetPercent ();
				nCurPos = (int)floor(fPercent * nMovableLine);
				if ( nCurPos < 0 ) nCurPos = 0;
				if ( m_nStart == nCurPos ) return;
				m_nStart = nCurPos;
				RenderView();
			}
		}
	}
}

void CSchoolWarRankingPageRank::RefreshRanking(int nSchool)
{
	if ( m_pSlotOwn )
	{
		m_pSlotOwn->DataReset();
		m_pSlotOwn->SetVisibleSingle( FALSE );
	}

	if ( m_pSlotOwn )
	{
		const SSCHOOLWAR_RANK_SELF& sMyRank = GLGaeaClient::GetInstance().m_SchoolWarSelfRank;

		if ( sMyRank.bVALID )
		{
			CString strNAME = GLGaeaClient::GetInstance().GetCharacter()->m_szName;
			m_pSlotOwn->SetVisibleSingle( TRUE );
			m_pSlotOwn->DataUpdate( sMyRank.wRanking, strNAME.GetString(), sMyRank.wSCHOOL, sMyRank.wKillNum, sMyRank.wDeathNum );
		}
	}

	m_nTotal = (int)GLGaeaClient::GetInstance().m_SchoolWarRankingPlayerVec.size();

	RenderView(nSchool);
}

void CSchoolWarRankingPageRank::RenderView(int nSchool)
{
	for ( int i=0; i < SCHOOLWAR_PLAYER_SLOT_MAX; ++ i )
	{
		if ( m_pSlotPlayer[i] )
		{
			m_pSlotPlayer[i]->DataReset();
			m_pSlotPlayer[i]->SetVisibleSingle( FALSE );
		}
	}

	SCHOOLWAR_RANK_PLAYER_CLIENT_VEC& vecRank = GLGaeaClient::GetInstance().m_SchoolWarRankingPlayerVec;

	int nEnd = m_nStart + SCHOOLWAR_PLAYER_SLOT_MAX;
	int nSlot = 0;

	for ( int i = m_nStart; i < nEnd; ++ i )
	{
		if ( i >= (int)vecRank.size() )	continue;
		if ( m_pSlotPlayer[nSlot] )
		{
			SSCHOOLWAR_RANK_PLAYER_CLIENT sRANK = vecRank[i];
			if( nSchool != 4 && sRANK.wSCHOOL != nSchool ) continue;
			m_pSlotPlayer[nSlot]->SetVisibleSingle( TRUE );
			m_pSlotPlayer[nSlot]->DataUpdate( sRANK.wRanking, sRANK.szCharName, sRANK.wSCHOOL, sRANK.wKillNum, sRANK.wDeathNum, sRANK.dwCharID );
			nSlot ++ ;
		}
	}
}
