#include "StdAfx.h"
#include "TyrannyNoticeDisplay.h"
#include "TyrannyNoticeType.h"
#include "../RanClientUILib/Interface/InnerInterface.h"
#include "GLGaeaClient.h"
#include "DxViewPort.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CTyrannyNoticeDisplay::CTyrannyNoticeDisplay ()
{
}

CTyrannyNoticeDisplay::~CTyrannyNoticeDisplay ()
{
}

void CTyrannyNoticeDisplay::CreateSubControl ()
{
	CString strKeyword[MAXTYPE] = 
	{
     	"TYRANNY_BEGIN"
		"TYRANNY_END"
		"TYRANNY_CAPTURE00"
		"TYRANNY_CAPTURE01"
		"TYRANNY_CAPTURE02"
	};

	for ( int i = 0; i < MAXTYPE; ++i )
	{
		m_pCTF_TYPE[i] = new CTyrannyNoticeType;
		m_pCTF_TYPE[i]->CreateSub ( this, strKeyword[i].GetString(), UI_FLAG_DEFAULT, TYRANNY_BEGIN + i );
		m_pCTF_TYPE[i]->CreateSubControl ( strKeyword[i] );
		m_pCTF_TYPE[i]->SetVisibleSingle ( FALSE );
		m_pCTF_TYPE[i]->SetUseRender ( FALSE );
		m_pCTF_TYPE[i]->STOP ();
		m_pCTF_TYPE[i]->RESET ();		
		RegisterControl ( m_pCTF_TYPE[i] );

        m_pCTF_TYPE_DELAY[i] = new CUIControl;
		m_pCTF_TYPE_DELAY[i]->CreateSub ( this, strKeyword[i].GetString() );
		m_pCTF_TYPE_DELAY[i]->SetVisibleSingle ( FALSE );
		RegisterControl ( m_pCTF_TYPE_DELAY[i] );
	}

	m_pPositionControl = new CUIControl;
	m_pPositionControl->CreateSub ( this, "TYRANNY_NOTICE_DISPLAY_EX" );
	m_pPositionControl->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pPositionControl );
}

bool	CTyrannyNoticeDisplay::START ( UIGUID cID )
{
	//if ( cID < TYRANNY_BEGIN || TYRANNY_CAPTURE02 < cID ) return false;
	
	int nIndex = cID - TYRANNY_BEGIN;
	m_pCTF_TYPE[nIndex]->SetVisibleSingle ( TRUE );
	m_pCTF_TYPE[nIndex]->START ();

	return true;
}

bool	CTyrannyNoticeDisplay::RESET ( UIGUID cID )
{
	//if ( cID < TYRANNY_BEGIN || TYRANNY_CAPTURE02 < cID ) return false;

	int nIndex = cID - TYRANNY_BEGIN;	
	m_pCTF_TYPE[nIndex]->RESET ();

    return true;
}

bool	CTyrannyNoticeDisplay::STOP ( UIGUID cID )
{
	//if ( cID < TYRANNY_BEGIN || TYRANNY_CAPTURE02 < cID ) return false;

	int nIndex = cID - TYRANNY_BEGIN;	
	m_pCTF_TYPE[nIndex]->STOP ();
	m_pCTF_TYPE[nIndex]->SetVisibleSingle ( FALSE );

	return true;
}

void CTyrannyNoticeDisplay::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ) return ;

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	D3DXVECTOR3 vPos = GLGaeaClient::GetInstance().GetCharacter()->GetPosBodyHeight();

	static D3DXVECTOR3 vScreenBack;
	D3DXVECTOR3 vScreen = DxViewPort::GetInstance().ComputeVec3Project ( &vPos, NULL );

	// 마우스 움직임에 흔들림을 보정한다.
	if( abs( vScreenBack.x - vScreen.x ) < 1.0f )
	{
		vScreen.x = vScreenBack.x;
	}
	
	bool bPLAYING( false );
	bool bKEEPING( false );

	for ( int i = 0; i < MAXTYPE; ++i )
	{
		const UIRECT& rcOriginPos = m_pCTF_TYPE[i]->GetGlobalPos ();

		D3DXVECTOR2 vPos;
		vPos.x = floor(vScreen.x - ( rcOriginPos.sizeX * 0.5f ));
		vPos.y = m_pPositionControl->GetGlobalPos().top;

		if ( m_pCTF_TYPE[i]->ISPLAYING () )
		{
			m_pCTF_TYPE[i]->SetGlobalPos ( vPos );

			bPLAYING = true;
		}
		else
		{
			STOP ( TYRANNY_BEGIN + i );
		}

		if ( m_pCTF_TYPE[i]->IsVisible () )
		{
			m_pCTF_TYPE[i]->SetGlobalPos ( vPos );

			bKEEPING = true;
		}
	}

	vScreenBack = vScreen;

	if ( !bPLAYING && !bKEEPING ) CInnerInterface::GetInstance().HideGroup ( GetWndID () );
}

void CTyrannyNoticeDisplay::ALLSTOP ()
{
	for ( int i = 0; i < MAXTYPE; ++i )
	{
		m_pCTF_TYPE[i]->STOP ();
		m_pCTF_TYPE[i]->RESET ();
		m_pCTF_TYPE[i]->SetVisibleSingle ( FALSE );

		m_pCTF_TYPE_DELAY[i]->SetVisibleSingle ( FALSE );
	}

	CInnerInterface::GetInstance().HideGroup ( GetWndID () );
}

bool CTyrannyNoticeDisplay::KEEP_START ( UIGUID cID )
{
	if ( cID < TYRANNY_BEGIN || TYRANNY_CAPTURE02 < cID ) return false;

	int nIndex = cID - TYRANNY_BEGIN;	

	m_pCTF_TYPE_DELAY[nIndex]->SetVisibleSingle ( TRUE );
	m_pCTF_TYPE_DELAY[nIndex]->SetDiffuseAlpha ( 50 );

	return true;
}

void CTyrannyNoticeDisplay::KEEP_STOP ()
{
	for ( int i = 0; i < MAXTYPE; ++i )
	{
		m_pCTF_TYPE_DELAY[i]->SetVisibleSingle ( FALSE );
	}
}