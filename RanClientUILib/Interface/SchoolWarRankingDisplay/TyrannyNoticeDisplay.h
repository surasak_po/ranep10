#pragma	once

#include "../EngineUIlib/GUInterface/UIGroup.h"

class	CTyrannyNoticeType;
class	CTyrannyNoticeDisplay : public CUIGroup
{
public:
	enum
	{
		UI_TYRANNY_BEGIN = NO_ID + 1,
		UI_TYRANNY_END,
		UI_TYRANNY_CAPTURE00,
		UI_TYRANNY_CAPTURE01,
		UI_TYRANNY_CAPTURE02,
		MAXTYPE = 5
	};

public:
	CTyrannyNoticeDisplay ();
	virtual	~CTyrannyNoticeDisplay ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

public:
	bool	START ( UIGUID cID );
	bool	RESET ( UIGUID cID );
	bool	STOP ( UIGUID cID );

	void	ALLSTOP ();

public:
	bool	KEEP_START ( UIGUID cID );
	void	KEEP_STOP ();

private:
	CTyrannyNoticeType*	m_pCTF_TYPE[MAXTYPE];
	CUIControl*			m_pCTF_TYPE_DELAY[MAXTYPE];

	CUIControl*		m_pPositionControl;	
};