#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "GLCharDefine.h"

class	CBasicTextBox;
class	CSchoolWarCTFSlot : public CUIGroup
{
public:
	enum
	{
		MAX_SCHOOL_SLOT = 3,
	};

	CSchoolWarCTFSlot ();
	virtual	~CSchoolWarCTFSlot ();

	void			CreateSubControl ();
	//void			CreateName( CString strName );
	void			SetAcquire( WORD wSchool );

protected:
	CUIControl*		CreateControl ( const char* szControl );
	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign );

	CUIControl*		m_pSchoolIcon[MAX_SCHOOL_SLOT];
	CUIControl*		m_pSchoolNone;

	CBasicTextBox*	m_pTowerName;
};