#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"
#include "GLCharDefine.h"
#include "GLParty.h"
#include "../EngineUILib/GUInterface/BasicTextBox.h"
#include "../EngineUILib/GUInterface/BasicProgressBar.h"

class	CBasicTextBox;
class	CBasicProgressBar;
class	CBasicLineBox;

class	CSchoolWarRankingSlot : public CUIGroup
{
public:
	CSchoolWarRankingSlot ();
	virtual	~CSchoolWarRankingSlot ();

public:
	void	CreateSubControl ();

private:
	CBasicTextBox*		m_pRankNum;
	CUIControl*			m_pRankSchool[3];
	CBasicTextBox*		m_pRankName;
	CBasicTextBox*		m_pRankKill;
	CBasicTextBox*		m_pRankDeath;
	bool				m_bSelf;
protected:
	CUIControl*		CreateControl ( const char* szControl );
	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR D3DCOLOR, int nAlign );

public:
	void	DataUpdate( int nNUM, std::string strNAME, int nSCHOOL, int nKILL, int nDEATH , DWORD dwCHARID = UINT_MAX );
	void	DataReset();
	void	SetSelf(bool bSelf) { m_bSelf = bSelf; }
	bool	IsSelf() { return m_bSelf; }
};