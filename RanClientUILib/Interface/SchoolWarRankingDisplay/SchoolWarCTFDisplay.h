#pragma	once

#include "../UIWindowEx.h"

class	CSchoolWarRankingPageRank;
class	CSchoolWarRankingPageWinner;
class	CBasicLineBoxEx;
class	CBasicTextBox;
class	CSchoolWarCTFDisplay : public CUIWindowEx
{
public:
	CSchoolWarCTFDisplay ();
	virtual	~CSchoolWarCTFDisplay ();

	void CreateSubControl ();

public:
	virtual	void SetVisibleSingle ( BOOL bVisible );
	void Refresh();
	void SetWarTime(float fElapsedTime);

	CUIControl*	m_pBackLeft;
	CUIControl*	m_pBackBody;
	CUIControl*	m_pBackRight;

	CUIControl*	CreateControl ( const char* szControl );

	CBasicTextBox* m_pWinnerText;

	CUIControl*	m_pWinnerIconNone;
	CUIControl*	m_pWinnerIcon[3];
	CBasicTextBox*	m_pTimeTextUnder;
	CBasicTextBox*	m_pTimeTextNext;
public:
	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign );

	void CheckMouseState ();

	virtual void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

	bool m_bCHECK_MOUSE_STATE;
	BOOL m_bFirstGap;
	D3DXVECTOR2	m_vGap;
	int			m_PosX;
	int			m_PosY;
};