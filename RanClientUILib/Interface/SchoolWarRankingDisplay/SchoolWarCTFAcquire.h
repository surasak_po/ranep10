#pragma	once

#include "../UIWindowEx.h"
#include "SchoolWarCTFSlot.h"
class CBasicTextBox;
class CSchoolWarCTFSlot;
class CSchoolWarCTFAcquire : public CUIWindowEx
{
public:
	CSchoolWarCTFAcquire ();
	virtual	~CSchoolWarCTFAcquire ();

	void CreateSubControl ();

	void SetTimer( float fElapsedTime );
	void SetAcquireInfo();
	CUIControl*	m_pBackLeft;
	CUIControl*	m_pBackBody;
	CUIControl*	m_pBackRight;
	CUIControl*	m_pLine1;
	CUIControl*	m_pLine2;
	CUIControl*	m_pBackGray;		

protected:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	CBasicTextBox* CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign );
	CSchoolWarCTFSlot* CreateTowerSlot ( const char* szControlKeyword );

private:
	CSchoolWarCTFSlot* m_pTowerSlot[CSchoolWarCTFSlot::MAX_SCHOOL_SLOT];
	CBasicTextBox* m_pTimeTextUnder;
};