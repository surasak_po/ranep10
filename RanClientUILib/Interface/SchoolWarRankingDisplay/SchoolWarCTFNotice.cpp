#include "StdAfx.h"
#include "SchoolWarCTFNotice.h"
#include "../RanClientUILib/Interface/InnerInterface.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarCTFNotice::CSchoolWarCTFNotice ()
	: m_pBack( NULL )
	, m_pBackNotice( NULL )
	, m_pBegin( NULL )
	, m_pEnd( NULL )
	, m_emType( EM_NOTICE_NULL )
	, m_bBegin( false )
	, m_fElapsedTime( 0.0f )
	, m_wSchool( -1 )
{
	for(int i = 0; i < MAX_SCHOOL_NOTICE; i++ ) 
	{
		m_pIconNotice[i] = NULL;
		m_pIconNoticeText[i] = NULL;
	}
}

CSchoolWarCTFNotice::~CSchoolWarCTFNotice ()
{
}

void CSchoolWarCTFNotice::CreateSubControl ()
{	
	m_pBack = CreateControl( "SCHOOLWAR_CTFNOTICE_BACK" );
	m_pBackNotice = CreateControl( "SCHOOLWAR_CTFNOTICE_BACK_NOTICE" );

/*	for(int i = 0; i < MAX_SCHOOL_NOTICE; i++ )
	{
		CString strKeyword;
		strKeyword.Format( "SCHOOLWAR_CTFNOTICE_ICON0%d", i );
		m_pIconNotice[i] = CreateControl( strKeyword );

		strKeyword.Format( "SCHOOLWAR_CTFNOTICE_ICON_TEXT0%d", i );
		m_pIconNoticeText[i] = CreateControl( strKeyword );
	}*/

	m_pBegin = CreateControl( "SCHOOLWAR_CTFNOTICE_START" );
	m_pEnd = CreateControl( "SCHOOLWAR_CTFNOTICE_END" );

	ResetAll();
}

void CSchoolWarCTFNotice::ResetAll()
{
	m_pBack->SetVisibleSingle( FALSE );
	m_pBackNotice->SetVisibleSingle( FALSE );

	//for(int i = 0; i < MAX_SCHOOL_NOTICE; i++ )
	//	m_pIconNotice[i]->SetVisibleSingle( FALSE );

	m_pBegin->SetVisibleSingle( FALSE );
	m_pEnd->SetVisibleSingle( FALSE );

	m_emType = EM_NOTICE_NULL;
	
	m_fElapsedTime = 0.0f;
	m_wSchool = -1;
	m_bBegin = false;
}

void CSchoolWarCTFNotice::SetNotice( WORD wSchool )
{
	if ( wSchool != -1 )
	{
		//m_emType = emType;
		m_bBegin = true;
		m_wSchool = wSchool;
	}
}

void CSchoolWarCTFNotice::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	//if ( m_emType != EM_NOTICE_NULL )
		//m_bBegin = true;

	if ( m_bBegin )
	{
		m_fElapsedTime += fElapsedTime;

		if ( m_fElapsedTime >= 5.0f )
		{
			m_bBegin = false;
		}
		else
		{
			m_pBack->SetVisibleSingle( FALSE );
			m_pBackNotice->SetVisibleSingle( FALSE );

			//for(int i = 0; i < MAX_SCHOOL_NOTICE; i++ )
			//	m_pIconNotice[i]->SetVisibleSingle( FALSE );

			m_pBegin->SetVisibleSingle( FALSE );
			m_pEnd->SetVisibleSingle( FALSE );

			switch( m_wSchool )
			{
			case 3:
				m_pBack->SetVisibleSingle( TRUE );
				m_pBegin->SetVisibleSingle( TRUE );
				break;
			case 4:
				m_pBack->SetVisibleSingle( TRUE );
				m_pEnd->SetVisibleSingle( TRUE );
				break;
			/*case 0:
			case 1:
			case 2:
			int wSchool = (int)GLGaeaClient::GetInstance().GetCharacter()->GETSCHOOL();
			//pChar->
			if ( wSchool == 0 )
			{
				m_pBackNotice->SetVisibleSingle( TRUE );
				m_pIconNotice[0]->SetVisibleSingle( TRUE );
				m_pIconNoticeText[0]->SetVisibleSingle( TRUE );
			}else if ( wSchool == 1 ){
				m_pBackNotice->SetVisibleSingle( TRUE );
				m_pIconNotice[1]->SetVisibleSingle( TRUE );
				m_pIconNoticeText[1]->SetVisibleSingle( TRUE );
			}else if ( wSchool == 2 ){
				m_pBackNotice->SetVisibleSingle( TRUE );
				m_pIconNotice[2]->SetVisibleSingle( TRUE );
				m_pIconNoticeText[2]->SetVisibleSingle( TRUE );				
			    }
				break;*/
			}
		}
	}
	else
	{
		ResetAll();
		CInnerInterface::GetInstance().HideGroup( GetWndID() );
	}
}