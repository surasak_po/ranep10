#include "StdAfx.h"
#include "SchoolWarCTFDisplay.h"
#include "../InnerInterface.h"
#include "../UITextControl.h"
#include "../GameTextControl.h"
#include "../EngineLib/DxCommon/DxFontMan.h"
#include "../EngineLib/DxCommon/d3dfont.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarCTFDisplay::CSchoolWarCTFDisplay ():
	  m_pBackLeft( NULL )
	, m_pBackBody( NULL )
	, m_pBackRight( NULL )
	, m_pWinnerText( NULL )
	, m_pWinnerIconNone( NULL )
	, m_pTimeTextUnder( NULL )
	, m_pTimeTextNext( NULL )
	, m_bCHECK_MOUSE_STATE( false )
	, m_bFirstGap(FALSE)
	, m_PosX(0)
	, m_PosY(0)
{
	for(int i = 0; i < 3; i++ )
	{
		m_pWinnerIcon[i] = NULL;
	}
}

CSchoolWarCTFDisplay::~CSchoolWarCTFDisplay ()
{
}

CUIControl*	CSchoolWarCTFDisplay::CreateControl ( const char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}


void CSchoolWarCTFDisplay::CreateSubControl ()
{	
	m_pBackLeft		= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_LEFT" );
	m_pBackBody		= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_BODY" );
	m_pBackRight	= CreateControl( "INFO_CTF_DISPLAY_BACKGROUND_RIGHT" );

	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, D3DFONT_SHADOW | D3DFONT_ASCII );

	m_pWinnerText	= CreateStaticControl( "INFO_CTF_DISPLAY_WINNER_TEXT", pFont, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
	m_pWinnerText->SetText ( ID2GAMEWORD("INFO_CTF_DISPLAY_WINNER_TEXT_STATIC",0), NS_UITEXTCOLOR::WHITE );

	m_pTimeTextUnder	= CreateStaticControl( "INFO_CTF_DISPLAY_TIME_UNDER_TEXT", pFont, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );
	m_pTimeTextNext		= CreateStaticControl( "INFO_CTF_DISPLAY_TIME_NEXT_TEXT", pFont, TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y );

	m_pWinnerIconNone = CreateControl( "INFO_CTF_DISPLAY_WINNER_ICON_NONE_EX" );

	CString strSchool[3] = 
	{
		"INFO_CTF_DISPLAY_WINNER_ICON_SG_EX",
		"INFO_CTF_DISPLAY_WINNER_ICON_MP_EX",
		"INFO_CTF_DISPLAY_WINNER_ICON_PHX_EX"
	};

	for(int i = 0; i < 3; i++ )
	{
		m_pWinnerIcon[i] = CreateControl( (char*)strSchool[i].GetString() );
		m_pWinnerIcon[i]->SetVisibleSingle(FALSE);
	}
}

void CSchoolWarCTFDisplay::SetVisibleSingle ( BOOL bVisible )
{
	CUIWindowEx::SetVisibleSingle( bVisible );
}
void CSchoolWarCTFDisplay::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	m_bCHECK_MOUSE_STATE = false;

	m_PosX = x;
	m_PosY = y;

	Refresh();

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );
	
	if ( IsExclusiveSelfControl() )
	{
		SetGlobalPos ( D3DXVECTOR2 ( x - m_vGap.x, y - m_vGap.y ) );
	}
}

void CSchoolWarCTFDisplay::Refresh()
{
	for(int i = 0; i < 3; i++ ) 
		m_pWinnerIcon[i]->SetVisibleSingle(FALSE);

	int nWinner = GLGaeaClient::GetInstance().m_nCTFWinner;
	if ( nWinner < 0 || nWinner > 2 )
	{
		m_pWinnerIconNone->SetVisibleSingle(TRUE);
		return;
	}

	m_pWinnerIconNone->SetVisibleSingle(FALSE);

	if ( m_pWinnerIcon[nWinner] )
		m_pWinnerIcon[nWinner]->SetVisibleSingle(TRUE);
}

CBasicTextBox* CSchoolWarCTFDisplay::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}

void CSchoolWarCTFDisplay::SetWarTime(float fElapsedTime)
{
	CString strCombine;
	int nSec = (int)fElapsedTime%60;
	int nMin = (int)(fElapsedTime/60)%60;
	int nHour = (int)(fElapsedTime/60)/60;
	
	if ( fElapsedTime > 0.1f )
	{
	strCombine.Format("Next Tyranny Schedule: %02d : %02d : %02d", nHour, nMin, nSec);
     m_pTimeTextNext->SetOneLineText( strCombine, NS_UITEXTCOLOR::BRIGHTGREEN );
	}
}

void CSchoolWarCTFDisplay::CheckMouseState ()
{
	const DWORD dwMsg = GetMessageEx ();
	if ( CHECK_MOUSE_IN ( dwMsg ) )
	{
		if( UIMSG_LB_DUP & dwMsg )
			return ;
		
		if ( dwMsg & UIMSG_LB_DOWN )
		{
			SetExclusiveControl();	

			if ( !m_bFirstGap )
			{
				UIRECT rcPos = GetGlobalPos ();
				m_vGap.x = m_PosX - rcPos.left;
				m_vGap.y = m_PosY - rcPos.top;
				m_bFirstGap = TRUE;
			}
		}
		else if ( CHECK_LB_UP_LIKE ( dwMsg ) )
		{
			ResetExclusiveControl();
			m_bFirstGap = FALSE;				
		}
	}
	else if ( CHECK_LB_UP_LIKE ( dwMsg ) )		
	{								
		ResetExclusiveControl();
		m_bFirstGap = FALSE;					
	}
}

void CSchoolWarCTFDisplay::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindowEx::TranslateUIMessage ( ControlID, dwMsg );

	if ( !m_bCHECK_MOUSE_STATE )
	{
		CheckMouseState ();
		m_bCHECK_MOUSE_STATE = true;
	}
}