#pragma	once

#include "../UIWindowEx.h"
class CSchoolWarCTFNotice : public CUIWindowEx
{
protected:
	enum
	{
		MAX_SCHOOL_NOTICE = 3,
	};

public:
	enum EM_NOTICE_TYPE
	{
		EM_NOTICE_NULL	= 0,
		EM_NOTICE_BEGIN = 1,
		EM_NOTICE_END	= 2,
		EM_NOTICE_SG	= 3,
		EM_NOTICE_MP	= 4,
		EM_NOTICE_PHX	= 5
	};

	CSchoolWarCTFNotice ();
	virtual	~CSchoolWarCTFNotice ();

	void CreateSubControl ();

	void ResetAll();
	void SetNotice( WORD wSchool = -1 );

private:
	CUIControl*		m_pBack;
	CUIControl*		m_pBackNotice;

	CUIControl*		m_pIconNotice[MAX_SCHOOL_NOTICE];
	CUIControl*		m_pIconNoticeText[MAX_SCHOOL_NOTICE];

	CUIControl*		m_pBegin;
	CUIControl*		m_pEnd;

	EM_NOTICE_TYPE	m_emType;

	bool			m_bBegin;

	float			m_fElapsedTime;

	WORD			m_wSchool;

protected:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
};