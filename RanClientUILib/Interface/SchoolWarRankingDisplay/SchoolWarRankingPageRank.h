#pragma	once

#include "../EngineUILib/GUInterface/UIGroup.h"

#include "GLSchoolWar/GLSchoolWarData.h"
#include "GLSchoolWar/GLSchoolWar.h"
#include "GLSchoolWar/GLSchoolWarMan.h"

class	CSchoolWarRankingSlot;
class	CBasicScrollBarEx;

class	CSchoolWarRankingPageRank : public CUIGroup
{
public:
	enum
	{
		SCHOOLWAR_SCHOOL_SLOT_MAX = 3,
		SCHOOLWAR_PLAYER_SLOT_MAX = 10,
	};

	CSchoolWarRankingPageRank ();
	virtual	~CSchoolWarRankingPageRank ();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CSchoolWarRankingSlot*	m_pSlotSchool[SCHOOLWAR_SCHOOL_SLOT_MAX];
	CSchoolWarRankingSlot*	m_pSlotPlayer[SCHOOLWAR_PLAYER_SLOT_MAX];
	CSchoolWarRankingSlot*	m_pSlotOwn;
	CBasicScrollBarEx*		m_pScrollBar;

	int		m_nStart;
	int		m_nTotal;
	
public:
	void	RefreshRanking(int nSchool = 4);
	void	RenderView(int nSchool = 4);
};