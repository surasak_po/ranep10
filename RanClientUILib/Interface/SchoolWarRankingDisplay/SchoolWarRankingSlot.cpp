#include "StdAfx.h"
#include "SchoolWarRankingSlot.h"
#include "../../EngineLib/DxCommon/DxFontMan.h"
#include "../../EngineLib/DxCommon/d3dfont.h"
#include "../GameTextControl.h"
#include "../UITextControl.h"
#include "../BasicLineBox.h"
#include "GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSchoolWarRankingSlot::CSchoolWarRankingSlot () :
	m_pRankNum( NULL ),
	m_pRankName( NULL ),
	m_pRankKill( NULL ),
	m_pRankDeath( NULL ),
	m_bSelf(false)
{
	for ( int i=0; i<3; ++i )
	{
		m_pRankSchool[i] = NULL;
	}
}

CSchoolWarRankingSlot::~CSchoolWarRankingSlot ()
{
}

void CSchoolWarRankingSlot::CreateSubControl ()
{
	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, D3DFONT_SHADOW | D3DFONT_ASCII );

	CString strSchool[3] = 
	{
		"SCHOOLWAR_RANKING_SLOT_SCHOOL_SM",
		"SCHOOLWAR_RANKING_SLOT_SCHOOL_HA",
		"SCHOOLWAR_RANKING_SLOT_SCHOOL_BH"
	};

	for ( int i = 0; i < 3; ++ i )
	{
		m_pRankSchool[i] = CreateControl( strSchool[i].GetString() );
	}

	m_pRankNum = CreateStaticControl ( "SCHOOLWAR_RANKING_SLOT_NUM_TEXT", pFont, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X );
	m_pRankName = CreateStaticControl ( "SCHOOLWAR_RANKING_SLOT_NAME", pFont, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X );
	m_pRankKill = CreateStaticControl ( "SCHOOLWAR_RANKING_SLOT_KILL_TEXT", pFont, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X );
	m_pRankDeath = CreateStaticControl ( "SCHOOLWAR_RANKING_SLOT_DEATH_TEXT", pFont, NS_UITEXTCOLOR::WHITE, TEXT_ALIGN_CENTER_X );
}

CUIControl*	CSchoolWarRankingSlot::CreateControl ( const char* szControl )
{
	CUIControl* pControl = new CUIControl;
	pControl->CreateSub ( this, szControl );
	RegisterControl ( pControl );
	return pControl;
}

CBasicTextBox* CSchoolWarRankingSlot::CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign )
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub ( this, szControlKeyword );
	pStaticText->SetFont ( pFont );
	pStaticText->SetTextAlign ( nAlign );
	RegisterControl ( pStaticText );
	return pStaticText;
}

void CSchoolWarRankingSlot::DataUpdate( int nNUM, std::string strNAME, int nSCHOOL, int nKILL, int nDEATH , DWORD dwCHARID /*= UINT_MAX*/ )
{
	DataReset();

	if ( nSCHOOL < 0 || nSCHOOL >=3 )	return;

	if ( m_pRankSchool[nSCHOOL] )
		m_pRankSchool[nSCHOOL]->SetVisibleSingle( TRUE );

	D3DCOLOR dwColor = (IsSelf()) ? NS_UITEXTCOLOR::GREEN : NS_UITEXTCOLOR::WHITE;

	if ( m_pRankNum )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nNUM );
		m_pRankNum->SetText( strTEXT.GetString(), dwColor );
	}

	if ( m_pRankName )
	{
		m_pRankName->SetText( strNAME.c_str(), dwColor );
	}

	if ( m_pRankKill )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nKILL );
		m_pRankKill->SetText( strTEXT.GetString(), dwColor );
	}

	if ( m_pRankDeath )
	{
		CString strTEXT;
		strTEXT.Format( "%d", nDEATH );
		m_pRankDeath->SetText( strTEXT.GetString(), dwColor );
	}

	/*if ( dwCHARID != UINT_MAX )
	{
		DWORD dwCharID = GLGaeaClient::GetInstance().GetCharacter()->m_dwCharID;

		BOOL bSELF = ( dwCHARID == dwCharID );

		if ( m_pSelf )
			m_pSelf->SetVisibleSingle( bSELF );
	}*/
}

void CSchoolWarRankingSlot::DataReset()
{
	for ( int i = 0; i < 3; ++ i )
	{
		if ( m_pRankSchool[i] )
			m_pRankSchool[i]->SetVisibleSingle( FALSE );
	}

	if ( m_pRankNum )	m_pRankNum->ClearText();
	if ( m_pRankName )	m_pRankName->ClearText();
	if ( m_pRankKill )	m_pRankKill->ClearText();
	if ( m_pRankDeath ) m_pRankDeath->ClearText();
}