#pragma	once

#include "../UIWindowEx.h"

class	CBasicTextButton;
class	CSchoolWarRankingPageRank;
class	CSchoolWarRankingPageWinner;
class	CBasicLineBoxEx;
class	CBasicTextBox;
class	CSchoolWarRankingDisplay : public CUIWindowEx
{
protected:
	enum
	{
		SCHOOLWAR_RANKING_BACKGROUND = ET_CONTROL_NEXT,
		SCHOOLWAR_RANKING_PAGE_RANK,
		SCHOOLWAR_RANKING_BUTTON_RANK,
		SCHOOLWAR_RANKING_BUTTON_RANK_SG,
		SCHOOLWAR_RANKING_BUTTON_RANK_MP,
		SCHOOLWAR_RANKING_BUTTON_RANK_PHX,
		SCHOOLWAR_BUTTON_CLOSE,
	};

public:
	CSchoolWarRankingDisplay ();
	virtual	~CSchoolWarRankingDisplay ();

public:
	CBasicTextButton*	CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText );
	CBasicTextButton*	CreateTextButton ( char* szButton, UIGUID ControlID , char* szText );

	void CreateSubControl ();

private:
	int								m_nActivePage;
	CUIControl*						m_pBackGround;
	CBasicTextButton*				m_pButtonRankAll;
	CBasicTextButton*				m_pButtonRank[3];
	CSchoolWarRankingPageRank*		m_pPageRank;

	CBasicLineBoxEx*				m_pLineBoxUpper;
	CBasicLineBoxEx*				m_pLineBoxBottom;
	CBasicLineBoxEx*				m_pLineBoxTopWhite;
	CBasicLineBoxEx*				m_pLineBoxBottomWhite;

	CBasicTextButton*				m_pButtonClose;

public:
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual	void SetVisibleSingle ( BOOL bVisible );

public:
	void	RefreshRanking();
	void	OpenPage( int nPage );

	CBasicTextBox*	CreateStaticControl ( char* szControlKeyword, CD3DFontPar* pFont, D3DCOLOR dwColor, int nAlign );
	CBasicTextBox*	m_pText[5];
};