#include "StdAfx.h"
#include "../RanClientUILib/Interface/ItemImage.h"
#include "../EngineUILib/GUInterface/BasicButton.h"
#include "../EngineUIlib/GUInterface/BasicProgressBar.h"
#include "../RanClientUILib/Interface/GameTextControl.h"
#include "../RanClientUILib/Interface/UITextControl.h"
#include "../RanClientUILib/Interface/BasicLineBoxEx.h"
#include "../RanClientUILib/Interface/InnerInterface.h"
#include "RewardBuff.h"
#include "../RanClientLib/G-Logic/GLGaeaClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CRewardBuff::CRewardBuff():
     m_pRewardDummy ( NULL )
{
     memset( m_pRewardBuff, NULL, sizeof( CItemImage* ) * 3 );
	 memset( m_pRewardBuffBack, NULL, sizeof( CUIControl* ) * 3 );
}
CRewardBuff::~CRewardBuff()
{
}

void CRewardBuff::CreateSubControl ()
{
		for( int i = 0; i < 3; ++i )
		{
			CString strTemp;
			strTemp.Format("REWARD_BUFF%d", i );

			m_pRewardBuff[i] = new CItemImage;
			m_pRewardBuff[i]->CreateSub( this, strTemp, UI_FLAG_DEFAULT, REWARD_BUFF0+i );
			m_pRewardBuff[i]->CreateSubControl();
			RegisterControl( m_pRewardBuff[i] );

			strTemp.Format("REWARD_BUFF_EX%d", i );
			m_pRewardBuffBack[i] = new CUIControl;
			m_pRewardBuffBack[i]->CreateSub ( this, strTemp, UI_FLAG_DEFAULT );	
			RegisterControl ( m_pRewardBuffBack[i] );			
	
		}
}

void CRewardBuff::SetSwBonus( bool bWinner )
{
 for ( int i = 0; i < 3; i++ )
 {
  if ( bWinner )
  {
	m_pRewardBuff[i]->SetVisibleSingle( TRUE );	
	m_pRewardBuffBack[i]->SetVisibleSingle( TRUE );		
	}		
	else{
	m_pRewardBuff[i]->SetVisibleSingle( FALSE );
	m_pRewardBuffBack[i]->SetVisibleSingle( FALSE );		
	}
  }
}
void CRewardBuff::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
switch ( ControlID )
	{
	case REWARD_BUFF0:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Gain: x2 EXP.", NS_UITEXTCOLOR::PALEGREEN  );
			}
		}
		break;
		case REWARD_BUFF1:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Gain: x2 GOLD.", NS_UITEXTCOLOR::PALEGREEN  );
			}
		}
		break;
		case REWARD_BUFF2:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Gain: x2 Item Drop.", NS_UITEXTCOLOR::PALEGREEN  );
			}
		}
		break;
	case REWARD_BUFF3:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
			{
				CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Gain: +20% Speed Buff.", NS_UITEXTCOLOR::PALEGREEN  );
			}
		}
		break;		
		
	}
}