#include "stdafx.h"
#include "s_COdbcManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int COdbcManager::UserRegister(
	const TCHAR* szUsrID, 
	const TCHAR* szPasswd, 
	const TCHAR* szPasswd2,
	const TCHAR* szUsrEmail,
	const TCHAR* szCaptcha)
{
	CString strUserID = szUsrID;
	strUserID.Trim(_T(" "));
	strUserID.Replace(_T("'"), _T("''"));

	CString strPasswd = szPasswd;
	strPasswd.Trim(_T(" "));
	strPasswd.Replace(_T("'"), _T("''"));

	CString strPasswd2 = szPasswd2;
	strPasswd2.Trim(_T(" "));
	strPasswd2.Replace(_T("'"), _T("''"));

	CString strCaptcha = szCaptcha;
	strCaptcha.Trim(_T(" "));
	strCaptcha.Replace(_T("'"), _T("''"));

	CString strEmail = szUsrEmail;
	strEmail.Trim(_T(" "));
	strEmail.Replace(_T("'"), _T("''"));

	TCHAR szTemp[512] = {0};

	_snprintf( szTemp, 512, "{call user_register('%s','%s','%s','%s',?)}", 
												strUserID.GetString(),
												strPasswd.GetString(),
												strPasswd2.GetString(),
												strEmail.GetString());

	int nReturn = m_pUserDB->ExecuteSpInt(szTemp);

	return nReturn;
}
