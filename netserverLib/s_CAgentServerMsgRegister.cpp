#include "stdafx.h"
#include "s_CAgentServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void CAgentServer::ChinaMsgRegister( MSG_LIST* pMsg )
{

	CHINA_NET_REGISTER_DATA* pNml = NULL;

	if (m_pClientManager->IsAccountPassing( pMsg->dwClient ) == true)
	{
		return;
	}
	else // 인증중으로 세팅
	{
		m_pClientManager->SetAccountPassing( pMsg->dwClient, true );
	}

	pNml = reinterpret_cast<CHINA_NET_REGISTER_DATA *> (pMsg->Buffer);

	if (sizeof(CHINA_NET_REGISTER_DATA) != pNml->nmg.dwSize)
	{
		CConsoleMessage::GetInstance()->Write(
			
			_T("ERROR:CHINA_NET_REGISTER_DATA Wrong Message Size") );
		return;
	}
CConsoleMessage::GetInstance()->Write(_T("CHINA_NET_REGISTER_DATA begin") );
	// 복호화
	m_Tea.decrypt( pNml->szUserid, USR_ID_LENGTH+1 );
	m_Tea.decrypt( pNml->szPassword, USR_PASS_LENGTH );
	m_Tea.decrypt( pNml->szPassword2, USR_PASS_LENGTH );
	m_Tea.decrypt( pNml->szCaptcha, USR_CAPTCHA+1 );
	m_Tea.decrypt( pNml->szUserEmail, USR_EMAIL+1 );

	TCHAR szCaptcha[USR_CAPTCHA+1] = {0};
	TCHAR szPassword[USR_PASS_LENGTH] = {0};
	TCHAR szPassword2[USR_PASS_LENGTH] = {0};
	TCHAR szUserid[USR_ID_LENGTH+1] = {0};
	TCHAR szUserEmail[USR_EMAIL+1] = {0};
	DWORD dwClient;

	
	dwClient = pMsg->dwClient;

	StringCchCopy( szUserid, USR_ID_LENGTH+1, pNml->szUserid );
	StringCchCopy( szPassword, USR_PASS_LENGTH, pNml->szPassword );
	StringCchCopy( szPassword2, USR_PASS_LENGTH, pNml->szPassword2 );
	StringCchCopy( szCaptcha, USR_CAPTCHA+1, pNml->szCaptcha );
	StringCchCopy( szUserEmail, USR_EMAIL+1, pNml->szUserEmail );
	
	m_pClientManager->SetAccountPass( pMsg->dwClient, false );
	CAgentUserRegister* pAction = new CAgentUserRegister(
		szUserid, 
		szPassword,
		szPassword2,
		szUserEmail,
		szCaptcha,
		dwClient);
	if ( pAction )	CConsoleMessage::GetInstance()->Write(_T("CHINA_NET_REGISTER_DATA Init Success") );
	else	CConsoleMessage::GetInstance()->Write(_T("CHINA_NET_REGISTER_DATA failed") );
	COdbcManager::GetInstance()->AddUserJob( (CDbAction*) pAction );

}

void CAgentServer::ChinaMsgRegisterBack (NET_REGISTER_FEEDBACK_DATA2* nlfd2)
{
	NET_REGISTER_FEEDBACK_DATA2		nlfd;

	nlfd.nmg.nType = CHINA_NET_MSG_REGISTER_FB;
	
	DWORD	dwClient   = (DWORD) nlfd2->nClient;
	DWORD	dwSndBytes = 0;
	int		nUserNum   = 0;		

	if ( nlfd2->nResult == EM_REGISTER_FB_SUB_OK )
	{
		nlfd.nResult   = nlfd2->nResult;
		if ( m_pClientManager->IsOnline( dwClient ) == true )
		{
		
			SendClient(dwClient, &nlfd);
			CConsoleMessage::GetInstance()->Write(_T("NET_REGISTER_FEEDBACK_DATA2 EM_REGISTER_FB_SUB_OK") );
	
		}
	}else if ( nlfd2->nResult == EM_REGISTER_FB_SUB_FAIL )
	{
		nlfd.nResult   = nlfd2->nResult;
		if ( m_pClientManager->IsOnline( dwClient ) == true )
		{
		
			SendClient(dwClient, &nlfd);
			CConsoleMessage::GetInstance()->Write(_T("NET_REGISTER_FEEDBACK_DATA2 EM_REGISTER_FB_SUB_FAIL") );
	
		}
	}
}


