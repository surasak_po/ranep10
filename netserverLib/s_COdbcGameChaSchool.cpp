#include "stdafx.h"
#include "s_COdbcManager.h"
#include "s_CDbAction.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
* 캐릭터의 헤어스타일을 변경한다.
* \return DB_OK, DB_ERROR
*/
int COdbcManager::SetChaSchoolChange(DWORD dwChaNum, int nSchool)
{	
	if (nSchool < 0)
		return DB_ERROR;

	//std::strstream strTemp;
	//strTemp << "{call UpdateChaSchoolChange(";
	//strTemp << dwChaNum   << ",";
	//strTemp << nSchool;
	//strTemp << ",?)}";
	//strTemp << std::ends;

	TCHAR szTemp[128] = {0};
	_snprintf( szTemp, 128, "{call UpdateChaSchoolChange(%u,%d,?)}", dwChaNum, nSchool );

	int nReturn = m_pGameDB->ExecuteSpInt(szTemp);
	
//	strTemp.freeze( false );	// Note : std::strstream의 freeze. 안 하면 Leak 발생.

	return nReturn;
}