#include "stdafx.h"

#include "s_CDbAction.h"
#include "s_CSessionServer.h"
#include "s_CFieldServer.h"
#include "s_CAgentServer.h"

#include "../RanClientLib/G-Logic/GLCharData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CAgentUserRegister::CAgentUserRegister(
	const TCHAR* szUserID,
	const TCHAR* szPasswd, 
	const TCHAR* szPasswd2,
	const TCHAR* szUserEmail,
	const TCHAR* szCaptcha,
	DWORD  dwClientNum)
	: m_strUserID( szUserID )
	, m_strPasswd( szPasswd )	
	, m_strPasswd2( szPasswd2 )	
	, m_strUserEmail ( szUserEmail )
	, m_strCaptcha( szCaptcha )
	, m_dwClientNum ( dwClientNum )
{	

}

int CAgentUserRegister::Execute(CServer* pServer)
{
	CAgentServer* pTemp = reinterpret_cast<CAgentServer*> (pServer);

	int nRetCode = 0;	
	int nResult = COdbcManager::GetInstance()->UserRegister(m_strUserID.GetString(),
													     m_strPasswd.GetString(),
													     m_strPasswd2.GetString(),
													     m_strUserEmail.GetString(),
														 m_strCaptcha.GetString());

	// 전송할 구조체를 세팅한다.
	NET_REGISTER_FEEDBACK_DATA2 NetMsgFB;
	NetMsgFB.nmg.nType = CHINA_NET_MSG_REGISTER_FB;
	NetMsgFB.nClient = m_dwClientNum;

	StringCchCopy(NetMsgFB.szUserid,	USR_ID_LENGTH+1, m_strUserID.GetString()); // id
	StringCchCopy(NetMsgFB.szUserEmail,	USR_EMAIL+1, m_strUserEmail.GetString()); // id
	StringCchCopy(NetMsgFB.szUserpass,		USR_PASS_LENGTH+1, m_strPasswd.GetString()); // ip
	StringCchCopy(NetMsgFB.szUserpass2,		USR_PASS_LENGTH+1, m_strPasswd2.GetString()); // ip
	StringCchCopy(NetMsgFB.szCaptcha,		USR_CAPTCHA+1, m_strCaptcha.GetString()); // ip

	
	switch (nResult)
	{
	case DB_ERROR : 
		{
		NetMsgFB.nResult = EM_REGISTER_FB_SUB_SYSTEM;
		CConsoleMessage::GetInstance()->Write("CAgentUserREGISTER result %d DB ERROR", nResult);
		 pTemp->ChinaMsgRegisterBack( &NetMsgFB );
		}break;
	case 0 :
		{
		NetMsgFB.nResult = EM_REGISTER_FB_SUB_FAIL;
		CConsoleMessage::GetInstance()->Write("CAgentUserRegister result %d REGISTER FAIL", nResult);
		 pTemp->ChinaMsgRegisterBack( &NetMsgFB );
		}break;
	default : 
		{
		NetMsgFB.nResult = EM_REGISTER_FB_SUB_OK;
		CConsoleMessage::GetInstance()->Write("CAgentUserRegister result UserNum %d success!", nResult);
		 pTemp->ChinaMsgRegisterBack( &NetMsgFB );
		}break;
	};

		
	return NET_OK;
}